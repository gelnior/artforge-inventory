package fr.hd3d.inventory.ui.test;

import org.junit.Test;

import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RecordReaderSingleton;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.modeldata.writer.ModelDataJsonWriterSingleton;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUserRigthsResolver;
import fr.hd3d.common.ui.test.util.modeldata.reader.RecordReaderMock;
import fr.hd3d.common.ui.test.util.modeldata.reader.SheetReaderMock;
import fr.hd3d.common.ui.test.util.modeldata.writer.ModelDataJsonWriterMock;
import fr.hd3d.inventory.ui.client.controller.ListingController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.ListingModel;
import fr.hd3d.inventory.ui.client.model.modeldata.reader.SheetReaderSingleton;
import fr.hd3d.inventory.ui.test.mock.ExplorerMock;


public class ExplorerControllerTest extends UITestCase
{
    ListingModel model;
    ExplorerMock view;
    ListingController controller;

    /**
     * Set up readers and controllers.
     */
    @Override
    public void setUp()
    {
        super.setUp();

        RecordReaderSingleton.setInstanceAsMock(new RecordReaderMock());
        SheetReaderSingleton.setInstanceAsMock(new SheetReaderMock());
        ModelDataJsonWriterSingleton.setInstanceAsMock(new ModelDataJsonWriterMock());

        model = new ListingModel();
        view = new ExplorerMock(model);
        controller = new ListingController(view, model);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    /**
     * Test that : <br>
     * <ul>
     * <li>modifiers buttons are disabled and no sheet is selected when sheet loading return no sheet.</li>
     * <li>modifiers buttons are enabled and first sheet is selected when loading returns sheet.</li>
     * </ul>
     */
    @Test
    public void testOnSheetsLoaded()
    {
        SheetModelData sheet_01 = new SheetModelData(1L, "sheet_01");
        SheetModelData sheet_02 = new SheetModelData(1L, "sheet_02");

        EventDispatcher.forwardEvent(InventoryEvents.SHEETS_LOAD_FINISHED);
        assertFalse(this.view.isModifiersEnabled());
        assertFalse(this.view.isFirstSheetSelected());

        this.model.getSheetStore().add(sheet_01);
        this.model.getSheetStore().add(sheet_02);

        EventDispatcher.forwardEvent(InventoryEvents.SHEETS_LOAD_FINISHED);
        assertTrue(this.view.isModifiersEnabled());
        assertTrue(this.view.isFirstSheetSelected());
    }

    /**
     * Test that when show new sheet window event occurs, the right dialog is shown.
     */
    @Test
    public void testOnShowNewSheet()
    {
        EventDispatcher.forwardEvent(SheetEditorEvents.NEW_SHEET_CLICKED);
        assertTrue(this.view.isNewSheetEditorShown());
        assertFalse(this.view.isSheetEditorShown());
    }

    /**
     * Test that when show edit sheet window event occurs, the right dialog is shown.
     */
    @Test
    public void testShowEditSheet()
    {
        EventDispatcher.forwardEvent(SheetEditorEvents.EDIT_SHEET_CLICKED);
        assertFalse(this.view.isNewSheetEditorShown());
        assertTrue(this.view.isSheetEditorShown());
    }

    /**
     * Test that when view changes it resets selection, explorer record path is updated and records marked as created or
     * deleted are no longer marked.
     */
    @Test
    public void testOnChangeView()
    {
        SheetModelData sheet = new SheetModelData(1L, "sheet_01");
        RecordModelData record01 = new RecordModelData();
        RecordModelData record02 = new RecordModelData();

        this.model.getCreatedRecords().add(record01);
        this.model.getDeletedRecords().add(record02);
        this.view.getSelectedModels().add(record01);

        EventDispatcher.forwardEvent(ExplorerEvents.CHANGE_VIEW);
        assertTrue(this.model.getCreatedRecords().isEmpty());
        assertTrue(this.model.getDeletedRecords().isEmpty());
        assertFalse(this.view.getSelectedModels().isEmpty());

        String boundClassName = "Computer";
        sheet.setBoundClassName("Computer");
        this.view.selectSheet(sheet);

        this.model.getCreatedRecords().add(record01);
        this.model.getDeletedRecords().add(record02);

        EventDispatcher.forwardEvent(ExplorerEvents.CHANGE_VIEW);
        assertTrue(this.model.getCreatedRecords().isEmpty());
        assertTrue(this.model.getDeletedRecords().isEmpty());
        assertTrue(this.view.getSelectedModels().isEmpty());
        assertEquals(ServicesPath.getPath(boundClassName), this.model.getRecordPath());
        assertEquals(boundClassName, this.model.getSimpleClassName());
    }

    /**
     * Test nothing but ensure that no exception is raised.
     */
    @Test
    public void testOnDeleteView()
    {
        EventDispatcher.forwardEvent(SheetEditorEvents.DELETE_SHEET_CLICKED);
    }

    /**
     * Test that, when a view is deleted on server, the view is deleted locally and that the first sheet is selected. If
     * there are no more view, it tests that modifiers are disabled.
     */
    @Test
    public void testOnDeleteViewFinished()
    {
        SheetModelData sheet01 = new SheetModelData(1L, "sheet_01");
        SheetModelData sheet02 = new SheetModelData(2L, "sheet_02");

        this.model.getSheetStore().add(sheet01);
        this.model.getSheetStore().add(sheet02);
        this.view.enableModifiers(true);

        this.view.selectSheet(sheet01);

        AppEvent event = new AppEvent(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
        event.setData(CommonConfig.CLASS_EVENT_VAR_NAME, SheetModelData.SIMPLE_CLASS_NAME);
        EventDispatcher.forwardEvent(event);
        assertFalse(this.model.getSheetStore().contains(sheet01));
        assertEquals(sheet02, this.view.getCurrentSheet());
        assertTrue(this.view.isModifiersEnabled());

        EventDispatcher.forwardEvent(event);
        assertEquals(0, this.model.getSheetStore().getCount());
        assertFalse(this.view.isModifiersEnabled());
    }

    /**
     * Tests that when a sheet is saved, the sheet id is stored by the controller.
     */
    @Test
    public void testOnEndSave()
    {
        // TODO
    }

    /**
     * Test that when selection changes, the identity panel is shown (in fact refreshed) if there is only one row
     * selected and if that row has an ID.
     */
    @Test
    public void testOnSelectChange()
    {
        Hd3dModelData record = new Hd3dModelData();
        SheetModelData sheet = new SheetModelData(1L, "sheet01");

        record.setId(123L);
        sheet.setBoundClassName("Computer");

        this.view.getSelectedModels().add(record);
        this.view.selectSheet(sheet);
        //
        // AppEvent event = new AppEvent(ExplorateurEvents.SELECT_CHANGE);
        // event.setData(ExplorateurEvents.EVENT_VAR_MODELDATA, record);
        // EventDispatcher.forwardEvent(event);
        // assertEquals(record.getId(), this.view.getIdentityId());
        // assertTrue(this.view.isIdentityPanelShown());
        // assertEquals(sheet.getBoundClassName(), this.view.getIdentityBoundClassName());
        //
        // record = null;
        // event.setData(ExplorateurEvents.EVENT_VAR_MODELDATA, record);
        // EventDispatcher.forwardEvent(event);
        // assertFalse(this.view.isIdentityPanelShown());
        //
        // record = new Hd3dModelData();
        // event.setData(ExplorateurEvents.EVENT_VAR_MODELDATA, record);
        // EventDispatcher.forwardEvent(event);
        // assertFalse(this.view.isIdentityPanelShown());
        //
        // record = new Hd3dModelData();
        // record.setId(123L);
        // Hd3dModelData record2 = new Hd3dModelData();
        // record2.setId(124L);
        // this.view.getSelectedModels().add(record);
        // event.setData(ExplorateurEvents.EVENT_VAR_MODELDATA, record);
        // EventDispatcher.forwardEvent(event);
        // assertFalse(this.view.isIdentityPanelShown());
    }

    /**
     * Test that new row is inserted in the explorer, put it in the created records list and that selection is reset.
     */
    @Test
    public void testOnNewRow()
    {
        RecordModelData record1 = new RecordModelData();
        RecordModelData record2 = new RecordModelData();
        this.view.getSelectedModels().add(record1);
        this.view.getSelectedModels().add(record2);
        this.view.getExplorerStore().add(record1);
        this.view.getExplorerStore().add(record2);

        EventDispatcher.forwardEvent(ExplorerEvents.NEW_ROW);
        assertEquals(0, this.view.getSelectedModels().size());
        assertEquals(1, this.model.getCreatedRecords().size());
        assertEquals(3, this.view.getExplorerStore().size());
    }

    /**
     * When row is deleted, it test that row are removed from the explorer, the created records list and that selection
     * is reset.
     */
    @Test
    public void testOnDeleteRow()
    {
        RecordModelData record1 = new RecordModelData();
        RecordModelData record2 = new RecordModelData();
        RecordModelData record3 = new RecordModelData();

        record1.setId(123L);
        record2.setId(124L);
        record3.setId(125L);

        this.view.getSelectedModels().add(record1);
        this.view.getSelectedModels().add(record2);
        this.view.getExplorerStore().add(record1);
        this.view.getExplorerStore().add(record2);
        this.view.getExplorerStore().add(record3);

        EventDispatcher.forwardEvent(ExplorerEvents.DELETE_ROW);
        assertEquals(0, this.view.getSelectedModels().size());
        assertEquals(1, this.view.getExplorerStore().size());
        assertEquals(2, this.model.getDeletedRecords().size());

        EventDispatcher.forwardEvent(ExplorerEvents.NEW_ROW);
        EventDispatcher.forwardEvent(ExplorerEvents.NEW_ROW);
        this.view.getSelectedModels().addAll(this.view.getExplorerStore());

        assertEquals(2, this.model.getCreatedRecords().size());
        EventDispatcher.forwardEvent(ExplorerEvents.DELETE_ROW);
        assertEquals(0, this.view.getExplorerStore().size());
        assertEquals(0, this.view.getSelectedModels().size());
        assertEquals(3, this.model.getDeletedRecords().size());
        assertEquals(0, this.model.getCreatedRecords().size());

    }

    /**
     * Test nothing but ensures that no exception is raised.
     */
    @Test
    public void testOnSaveData()
    {
        EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA);
    }

    /**
     * Test that when explorer has finished its update operations, the controller knows it.
     */
    @Test
    public void testOnAfterSave()
    {
        EventDispatcher.forwardEvent(ExplorerEvents.AFTER_SAVE);
        assertTrue(controller.isUpdateDone());
    }

    /**
     * Test that when all creation queries has succeed, the controller knows it.
     */
    @Test
    public void testOnRowCreationSuccess()
    {
        this.controller.setCreationQueryNumber(3);
        assertEquals(3, this.controller.getCreationQueryNumber());
        EventDispatcher.forwardEvent(InventoryEvents.ROW_CREATION_SUCCESS);
        assertEquals(2, this.controller.getCreationQueryNumber());
        EventDispatcher.forwardEvent(InventoryEvents.ROW_CREATION_SUCCESS);
        assertEquals(1, this.controller.getCreationQueryNumber());
        EventDispatcher.forwardEvent(InventoryEvents.ROW_CREATION_SUCCESS);
        assertEquals(0, this.controller.getCreationQueryNumber());
        EventDispatcher.forwardEvent(InventoryEvents.ROW_CREATION_SUCCESS);
        assertEquals(0, this.controller.getCreationQueryNumber());
        EventDispatcher.forwardEvent(InventoryEvents.ROW_CREATION_SUCCESS);
    }

    /**
     * Test that when all deletion queries has succeed, the controller knows it.
     */
    @Test
    public void testOnRowDeletionSuccess()
    {
        this.controller.setDeletionQueryNumber(3);
        assertEquals(3, this.controller.getDeletionQueryNumber());
        EventDispatcher.forwardEvent(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
        assertEquals(2, this.controller.getDeletionQueryNumber());
        EventDispatcher.forwardEvent(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
        assertEquals(1, this.controller.getDeletionQueryNumber());
        EventDispatcher.forwardEvent(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
        assertEquals(0, this.controller.getDeletionQueryNumber());
        EventDispatcher.forwardEvent(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
        assertEquals(0, this.controller.getDeletionQueryNumber());
        EventDispatcher.forwardEvent(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
    }

    /**
     * Test that when relation button is clicked, the relation dialog is shown.
     */
    @Test
    public void testOnRelationsAddClick()
    {
        EventDispatcher.forwardEvent(ExplorerEvents.RELATIONS_ADD_CLICK);
        assertTrue(this.view.isRelationDialogShown());
    }

    /**
     * Tests that not allowed sheet tool items are hidden depending on the set permissions.
     */
    @Test
    public void testSheetPermissions()
    {
        this.controller.setToolbarPermission();
        assertFalse(this.view.isViewComboVisible());
        assertFalse(this.view.isNewSheetItemVisible());
        assertFalse(this.view.isEditSheetItemVisible());
        assertFalse(this.view.isDeleteSheetItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.addUserPermission("v1:sheets:read");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isViewComboVisible());
        assertFalse(this.view.isNewSheetItemVisible());
        assertFalse(this.view.isEditSheetItemVisible());
        assertFalse(this.view.isDeleteSheetItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.addUserPermission("v1:sheets:create");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isViewComboVisible());
        assertTrue(this.view.isNewSheetItemVisible());
        assertFalse(this.view.isEditSheetItemVisible());
        assertFalse(this.view.isDeleteSheetItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.addUserPermission("v1:sheets:*:update");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isViewComboVisible());
        assertTrue(this.view.isNewSheetItemVisible());
        assertTrue(this.view.isEditSheetItemVisible());
        assertFalse(this.view.isDeleteSheetItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.addUserPermission("v1:sheets:*:delete");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isViewComboVisible());
        assertTrue(this.view.isNewSheetItemVisible());
        assertTrue(this.view.isEditSheetItemVisible());
        assertTrue(this.view.isDeleteSheetItemVisible());
        assertFalse(this.view.isSeparatorVisible());
    }

    /**
     * Tests that not allowed model/type tool items are hidden depending on the set permissions.
     */
    @Test
    public void testModelsAndTypesPermissions()
    {
        this.controller.setToolbarPermission();
        assertFalse(this.view.isEditModelsItemVisible());
        assertFalse(this.view.isEditTypesItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.clearUserRights();
        TestUserRigthsResolver.addUserPermission("v1:computermodels:*:update");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isEditModelsItemVisible());
        assertFalse(this.view.isEditTypesItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.clearUserRights();
        TestUserRigthsResolver.addUserPermission("v1:devicemodels:*:update");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isEditModelsItemVisible());
        assertFalse(this.view.isEditTypesItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.clearUserRights();
        TestUserRigthsResolver.addUserPermission("v1:screenmodels:*:update");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isEditModelsItemVisible());
        assertFalse(this.view.isEditTypesItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.clearUserRights();
        TestUserRigthsResolver.addUserPermission("v1:devicetypes:*:update");
        this.controller.setToolbarPermission();
        assertFalse(this.view.isEditModelsItemVisible());
        assertTrue(this.view.isEditTypesItemVisible());
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.addUserPermission("v1:computertypes:*:update");
        this.controller.setToolbarPermission();
        assertFalse(this.view.isEditModelsItemVisible());
        assertTrue(this.view.isEditTypesItemVisible());
        assertFalse(this.view.isSeparatorVisible());
    }

    /**
     * Tests that not allowed separator tool item is shown only when model/type tool items and sheet tool items are
     * visibles.
     */
    @Test
    public void testSeparator()
    {
        TestUserRigthsResolver.clearUserRights();
        TestUserRigthsResolver.addUserPermission("v1:sheets:*:update");
        this.controller.setToolbarPermission();
        assertFalse(this.view.isSeparatorVisible());

        TestUserRigthsResolver.addUserPermission("v1:screenmodels:*:update");
        this.controller.setToolbarPermission();
        assertTrue(this.view.isSeparatorVisible());
    }

    @Test
    public void testSetExplorerPermissions()
    {

    }

    @Test
    public void testPermissionOnSelectChange()
    {

    }
}

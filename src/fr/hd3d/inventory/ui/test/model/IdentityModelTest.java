package fr.hd3d.inventory.ui.test.model;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;

import fr.hd3d.common.ui.client.widget.identitypanel.IdentityPanelModel;


public class IdentityModelTest extends TestCase
{
    IdentityPanelModel model = new IdentityPanelModel();

    @Override
    public void setUp()
    {

    }

    @Test
    public void testAddRelationStore()
    {
        List<String> store01 = new ArrayList<String>();
        List<String> store02 = new ArrayList<String>();
        List<String> store03 = new ArrayList<String>();

        store02.add("store_02_data");
        store03.add("store_03_data1");
        store03.add("store_03_data2");

        this.model.addRelationStore("store_01", store01);
        this.model.addRelationStore("store_02", store02);
        this.model.addRelationStore("store_03", store03);

        assertEquals(this.model.getRelationStore("store_01").getCount(), store01.size());
        assertEquals(this.model.getRelationStore("store_02").getCount(), store02.size());
        assertEquals(this.model.getRelationStore("store_03").getCount(), store03.size());
    }

    public void testClearRelationStoresData()
    {
        this.testAddRelationStore();
        this.model.clearRelationStoresData();

        assertEquals(this.model.getRelationStore("store_01").getCount(), 0);
        assertEquals(this.model.getRelationStore("store_02").getCount(), 0);
        assertEquals(this.model.getRelationStore("store_03").getCount(), 0);
    }

    public void testClearRelationStores()
    {
        this.testAddRelationStore();
        this.model.clearRelationStores();

        assertNull(this.model.getRelationStore("store_01"));
        assertNull(this.model.getRelationStore("store_02"));
        assertNull(this.model.getRelationStore("store_03"));
    }
}

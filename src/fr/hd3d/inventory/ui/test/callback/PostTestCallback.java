package fr.hd3d.inventory.ui.test.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;


public class PostTestCallback extends BaseCallback
{
    Hd3dModelData model;

    public PostTestCallback(Hd3dModelData model)
    {
        this.model = model;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        if (response.getStatus().isSuccess())
        {
            String url = response.getEntity().getIdentifier().toString();
            model.setId(RestRequestHandlerSingleton.getInstance().getIdFromLocation(url));
        }
    }
}

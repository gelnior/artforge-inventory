package fr.hd3d.inventory.ui.test.mock;

import fr.hd3d.inventory.ui.client.view.IModelDialog;


public class ModelDialogMock implements IModelDialog
{
    public boolean isShown = false;
    public boolean isSaveShown = false;
    public boolean isHeadersEnabled = true;

    private boolean isDeviceTabEnabled = false;
    private boolean isComputerTabEnabled = false;
    private boolean isScreenTabEnabled = false;

    public void show()
    {
        this.isShown = true;
    }

    public void hide()
    {
        this.isShown = false;
    }

    public boolean isShown()
    {
        return this.isShown;
    }

    public void hideSaving()
    {
        this.isSaveShown = false;
    }

    public void showSaving()
    {
        this.isSaveShown = true;
    }

    public boolean isSavingShown()
    {
        return this.isSaveShown;
    }

    public void disableTabHeaders()
    {
        this.isHeadersEnabled = false;
    }

    public void enableTabHeaders()
    {
        this.isHeadersEnabled = true;
    }

    public void enableComputerTab(boolean enabled)
    {
        this.isDeviceTabEnabled = enabled;
    }

    public void enableDeviceTab(boolean enabled)
    {
        this.isComputerTabEnabled = enabled;
    }

    public void enableScreenTab(boolean enabled)
    {
        this.isScreenTabEnabled = enabled;
    }

    public boolean isDeviceTabEnabled()
    {
        return this.isDeviceTabEnabled;
    }

    public boolean isComputerTabEnabled()
    {
        return this.isComputerTabEnabled;
    }

    public boolean isScreenTabEnabled()
    {
        return this.isScreenTabEnabled;
    }
}

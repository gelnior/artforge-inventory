package fr.hd3d.inventory.ui.test.mock;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.store.Store;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.mainview.CustomButtonInfos;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.inventory.ui.client.model.ListingModel;
import fr.hd3d.inventory.ui.client.view.IListingView;


public class ExplorerMock implements IListingView
{
    private boolean isFirstSheetSelected = false;
    private boolean isModifiersEnabled = false;
    private boolean isSheetEditorShown = false;
    private boolean isNewSheetEditorShown = false;
    private boolean isRelationDialogShown = false;

    private final List<Hd3dModelData> selection = new ArrayList<Hd3dModelData>();
    private final List<Hd3dModelData> explorerStore = new ArrayList<Hd3dModelData>();
    private SheetModelData currentSheet;
    private ListingModel model;
    private Long identityId;
    private String identityBoundClassName;
    private boolean isIdentityPanelShown;
    private boolean isSaveEnabled;
    private boolean isSavingShown;
    private boolean isViewComboVisible;
    private boolean isNewSheetItemVisible;
    private boolean isEditSheetItemVisible;
    private boolean isDeleteSheetItemVisible;
    private boolean isEditModelsItemVible;
    private boolean isEditTypesItemVisible;
    private boolean isSeparatorVisible;

    public ExplorerMock(ListingModel model)
    {
        this.model = model;
    }

    public boolean isFirstSheetSelected()
    {
        return this.isFirstSheetSelected;
    }

    public boolean isModifiersEnabled()
    {
        return this.isModifiersEnabled;
    }

    public boolean isSheetEditorShown()
    {
        return this.isSheetEditorShown;
    }

    public boolean isNewSheetEditorShown()
    {
        return this.isNewSheetEditorShown;
    }

    public boolean isIdentityPanelShown()
    {
        return isIdentityPanelShown;
    }

    public boolean isRelationDialogShown()
    {
        return isRelationDialogShown;
    }

    public boolean isSaveEnabled()
    {
        return isSaveEnabled;
    }

    public boolean isSavingShown()
    {
        return isSavingShown;
    }

    public Long getIdentityId()
    {
        return identityId;
    }

    public String getIdentityBoundClassName()
    {
        return identityBoundClassName;
    }

    public List<Hd3dModelData> getExplorerStore()
    {
        return this.explorerStore;
    }

    public SheetModelData getCurrentSheet()
    {
        return this.currentSheet;
    }

    public List<Hd3dModelData> getSelectedModels()
    {
        return this.selection;
    }

    public void setModel(ListingModel model)
    {
        this.model = model;
    }

    public void enableModifiers(boolean enabled)
    {
        this.isModifiersEnabled = enabled;
    }

    public void init()
    {}

    public void initWidget()
    {}

    public void insert(Hd3dModelData model)
    {
        this.explorerStore.add(model);
    }

    public void deleteRow(Hd3dModelData model)
    {
        this.explorerStore.remove(model);
    }

    public void resetSelection()
    {
        this.selection.clear();
    }

    public void selectFirstSheet()
    {
        this.isFirstSheetSelected = true;
        if (this.model.getSheetStore().getCount() > 0)
        {
            this.currentSheet = this.model.getSheetStore().getAt(0);
        }
    }

    public void selectSheet(SheetModelData sheet)
    {
        this.currentSheet = sheet;
    }

    public void showIdentityPanel(Long id, String boundClassName)
    {
        this.identityId = id;
        this.identityBoundClassName = boundClassName;
        this.isIdentityPanelShown = true;
    }

    public void hideIdentityPanel()
    {
        this.isIdentityPanelShown = false;
    }

    public void showRelationDialog()
    {
        this.isRelationDialogShown = true;
    }

    public void showSheetEditorWindow(boolean isNew)
    {
        if (isNew)
        {
            this.isNewSheetEditorShown = true;
        }
        else
        {
            this.isSheetEditorShown = true;
        }
    }

    public void disableSave()
    {
        this.isSaveEnabled = false;
    }

    public void enableSave()
    {
        this.isSaveEnabled = true;
    }

    public void hideSaving()
    {
        this.isSavingShown = false;
    }

    public void showSaving()
    {
        this.isSavingShown = true;
    }

    public void buildIdentityPanel(String simpleClassName)
    {
    // TODO Auto-generated method stub

    }

    public void clearAllIdentityPanel()
    {
    // TODO Auto-generated method stub

    }

    public void clearIdentityPanel()
    {
    // TODO Auto-generated method stub

    }

    public void refreshIdentityData()
    {
    // TODO Auto-generated method stub

    }

    public void refreshIdentityPanel(Long id)
    {
    // TODO Auto-generated method stub

    }

    public void selectFirstPage()
    {
    // TODO Auto-generated method stub

    }

    public void enableAddRowToolItem(boolean visible)
    {
    // TODO Auto-generated method stub

    }

    public void enableRelationToolItem(boolean visible)
    {
    // TODO Auto-generated method stub

    }

    public void enableDeleteRowToolItem(boolean visible)
    {
    // TODO Auto-generated method stub

    }

    public void enableSaveSheetToolItem(boolean visible)
    {
    // TODO Auto-generated method stub

    }

    public boolean isEditModelsItemVisible()
    {
        return isEditModelsItemVible;
    }

    public void setEditModelsItemVisible(boolean visible)
    {
        isEditModelsItemVible = visible;
    }

    public boolean isEditTypesItemVisible()
    {
        return isEditTypesItemVisible;
    }

    public void setEditTypesToolItemVisible(boolean visible)
    {
        isEditTypesItemVisible = visible;
    }

    public boolean isSeparatorVisible()
    {
        return isSeparatorVisible;
    }

    public void setSeparatorVisible(boolean visible)
    {
        isSeparatorVisible = visible;
    }

    public boolean isNewSheetItemVisible()
    {
        return isNewSheetItemVisible;
    }

    public void setNewSheetToolItemVisible(boolean visible)
    {
        isNewSheetItemVisible = visible;
    }

    public boolean isEditSheetItemVisible()
    {
        return isEditSheetItemVisible;
    }

    public void setEditSheetToolItemVisible(boolean visible)
    {
        isEditSheetItemVisible = visible;
    }

    public boolean isDeleteSheetItemVisible()
    {
        return isDeleteSheetItemVisible;
    }

    public void setDeleteSheetToolItemVisible(boolean visible)
    {
        isDeleteSheetItemVisible = visible;
    }

    public void setViewComboBoxVisible(boolean visible)
    {
        this.isViewComboVisible = visible;
    }

    public boolean isViewComboVisible()
    {
        return this.isViewComboVisible;
    }

    public String getFavCookieValue(String cookieSheetValue)
    {
        // TODO Auto-generated method stub
        return null;
    }

    public void createSheetEditorWindow(List<EntityModelData> entities)
    {
    // TODO Auto-generated method stub

    }

    public void putFavCookieValue(String key, String value)
    {
    // TODO Auto-generated method stub

    }

    public void showDeviceTypeIcon()
    {
    // TODO Auto-generated method stub

    }

    public void showModelsDialog()
    {
    // TODO Auto-generated method stub

    }

    public void showTypesDialog()
    {
    // TODO Auto-generated method stub

    }

    public void setEditorPermissions()
    {
    // TODO Auto-generated method stub

    }

    public void toggleAutoSave()
    {
    // TODO Auto-generated method stub

    }

    public void enableSheetComboBox(boolean enabled)
    {
    // TODO Auto-generated method stub

    }

    public void setExplorerSheet(SheetModelData sheet)
    {
    // TODO Auto-generated method stub

    }

    public void setSheetEntities(List<EntityModelData> entities)
    {
    // TODO Auto-generated method stub

    }

    @Override
    public Store<Hd3dModelData> getStore()
    {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void setUpdateScript(List<CustomButtonInfos> customButtonInfos)
    {
    // TODO Auto-generated method stub

    }

    @Override
    public void removeEmptyCreatedRows()
    {
    // TODO Auto-generated method stub

    }

}

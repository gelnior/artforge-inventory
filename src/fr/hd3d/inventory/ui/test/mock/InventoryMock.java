package fr.hd3d.inventory.ui.test.mock;

import fr.hd3d.inventory.ui.client.view.IInventory;


public class InventoryMock implements IInventory
{
    private boolean isWidgetInit = false;
    private boolean isControllerSet = false;
    private boolean isStartPanelShown = false;
    private boolean isLoading = false;

    private int displayedError = -1;

    public boolean isWidgetInit()
    {
        return this.isWidgetInit;
    }

    public boolean isStartPanelShown()
    {
        return this.isStartPanelShown;
    }

    public boolean isLoading()
    {
        return this.isLoading;
    }

    public int getDisplayedError()
    {
        return this.displayedError;
    }

    public void displayError(Integer error)
    {
        this.displayedError = error;
    }

    public void displayError(Integer error, String stack)
    {
        this.displayedError = error;
    }

    public void displayError(Integer error, String userMsg, String stack)
    {
        this.displayedError = error;
    }

    public void init()
    {}

    public void initWidgets()
    {
        this.isWidgetInit = true;
    }

    public void showStartPanel()
    {
        this.isStartPanelShown = true;
    }

    public void hideStartPanel()
    {
        this.isStartPanelShown = false;
    }

    public void startLoading()
    {
        this.isLoading = true;
    }

    public void stopLoading()
    {
        this.isLoading = false;
    }

    public void hideScriptRunningPanel()
    {
        // TODO Auto-generated method stub

    }

    public void showScriptRunningPanel()
    {
        // TODO Auto-generated method stub

    }

    public void setControllers()
    {
        this.isControllerSet = true;
    }
}

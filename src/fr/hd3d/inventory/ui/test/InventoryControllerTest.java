package fr.hd3d.inventory.ui.test;

import org.junit.Test;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.modeldata.reader.SheetReaderMock;
import fr.hd3d.inventory.ui.client.controller.InventoryController;
import fr.hd3d.inventory.ui.client.model.InventoryModel;
import fr.hd3d.inventory.ui.client.model.modeldata.reader.SheetReaderSingleton;
import fr.hd3d.inventory.ui.test.mock.InventoryMock;


/**
 * Test application main controller.
 * 
 * @author HD3D
 */
public class InventoryControllerTest extends UITestCase
{
    InventoryModel model;
    InventoryMock view;
    InventoryController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        SheetReaderSingleton.setInstanceAsMock(new SheetReaderMock());

        model = new InventoryModel();
        view = new InventoryMock();
        controller = new InventoryController(view, model);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    /**
     * Test that a message is displayed when an error occurs.
     */
    @Test
    public void testOnError()
    {
        ErrorDispatcher.sendError(CommonErrors.NO_CONFIG_FILE);
        assertEquals(CommonErrors.NO_CONFIG_FILE, this.view.getDisplayedError());
    }
}

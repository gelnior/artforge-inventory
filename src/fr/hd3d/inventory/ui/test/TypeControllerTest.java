package fr.hd3d.inventory.ui.test;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerEvents;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUserRigthsResolver;
import fr.hd3d.inventory.ui.client.controller.TypeDialogController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.TypeModel;
import fr.hd3d.inventory.ui.test.mock.TypeDialogMock;


public class TypeControllerTest extends UITestCase
{
    TypeModel model;
    TypeDialogMock view;
    TypeDialogController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        model = new TypeModel();
        view = new TypeDialogMock();
        controller = new TypeDialogController(view);
        controller.unMask();

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    @Test
    public void testOnSimpleSaveClicked()
    {
        this.view.enableTabHeaders();
        EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_CONFIRMED);
        assertFalse(this.view.isHeadersEnabled);
    }

    @Test
    public void testOnSimpleSaveSuccess()
    {
        EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_CONFIRMED);
        EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_SUCCESS);
        assertTrue(this.view.isHeadersEnabled);
    }

    @Test
    public void testOnTypeCloseClicked()
    {
        this.view.show();
        EventDispatcher.forwardEvent(InventoryEvents.TYPE_CLOSE_CLICKED);
        assertFalse(this.view.isShown());
        assertTrue(this.controller.isMasked());
    }

    @Test
    public void testOnError()
    {
        this.view.showSaving();
        this.view.disableTabHeaders();
        EventDispatcher.forwardEvent(CommonEvents.ERROR);
        assertFalse(this.view.isSavingShown());
        assertTrue(this.view.isHeadersEnabled);
    }

    @Test
    public void testPermission()
    {
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertFalse(this.view.isComputerTabEnabled());
        assertFalse(this.view.isDeviceTabEnabled());

        TestUserRigthsResolver.addUserPermission("v1:computertypes:read");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertTrue(this.view.isComputerTabEnabled());
        assertFalse(this.view.isDeviceTabEnabled());

        TestUserRigthsResolver.addUserPermission("v1:devicetypes:read");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertTrue(this.view.isComputerTabEnabled());
        assertTrue(this.view.isDeviceTabEnabled());
    }
}

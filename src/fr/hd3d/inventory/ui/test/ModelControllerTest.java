package fr.hd3d.inventory.ui.test;

import org.junit.Test;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerEvents;
import fr.hd3d.common.ui.test.UITestCase;
import fr.hd3d.common.ui.test.util.TestUserRigthsResolver;
import fr.hd3d.inventory.ui.client.controller.ModelDialogController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.test.mock.ModelDialogMock;


public class ModelControllerTest extends UITestCase
{
    ModelDialogMock view;
    ModelDialogController controller;

    @Override
    public void setUp()
    {
        super.setUp();

        view = new ModelDialogMock();
        controller = new ModelDialogController(view);

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.getControllers().clear();
        dispatcher.addController(controller);
    }

    @Test
    public void testOnSimpleSaveClicked()
    {
        this.view.enableTabHeaders();
        EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_CONFIRMED);
        assertFalse(this.view.isHeadersEnabled);
    }

    @Test
    public void testOnSimpleSaveSuccess()
    {
        EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_CONFIRMED);
        EventDispatcher.forwardEvent(SimpleExplorerEvents.SAVE_SUCCESS);
        assertTrue(this.view.isHeadersEnabled);
    }

    @Test
    public void testOnModelCloseClicked()
    {
        this.view.show();
        EventDispatcher.forwardEvent(InventoryEvents.MODEL_CLOSE_CLICKED);
        assertFalse(this.view.isShown());
        assertTrue(this.controller.isMasked());
    }

    @Test
    public void testOnError()
    {
        this.view.showSaving();
        this.view.disableTabHeaders();
        EventDispatcher.forwardEvent(CommonEvents.ERROR);
        assertFalse(this.view.isSavingShown());
        assertTrue(this.view.isHeadersEnabled);
    }

    @Test
    public void testPermission()
    {
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertFalse(this.view.isComputerTabEnabled());
        assertFalse(this.view.isDeviceTabEnabled());
        assertFalse(this.view.isScreenTabEnabled());

        TestUserRigthsResolver.addUserPermission("v1:devicemodels:read");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertTrue(this.view.isComputerTabEnabled());
        assertFalse(this.view.isDeviceTabEnabled());
        assertFalse(this.view.isScreenTabEnabled());

        TestUserRigthsResolver.addUserPermission("v1:computermodels:read");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertTrue(this.view.isComputerTabEnabled());
        assertTrue(this.view.isDeviceTabEnabled());
        assertFalse(this.view.isScreenTabEnabled());

        TestUserRigthsResolver.addUserPermission("v1:screenmodels:read");
        EventDispatcher.forwardEvent(CommonEvents.PERMISSION_INITIALIZED);
        assertTrue(this.view.isComputerTabEnabled());
        assertTrue(this.view.isDeviceTabEnabled());
        assertTrue(this.view.isScreenTabEnabled());
    }
}

package fr.hd3d.inventory.ui.client;

import com.google.gwt.core.client.EntryPoint;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.inventory.ui.client.view.impl.Inventory;
import fr.hd3d.inventory.ui.client.view.impl.ListingView;


/**
 * Entry point for Computer Inventory application.
 * 
 * @author HD3D
 */
public class ComputerInventory implements EntryPoint
{
    /**
     * This is the entry point method.
     */
    public void onModuleLoad()
    {
        PermissionUtil.setDebugOn();

        ListingView explorerView = new ListingView();
        explorerView.init();

        Inventory inventory = new Inventory(explorerView);
        inventory.init();

        EventDispatcher.forwardEvent(CommonEvents.START);
    }
}

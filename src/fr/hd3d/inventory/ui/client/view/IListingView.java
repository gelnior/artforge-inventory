package fr.hd3d.inventory.ui.client.view;

import java.util.List;

import com.extjs.gxt.ui.client.store.Store;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.mainview.CustomButtonInfos;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.inventory.ui.client.model.ListingModel;


/**
 * Interface for explorer view.
 * 
 * @author HD3D
 */
public interface IListingView
{
    public void setModel(ListingModel model);

    public void init();

    public void initWidget();

    public void showSheetEditorWindow(boolean isNew);

    public void selectFirstSheet();

    public void showRelationDialog();

    public void selectSheet(SheetModelData sheet);

    public List<Hd3dModelData> getSelectedModels();

    public void resetSelection();

    public void deleteRow(Hd3dModelData model);

    public void insert(Hd3dModelData model);

    public void enableModifiers(boolean enabled);

    public SheetModelData getCurrentSheet();

    public void showSaving();

    public void disableSave();

    public void hideSaving();

    public void enableSave();

    public void clearAllIdentityPanel();

    public void buildIdentityPanel(String simpleClassName);

    public void clearIdentityPanel();

    public void refreshIdentityPanel(Long id);

    public void refreshIdentityData();

    public void showTypesDialog();

    public void showModelsDialog();

    public void showDeviceTypeIcon();

    public void setViewComboBoxVisible(boolean visible);

    public void enableAddRowToolItem(boolean visible);

    public void enableDeleteRowToolItem(boolean visible);

    public void enableRelationToolItem(boolean visible);

    public void enableSaveSheetToolItem(boolean visible);

    public void setEditModelsItemVisible(boolean visible);

    public void setEditTypesToolItemVisible(boolean visible);

    public void setSeparatorVisible(boolean visible);

    public void setNewSheetToolItemVisible(boolean visible);

    public void setEditSheetToolItemVisible(boolean visible);

    public void setDeleteSheetToolItemVisible(boolean visible);

    public String getFavCookieValue(String cookieSheetValue);

    public void createSheetEditorWindow(List<EntityModelData> entities);

    public void putFavCookieValue(String key, String value);

    public void setEditorPermissions();

    public void toggleAutoSave();

    public void enableSheetComboBox(boolean enabled);

    public void setSheetEntities(List<EntityModelData> entities);

    public void setExplorerSheet(SheetModelData sheet);

    public Store<Hd3dModelData> getStore();

    public void setUpdateScript(List<CustomButtonInfos> customButtonInfos);

    public void removeEmptyCreatedRows();
}

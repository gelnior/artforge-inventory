package fr.hd3d.inventory.ui.client.view.column.renderer;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.ui.client.widget.explorer.view.grid.renderer.TaskTypeRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.FieldComboRenderer;
import fr.hd3d.inventory.ui.client.view.column.data.MapFactory;


public class InventoryStatusRenderer extends FieldComboRenderer
{

    public InventoryStatusRenderer()
    {
        super(MapFactory.getInventoryStatusMap());
    }

    @Override
    protected String getValueFromObject(Object obj)
    {
        String stringValue = "";

        if (obj instanceof String)
        {
            try
            {
                String value = obj.toString();
                EInventoryStatus inventoryStatus = EInventoryStatus.valueOf(value);
                String text = inventoryStatus.displayValue();
                String color = inventoryStatus.statusColor();
                String textColor = inventoryStatus.displayColor();

                stringValue = TaskTypeRenderer.getTypeRendered(text, color, textColor);
            }
            catch (IllegalArgumentException ex)
            {}
        }

        return stringValue;
    }

}

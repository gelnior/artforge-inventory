package fr.hd3d.inventory.ui.client.view.column.data;

import com.extjs.gxt.ui.client.core.FastMap;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.inventory.ui.client.constant.InventoryConstants;


public class MapFactory
{
    /** Constant strings to display : dialog messages, button label... */
    public static InventoryConstants CONSTANTS = GWT.create(InventoryConstants.class);

    private static FastMap<FieldModelData> screenQualificationType;
    private static FastMap<FieldModelData> screenQualificationMap;
    private static FastMap<FieldModelData> inventoryStatusMap;
    private static FastMap<FieldModelData> workerStatusMap;
    private static FastMap<FieldModelData> LicenseTypeMap;

    public static FastMap<FieldModelData> getScreenTypeMap()
    {
        if (screenQualificationType == null)
        {
            screenQualificationType = new FastMap<FieldModelData>();
            FieldUtils.addFieldToMap(screenQualificationType, 0, "CRT", "CRT");
            FieldUtils.addFieldToMap(screenQualificationType, 1, "LCD", "LCD");
            FieldUtils.addFieldToMap(screenQualificationType, 2, CONSTANTS.Unknown(), "UNKNOWN");
        }

        return screenQualificationType;
    }

    public static FastMap<FieldModelData> getScreenQualificationMap()
    {
        if (screenQualificationMap == null)
        {
            screenQualificationMap = new FastMap<FieldModelData>();
            FieldUtils.addFieldToMap(screenQualificationMap, 0, CONSTANTS.Excellent(), "REFERENCE");
            FieldUtils.addFieldToMap(screenQualificationMap, 1, CONSTANTS.Good(), "COLOR");
            FieldUtils.addFieldToMap(screenQualificationMap, 2, CONSTANTS.Average(), "GRAPHIC");
            FieldUtils.addFieldToMap(screenQualificationMap, 3, CONSTANTS.Bad(), "OFFICE");
            FieldUtils.addFieldToMap(screenQualificationMap, 4, CONSTANTS.VeryBad(), "OUT");
            FieldUtils.addFieldToMap(screenQualificationMap, 5, CONSTANTS.Unknown(), "UNKNOWN");
        }

        return screenQualificationMap;
    }

    public static FastMap<FieldModelData> getInventoryStatusMap()
    {
        if (inventoryStatusMap == null)
        {
            inventoryStatusMap = new FastMap<FieldModelData>();
            FieldUtils.addFieldToMap(inventoryStatusMap, 0, CONSTANTS.OK(), "OK");
            FieldUtils.addFieldToMap(inventoryStatusMap, 1, CONSTANTS.Destroyed(), "DESTROYED");
            FieldUtils.addFieldToMap(inventoryStatusMap, 2, CONSTANTS.OOS(), "OUT_OF_SERVICE");
            FieldUtils.addFieldToMap(inventoryStatusMap, 3, CONSTANTS.Missing(), "MISSING");
            FieldUtils.addFieldToMap(inventoryStatusMap, 4, CONSTANTS.Unknown(), "UNKNOWN");
            FieldUtils.addFieldToMap(inventoryStatusMap, 5, CONSTANTS.AfterSales(), "AFTER_SALES");
        }

        return inventoryStatusMap;
    }

    public static FastMap<FieldModelData> getWorkerStatusMap()
    {
        if (workerStatusMap == null)
        {
            workerStatusMap = new FastMap<FieldModelData>();
            FieldUtils.addFieldToMap(workerStatusMap, 0, CONSTANTS.NoWorker(), "NO_WORKER");
            FieldUtils.addFieldToMap(workerStatusMap, 1, CONSTANTS.Inactive(), "UNACTIVE");
            FieldUtils.addFieldToMap(workerStatusMap, 2, CONSTANTS.Active(), "ACTIVE");
            FieldUtils.addFieldToMap(workerStatusMap, 3, CONSTANTS.Running(), "RUNNING");
        }

        return workerStatusMap;
    }

    public static FastMap<FieldModelData> getLicenseTypeMap()
    {
        if (LicenseTypeMap == null)
        {
            LicenseTypeMap = new FastMap<FieldModelData>();
            FieldUtils.addFieldToMap(LicenseTypeMap, 0, CONSTANTS.Classic(), "CLASSIC");
            FieldUtils.addFieldToMap(LicenseTypeMap, 1, CONSTANTS.Domain(), "DOMAIN");
            FieldUtils.addFieldToMap(LicenseTypeMap, 2, CONSTANTS.Float(), "FLOAT");
            FieldUtils.addFieldToMap(LicenseTypeMap, 3, CONSTANTS.NodeLock(), "NODE_LOCK");
            FieldUtils.addFieldToMap(LicenseTypeMap, 4, CONSTANTS.Render(), "RENDER");
        }

        return LicenseTypeMap;
    }

}

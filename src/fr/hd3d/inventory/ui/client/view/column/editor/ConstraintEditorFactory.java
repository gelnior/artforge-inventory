package fr.hd3d.inventory.ui.client.view.column.editor;

import java.util.Map;

import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ProcessorModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ComputerModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.ComputerTypeReader;
import fr.hd3d.common.ui.client.modeldata.reader.DeviceModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.DeviceTypeReader;
import fr.hd3d.common.ui.client.modeldata.reader.ProcessorReader;
import fr.hd3d.common.ui.client.modeldata.reader.RoomReader;
import fr.hd3d.common.ui.client.modeldata.reader.ScreenModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.SoftwareReader;
import fr.hd3d.common.ui.client.modeldata.reader.StudioMapReader;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.inventory.ui.client.view.column.data.MapFactory;


/**
 * Editor Factory provides specific cell editors.
 * 
 * @author HD3D
 */
public class ConstraintEditorFactory
{
    public static final String EDITOR_SCREEN_TYPE = "screen-type";
    public static final String EDITOR_SCREEN_QUALIFICATION = "screen-qualification";
    public static final String EDITOR_COMPUTER_WORKER = "worker-status";
    public static final String EDITOR_LICENSE_TYPE = "license-type";
    public static final String EDITOR_DEVICE_TYPE = "device-type";
    public static final Object EDITOR_SOFTWARE = "software";
    public static final Object EDITOR_COMPUTER_TYPE = "computer-type";
    public static final Object EDITOR_COMPUTER_MODEL = "computer-model";
    public static final Object EDITOR_SCREEN_MODEL = "screen-model";
    public static final Object EDITOR_DEVICE_MODEL = "device-model";
    public static final Object EDITOR_ROOM = "room";

    /**
     * @return Combo box editor with screen type possible values : LCD, CRT.
     */
    public static FieldComboBox getScreenTypeEditor()
    {
        final FieldComboBox combo = getComboFromMap(MapFactory.getScreenTypeMap());

        return combo;
    }

    /**
     * @return Combo box editor with screen qualification possible values : EXCELLENT, GOOD, AVERAGE, BAD, VERY_BAD.
     */
    public static FieldComboBox getScreenQualificationEditor()
    {
        final FieldComboBox combo = getComboFromMap(MapFactory.getScreenQualificationMap());

        return combo;
    }

    /**
     * @return Combo box editor with inventory status possible values : OK, DESTROYED, OUT_OF_SERVICE, MISSING.
     */
    public static FieldComboBox getInventoryStatusEditor()
    {
        final FieldComboBox combo = getComboFromMap(MapFactory.getInventoryStatusMap());

        return combo;
    }

    /**
     * @return Combo box editor with worker status possible values : NO_WORKER, INACTIVE, ACTIVE, RUNNING.
     */
    public static FieldComboBox getWorkerStatusEditor()
    {
        FieldComboBox combo = getComboFromMap(MapFactory.getWorkerStatusMap());

        return combo;
    }

    /**
     * @return Combo box editor with device type possible values : GRAPHIC_CARD, HARD_DRIVE, KEYBOARD, MOUSE, PRINTER,
     *         OTHER.
     */
    public static ModelDataComboBox<DeviceTypeModelData> getDeviceTypeEditor()
    {
        ModelDataComboBox<DeviceTypeModelData> combo = new ModelDataComboBox<DeviceTypeModelData>(
                new DeviceTypeReader());
        combo.setContextMenu();

        return combo;
    }

    public static ModelDataComboBox<RoomModelData> getRoomEditor()
    {
        final ModelDataComboBox<RoomModelData> combo = new ModelDataComboBox<RoomModelData>(new RoomReader());

        return combo;
    }

    /**
     * @return Combo box editor with license type possible values : CLASSIC, DOMAIN, FLOAT, NODE_LOCK, RENDER.
     */
    public static FieldComboBox getLicenseTypeEditor()
    {
        final FieldComboBox combo = getComboFromMap(MapFactory.getLicenseTypeMap());

        return combo;
    }

    public static ModelDataComboBox<ProcessorModelData> getProcessorEditor()
    {
        final ModelDataComboBox<ProcessorModelData> combo = new ModelDataComboBox<ProcessorModelData>(
                new ProcessorReader());
        combo.setContextMenu();

        return combo;
    }

    public static ModelDataComboBox<StudioMapModelData> getStudioMapEditor()
    {
        final ModelDataComboBox<StudioMapModelData> combo = new ModelDataComboBox<StudioMapModelData>(
                new StudioMapReader());
        combo.setContextMenu();

        return combo;
    }

    public static ModelDataComboBox<SoftwareModelData> getSoftwareEditor()
    {
        final ModelDataComboBox<SoftwareModelData> combo = new ModelDataComboBox<SoftwareModelData>(
                new SoftwareReader());
        combo.setContextMenu();

        return combo;
    }

    public static ModelDataComboBox<ComputerTypeModelData> getComputerTypeEditor()
    {
        final ModelDataComboBox<ComputerTypeModelData> combo = new ModelDataComboBox<ComputerTypeModelData>(
                new ComputerTypeReader());
        combo.setContextMenu();

        return combo;
    }

    public static ModelDataComboBox<ComputerModelModelData> getComputerModelEditor()
    {
        final ModelDataComboBox<ComputerModelModelData> combo = new ModelDataComboBox<ComputerModelModelData>(
                new ComputerModelReader());
        combo.setContextMenu();

        return combo;
    }

    public static ModelDataComboBox<DeviceModelModelData> getDeviceModelEditor()
    {
        final ModelDataComboBox<DeviceModelModelData> combo = new ModelDataComboBox<DeviceModelModelData>(
                new DeviceModelReader());
        combo.setContextMenu();

        return combo;
    }

    public static ModelDataComboBox<ScreenModelModelData> getScreenModelEditor()
    {
        final ModelDataComboBox<ScreenModelModelData> combo = new ModelDataComboBox<ScreenModelModelData>(
                new ScreenModelReader());
        combo.setContextMenu();

        return combo;
    }

    /**
     * @param map
     *            The map containing the field model data (the key of each field is its value).
     * @return A combo box filled with field contained in map given in parameter and order by field id.
     */
    private static FieldComboBox getComboFromMap(Map<String, FieldModelData> map)
    {
        FieldComboBox combo = new FieldComboBox();
        for (Map.Entry<String, FieldModelData> entry : map.entrySet())
        {
            combo.getStore().add(entry.getValue());
        }

        return combo;
    }
}

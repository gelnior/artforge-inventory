package fr.hd3d.inventory.ui.client.view.column;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.Renderer;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.explorer.model.BaseColumnInfoProvider;
import fr.hd3d.common.ui.client.widget.explorer.model.modeldata.IColumn;
import fr.hd3d.common.ui.client.widget.grid.renderer.RecordModelDataRenderer;
import fr.hd3d.common.ui.client.widget.grid.renderer.UnitRenderer;
import fr.hd3d.inventory.ui.client.view.column.editor.EditorFactory;
import fr.hd3d.inventory.ui.client.view.column.renderer.RendererFactory;


/**
 * Needed by Explorer to specify special column behaviors.
 * 
 * @author HD3D
 */
public class InventoryColumnInfoProvider extends BaseColumnInfoProvider
{
    private final static String MHZ_UNIT = " Mhz";
    private final static String INCH_UNIT = "\"";
    private final static String MO_UNIT = " Mo";

    public InventoryColumnInfoProvider()
    {
        super();
    }

    @Override
    public CellEditor getEditor(String name, IColumn column)
    {
        CellEditor editor = super.getEditor(name, column);
        if (name.equals(Editor.SCREEN_TYPE))
        {
            return EditorFactory.getScreenTypeEditor();
        }
        else if (name.equals(Editor.COMPUTER_TYPE))
        {
            return EditorFactory.getComputerTypeEditor();
        }
        else if (name.equals(Editor.DEVICE_TYPE))
        {
            return EditorFactory.getDeviceTypeEditor();
        }
        else if (name.equals(Editor.LICENSE_TYPE))
        {
            return EditorFactory.getLicenseTypeEditor();
        }
        else if (name.equals(Editor.SCREEN_QUALIFICATION))
        {
            return EditorFactory.getScreenQualificationEditor();
        }
        else if (name.equals(Editor.INVENTORY_STATUS))
        {
            return EditorFactory.getInventoryStatusEditor();
        }
        else if (name.equals(Editor.WORKER_STATUS))
        {
            return EditorFactory.getWorkerStatusEditor();
        }
        else if (name.equals(Editor.COMPUTER_MODEL))
        {
            return EditorFactory.getComputerModelEditor();
        }
        else if (name.equals(Editor.SCREEN_MODEL))
        {
            return EditorFactory.getScreenModelEditor();
        }
        else if (name.equals(Editor.DEVICE_MODEL))
        {
            return EditorFactory.getDeviceModelEditor();
        }
        else if (name.equals(Editor.ROOM))
        {
            return EditorFactory.getRoomEditor();
        }
        else if (name.equals(Editor.SOFTWARE))
        {
            return EditorFactory.getSoftwareEditor();
        }
        else if (FieldUtils.isEntityPerson(name))
        {
            return EditorFactory.getPersonEditor();
        }

        return editor;
    }

    @Override
    public GridCellRenderer<Hd3dModelData> getRenderer(String name)
    {
        GridCellRenderer<Hd3dModelData> renderer = super.getRenderer(name);

        if (name.equals(Renderer.SCREEN_TYPE))
        {
            return RendererFactory.getScreenTypeRenderer();
        }
        else if (name.equals(Renderer.SCREEN_QUALIFICATION))
        {
            return RendererFactory.getScreenQualificationRenderer();
        }
        else if (name.equals(Renderer.INVENTORY_STATUS))
        {
            return RendererFactory.getInventoryStatusRenderer();
        }
        else if (name.equals(Renderer.WORKER_STATUS))
        {
            return RendererFactory.getWorkerStatusRenderer();
        }
        else if (name.equals(Renderer.PERSON))
        {
            return RendererFactory.getInventoryWorkerRenderer();
        }
        else if (name.equals(Renderer.FREQUENCY))
        {
            return new UnitRenderer(MHZ_UNIT);
        }
        else if (name.equals(Renderer.SCREEN_SIZE))
        {
            return new UnitRenderer(INCH_UNIT);
        }
        else if (name.equals(Renderer.RAM))
        {
            return new UnitRenderer(MO_UNIT);
        }
        else if (name.equals(Renderer.RECORD))
        {
            return new RecordModelDataRenderer();
        }
        else if (name.equals(Renderer.LICENSE_TYPE))
        {
            return RendererFactory.getLicenseTypeRenderer();
        }

        return renderer;
    }
}

package fr.hd3d.inventory.ui.client.view.column;

import com.extjs.gxt.ui.client.data.BaseModelData;
import com.extjs.gxt.ui.client.widget.form.Field;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.widget.constraint.IEditorProvider;
import fr.hd3d.inventory.ui.client.view.column.editor.ConstraintEditorFactory;


/**
 * Needed by Explorer to specify special column behaviors.
 * 
 * @author HD3D
 */
public class InventoryEditorProvider implements IEditorProvider
{
    public Field<?> getEditor(String name, EConstraintOperator operator, BaseModelData baseModel)
    {
        if (name.equals(Editor.SCREEN_TYPE))
        {
            return ConstraintEditorFactory.getScreenTypeEditor();
        }
        else if (name.equals(Editor.COMPUTER_TYPE))
        {
            return ConstraintEditorFactory.getComputerTypeEditor();
        }
        else if (name.equals(Editor.DEVICE_TYPE))
        {
            return ConstraintEditorFactory.getDeviceTypeEditor();
        }
        else if (name.equals(Editor.LICENSE_TYPE))
        {
            return ConstraintEditorFactory.getLicenseTypeEditor();
        }
        else if (name.equals(Editor.SCREEN_QUALIFICATION))
        {
            return ConstraintEditorFactory.getScreenQualificationEditor();
        }
        else if (name.equals(Editor.INVENTORY_STATUS))
        {
            return ConstraintEditorFactory.getInventoryStatusEditor();
        }
        else if (name.equals(Editor.WORKER_STATUS))
        {
            return ConstraintEditorFactory.getWorkerStatusEditor();
        }
        else if (name.equals(Editor.COMPUTER_MODEL))
        {
            return ConstraintEditorFactory.getComputerModelEditor();
        }
        else if (name.equals(Editor.SCREEN_MODEL))
        {
            return ConstraintEditorFactory.getScreenModelEditor();
        }
        else if (name.equals(Editor.DEVICE_MODEL))
        {
            return ConstraintEditorFactory.getDeviceModelEditor();
        }
        else if (name.equals(Editor.ROOM))
        {
            return ConstraintEditorFactory.getRoomEditor();
        }
        else if (name.equals("processor"))
        {
            return ConstraintEditorFactory.getProcessorEditor();
        }
        else if (name.equals("studio-map"))
        {
            return ConstraintEditorFactory.getStudioMapEditor();
        }
        else if (name.equals(Editor.SOFTWARE))
        {
            return ConstraintEditorFactory.getSoftwareEditor();
        }

        return null;
    }

    public String getItemConstraintColumnName(String name)
    {
        return null;
    }
}

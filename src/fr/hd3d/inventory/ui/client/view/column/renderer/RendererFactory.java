package fr.hd3d.inventory.ui.client.view.column.renderer;

import com.extjs.gxt.ui.client.widget.grid.GridCellRenderer;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.widget.grid.renderer.FieldComboRenderer;
import fr.hd3d.inventory.ui.client.view.column.data.MapFactory;


public class RendererFactory
{
    public static GridCellRenderer<Hd3dModelData> getScreenTypeRenderer()
    {
        return new FieldComboRenderer(MapFactory.getScreenTypeMap());
    }

    public static GridCellRenderer<Hd3dModelData> getScreenQualificationRenderer()
    {
        return new FieldComboRenderer(MapFactory.getScreenQualificationMap());
    }

    public static GridCellRenderer<Hd3dModelData> getInventoryStatusRenderer()
    {
        return new InventoryStatusRenderer();
    }

    public static GridCellRenderer<Hd3dModelData> getInventoryWorkerRenderer()
    {
        return new InventoryWorkerRenderer<Hd3dModelData>();
    }

    public static GridCellRenderer<Hd3dModelData> getWorkerStatusRenderer()
    {
        return new FieldComboRenderer(MapFactory.getWorkerStatusMap());
    }

    public static GridCellRenderer<Hd3dModelData> getLicenseTypeRenderer()
    {
        return new FieldComboRenderer(MapFactory.getLicenseTypeMap());
    }
}

package fr.hd3d.inventory.ui.client.view.column.editor;

import com.extjs.gxt.ui.client.widget.grid.CellEditor;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ComputerModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.ComputerTypeReader;
import fr.hd3d.common.ui.client.modeldata.reader.DeviceModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.DeviceTypeReader;
import fr.hd3d.common.ui.client.modeldata.reader.RoomReader;
import fr.hd3d.common.ui.client.modeldata.reader.ScreenModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.SoftwareReader;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.util.FieldUtils;
import fr.hd3d.common.ui.client.widget.FieldComboBox;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.grid.editor.FieldComboBoxEditor;
import fr.hd3d.common.ui.client.widget.grid.editor.ModelDataComboBoxEditor;
import fr.hd3d.inventory.ui.client.view.column.data.MapFactory;
import fr.hd3d.inventory.ui.client.view.widget.InventoryWorkerComboBox;


/**
 * Editor Factory provides specific cell editors.
 * 
 * @author HD3D
 */
public class EditorFactory
{
    /**
     * @return Combo box editor with screen type possible values : LCD, CRT.
     */
    public static CellEditor getScreenTypeEditor()
    {
        final FieldComboBox combo = FieldUtils.getComboFromMap(MapFactory.getScreenTypeMap());

        return new FieldComboBoxEditor(combo);
    }

    /**
     * @return Combo box editor with screen qualification possible values : EXCELLENT, GOOD, AVERAGE, BAD, VERY_BAD.
     */
    public static CellEditor getScreenQualificationEditor()
    {
        final FieldComboBox combo = FieldUtils.getComboFromMap(MapFactory.getScreenQualificationMap());

        return new FieldComboBoxEditor(combo);
    }

    /**
     * @return Combo box editor with inventory status possible values : OK, DESTROYED, OUT_OF_SERVICE, MISSING.
     */
    public static CellEditor getInventoryStatusEditor()
    {
        final FieldComboBox combo = FieldUtils.getComboFromMap(MapFactory.getInventoryStatusMap());

        return new FieldComboBoxEditor(combo);
    }

    /**
     * @return Combo box editor with worker status possible values : NO_WORKER, INACTIVE, ACTIVE, RUNNING.
     */
    public static CellEditor getWorkerStatusEditor()
    {
        FieldComboBox combo = FieldUtils.getComboFromMap(MapFactory.getWorkerStatusMap());

        return new FieldComboBoxEditor(combo);
    }

    /**
     * @return Combo box editor with device type possible values : GRAPHIC_CARD, HARD_DRIVE, KEYBOARD, MOUSE, PRINTER,
     *         OTHER.
     */
    public static CellEditor getDeviceTypeEditor()
    {
        ModelDataComboBox<DeviceTypeModelData> combo = new ModelDataComboBox<DeviceTypeModelData>(
                new DeviceTypeReader());

        return new ModelDataComboBoxEditor<DeviceTypeModelData>(combo);
    }

    public static CellEditor getRoomEditor()
    {
        final ModelDataComboBox<RoomModelData> combo = new ModelDataComboBox<RoomModelData>(new RoomReader());

        return new ModelDataComboBoxEditor<RoomModelData>(combo);
    }

    /**
     * @return Combo box editor with license type possible values : CLASSIC, DOMAIN, FLOAT, NODE_LOCK, RENDER.
     */
    public static CellEditor getLicenseTypeEditor()
    {
        final FieldComboBox combo = FieldUtils.getComboFromMap(MapFactory.getLicenseTypeMap());

        return new FieldComboBoxEditor(combo);
    }

    public static CellEditor getSoftwareEditor()
    {
        final ModelDataComboBox<SoftwareModelData> combo = new ModelDataComboBox<SoftwareModelData>(
                new SoftwareReader());

        return new ModelDataComboBoxEditor<SoftwareModelData>(combo);
    }

    public static CellEditor getComputerTypeEditor()
    {
        final ModelDataComboBox<ComputerTypeModelData> combo = new ModelDataComboBox<ComputerTypeModelData>(
                new ComputerTypeReader());

        return new ModelDataComboBoxEditor<ComputerTypeModelData>(combo);
    }

    public static CellEditor getComputerModelEditor()
    {
        final ModelDataComboBox<ComputerModelModelData> combo = new ModelDataComboBox<ComputerModelModelData>(
                new ComputerModelReader());

        return new ModelDataComboBoxEditor<ComputerModelModelData>(combo);
    }

    public static CellEditor getDeviceModelEditor()
    {
        final ModelDataComboBox<DeviceModelModelData> combo = new ModelDataComboBox<DeviceModelModelData>(
                new DeviceModelReader());

        return new ModelDataComboBoxEditor<DeviceModelModelData>(combo);
    }

    public static CellEditor getScreenModelEditor()
    {
        final ModelDataComboBox<ScreenModelModelData> combo = new ModelDataComboBox<ScreenModelModelData>(
                new ScreenModelReader());

        return new ModelDataComboBoxEditor<ScreenModelModelData>(combo);
    }

    /**
     * @return Grid editor to edit person with auto-complete combo box.
     */
    public static CellEditor getPersonEditor()
    {
        InventoryWorkerComboBox combo = new InventoryWorkerComboBox();

        return new ModelDataComboBoxEditor<PersonModelData>(combo);
    }
}

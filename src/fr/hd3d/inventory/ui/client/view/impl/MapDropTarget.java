package fr.hd3d.inventory.ui.client.view.impl;

import java.util.List;

import com.extjs.gxt.ui.client.dnd.DropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.Component;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.inventory.ui.client.config.InventoryConfig;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


public class MapDropTarget extends DropTarget
{

    public MapDropTarget(Component target)
    {
        super(target);
    }

    @Override
    protected void onDragDrop(DNDEvent event)
    {
        List<ComputerModelData> computers = event.getData();

        AppEvent mvcEvent = new AppEvent(InventoryEvents.MAP_COMPUTER_DROPPED);
        mvcEvent.setData(InventoryConfig.COMPUTER_DROPPED_EVENT_VAR_NAME, computers.get(0));
        mvcEvent.setData(InventoryConfig.DROP_LOCATION_X_EVENT_VAR_NAME, new Long(event.getClientX()
                - event.getDropTarget().getComponent().getAbsoluteLeft()));
        mvcEvent.setData(InventoryConfig.DROP_LOCATION_Y_EVENT_VAR_NAME, new Long(event.getClientY()
                - event.getDropTarget().getComponent().getAbsoluteTop()));

        EventDispatcher.forwardEvent(mvcEvent);
    }
}

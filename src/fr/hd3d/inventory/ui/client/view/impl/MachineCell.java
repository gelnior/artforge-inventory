package fr.hd3d.inventory.ui.client.view.impl;

import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.util.TextMetrics;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.Text;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Element;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.inventory.ui.client.view.widget.EditRelationsMenuItem;


/**
 * @author melissa-faucher
 * 
 */
public class MachineCell extends Text
{
    private static final String MACHINE_CLASS = "machine";
    private static final String MACHINE_SELECTED_CLASS = "machine-selected";
    private static final String MACHINE_DEFAULT_CLASS = "machine-default";
    private final String baseStyleWithoutLabel;

    private final ComputerModelData machine;

    private final MapView view;

    private String backgroundColor;
    private String textColor;
    private boolean visibleLabel = true;
    private String displayedText = "";
    private String htmlElement = "";
    private String labelToDisplay = ComputerModelData.NAME_FIELD;
    private final int height = 16;
    public static final Long octocorevalue = 8L;

    /** The screen indicator */
    private static RelationIndicator screenIndicator = new RelationIndicator(RelationIndicator.SCREEN_INDICATOR_CLASS);

    /** The device indicator */
    private static RelationIndicator deviceIndicator = new RelationIndicator(RelationIndicator.DEVICE_INDICATOR_CLASS);

    /**
     * @param machine
     * @param view
     */
    public MachineCell(ComputerModelData machine, MapView view)
    {
        super();

        this.disableTextSelection(true);

        this.view = view;
        this.machine = machine;
        this.setInventoryStatusColors(machine);
        this.visibleLabel = this.view.areLabelsVisible();
        this.labelToDisplay = this.view.getLabelToDisplay();

        this.baseStyleWithoutLabel = this.createNoLabelAttribute();

        this.initText();
        this.setElement(DOM.createDiv());
        this.setElContent();
        this.setElStyles();
        this.setListeners();
        this.setMenu();

        this.onAttach();
        this.updateIndicators();
    }

    private void setListeners()
    {
        MachineDropTarget machineTarget = new MachineDropTarget(this);
        machineTarget.setAllowSelfAsSource(false);
        machineTarget.setOperation(Operation.COPY);
    }

    public static Element getScreenIndicatorElement()
    {
        return screenIndicator.getElement();
    }

    public static String getScreenIndicatorElementAsHTMLString()
    {
        return screenIndicator.getElement().getString();
    }

    public static Element getDeviceIndicatorElement()
    {
        return deviceIndicator.getElement();
    }

    public static String getDeviceIndicatorElementAsHTMLString()
    {
        return deviceIndicator.getElement().getString();
    }

    private String createNoLabelAttribute()
    {
        String css = "height: " + this.height + "px; ";
        css += "width: " + this.height + "px; ";
        css += "background-color: " + this.backgroundColor + "; ";
        css += "color: " + this.textColor + "; ";
        return css;
    }

    private String createWithLabelAttribute()
    {
        String css = "height: " + this.height + "px; ";
        if (this.labelToDisplay != null)
            css += "width: " + (TextMetrics.get().getWidth(this.displayedText) + this.height) + "px; ";
        else
            css += "width: " + this.height + "px; ";
        css += "background-color: " + this.backgroundColor + "; ";
        css += "color: " + this.textColor + "; ";
        css += "text-align : center; ";

        return css;
    }

    /**
     * @return true if the label is displayed on the computer.
     */
    public boolean isVisibleLabel()
    {
        return visibleLabel;
    }

    /**
     * Sets the visibility of the label.
     * 
     * @param visibleLabel
     */
    public void setVisibleLabel(boolean visibleLabel)
    {
        if (this.visibleLabel == visibleLabel)
            return;

        this.visibleLabel = visibleLabel;
        this.updateDisplay();
    }

    private void updateStyle()
    {
        String css = this.visibleLabel ? createWithLabelAttribute() : baseStyleWithoutLabel;
        getElement().setAttribute("style", addMachinePosition(css));
    }

    private String addMachinePosition(String css)
    {
        css += "left: " + machine.getXPosition() + "px; ";
        css += "top: " + machine.getYPosition() + "px; ";

        return css;
    }

    private void updateText()
    {
        if (this.visibleLabel && this.machine.get(this.labelToDisplay) != null)
            this.displayedText = this.machine.get(this.labelToDisplay).toString();
        else
            this.displayedText = "";
        updateIndicators();
    }

    private void initText()
    {
        if (this.visibleLabel && this.machine.get(this.labelToDisplay) != null)
            this.displayedText = this.machine.get(this.labelToDisplay).toString();
        else
            this.displayedText = "";
        setText(this.displayedText);
    }

    /**
     * Sets the "selected" style on computer.
     */
    public void setSelected()
    {
        this.getElement().addClassName(MACHINE_SELECTED_CLASS);
        this.getElement().removeClassName(MACHINE_DEFAULT_CLASS);
    }

    /**
     * Removed the "selected" style from computer.
     */
    public void removeSelected()
    {
        this.getElement().removeClassName(MACHINE_SELECTED_CLASS);
        this.getElement().addClassName(MACHINE_DEFAULT_CLASS);
    }

    private void setElStyles()
    {
        this.updateStyle();
        getElement().addClassName(MACHINE_CLASS);
        getElement().addClassName(MACHINE_DEFAULT_CLASS);
    }

    private void setElContent()
    {
        updateIndicators();
        DOM.setEventListener(getElement(), this);
    }

    public void updateIndicators()
    {
        this.htmlElement = this.displayedText;
        boolean hasIndicator = false;
        if (!machine.getScreenIDs().isEmpty())
        {
            this.htmlElement += MachineCell.getScreenIndicatorElementAsHTMLString();
            hasIndicator = true;
        }
        if (!machine.getDeviceIDs().isEmpty())
        {
            this.htmlElement += MachineCell.getDeviceIndicatorElementAsHTMLString();
            hasIndicator = true;
        }

        if (hasIndicator)
            this.el().update(this.htmlElement);
        else
            setText(this.displayedText);
    }

    /**
     * @return the associated record.
     */
    public ComputerModelData getMachine()
    {
        return this.machine;
    }

    private void setInventoryStatusColors(ComputerModelData machine)
    {
        EInventoryStatus inventoryStatus = EInventoryStatus.UNKNOWN;
        if (machine.getInventoryStatus() != null)
        {
            inventoryStatus = EInventoryStatus.valueOf(EInventoryStatus.class, machine.getInventoryStatus());
        }
        this.backgroundColor = inventoryStatus.statusColor();
        this.textColor = inventoryStatus.displayColor();
    }

    /**
     * Sets the type of information to display on the computer.
     * 
     * @param label
     */
    public void setDisplayedLabel(String label)
    {
        this.labelToDisplay = label;
        this.updateDisplay();
    }

    /**
     * Updates displayed text and style of the computer.
     */
    private void updateDisplay()
    {
        this.updateText();
        this.updateStyle();
    }

    /**
     * @return true if the computer has 8 cores, false otherwise.
     */
    public boolean isOctocore()
    {
        return octocorevalue.equals(this.machine.getNumberOfCores());
    }

    /**
     * @return true if the computer has no assigned worker, false otherwise.
     */
    public boolean isUnassigned()
    {
        return Util.isEmptyString(this.machine.getPersonName());
    }

    /**
     * @return the room of the computer.
     */
    public String getRoomName()
    {
        return machine.getRoomName();
    }

    /**
     * Sets the "edit relations" menu.
     */
    private void setMenu()
    {
        EasyMenu menu = new EasyMenu();
        menu.add(new EditRelationsMenuItem(this.machine));
        this.setContextMenu(menu);
    }

}

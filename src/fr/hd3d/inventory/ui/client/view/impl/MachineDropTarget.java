package fr.hd3d.inventory.ui.client.view.impl;

import java.util.List;

import com.extjs.gxt.ui.client.dnd.DropTarget;
import com.extjs.gxt.ui.client.event.DNDEvent;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.InventoryItemModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.inventory.ui.client.config.InventoryConfig;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


public class MachineDropTarget extends DropTarget
{

    private final ComputerModelData computer;

    public MachineDropTarget(MachineCell target)
    {
        super(target);
        this.computer = target.getMachine();
    }

    @Override
    protected void onDragDrop(DNDEvent event)
    {
        List<InventoryItemModelData> items = event.getData();
        AppEvent mvcEvent;

        if ((items.get(0) instanceof ScreenModelData))
        {
            mvcEvent = new AppEvent(InventoryEvents.MACHINE_SCREEN_DROPPED);
            mvcEvent.setData(InventoryConfig.SCREEN_DROPPED_EVENT_VAR_NAME, items.get(0));
            mvcEvent.setData(InventoryConfig.SCREEN_DROP_LOCATION_COMPUTER_EVENT_VAR_NAME, this.computer);
        }
        else if ((items.get(0) instanceof DeviceModelData))
        {
            mvcEvent = new AppEvent(InventoryEvents.MACHINE_DEVICE_DROPPED);
            mvcEvent.setData(InventoryConfig.DEVICE_DROPPED_EVENT_VAR_NAME, items.get(0));
            mvcEvent.setData(InventoryConfig.DEVICE_DROP_LOCATION_COMPUTER_EVENT_VAR_NAME, this.computer);
        }
        else
            return;

        EventDispatcher.forwardEvent(mvcEvent);
    }

}

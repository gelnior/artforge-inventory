package fr.hd3d.inventory.ui.client.view.impl;

import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.widget.MessageBox;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.Viewport;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;

import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EventBaseListener;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.mainview.MainView;
import fr.hd3d.inventory.ui.client.constant.InventoryConstants;
import fr.hd3d.inventory.ui.client.constant.InventoryMessages;
import fr.hd3d.inventory.ui.client.controller.InventoryController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.error.InventoryErrors;
import fr.hd3d.inventory.ui.client.model.InventoryModel;
import fr.hd3d.inventory.ui.client.view.IInventory;


/**
 * This is the main view that handles configuration, common widgets (error message, loading box...) and navigator
 * integration.
 * 
 * @author HD3D
 */
public class Inventory extends MainView implements IInventory
{
    /** Constant strings to display : dialog messages, button label... */
    public static InventoryConstants CONSTANTS = GWT.create(InventoryConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static InventoryMessages MESSAGES = GWT.create(InventoryMessages.class);
    /** Controller that handles main events. */
    private InventoryController controller;

    /** View that handles explorer stuff. */
    private final ListingView listingView;

    /** View that handles map stuff. */
    private MapView mapView;

    /**
     * Default constructor.
     * 
     * @param explorerView
     *            View that handles explorer stuff.
     */
    public Inventory(ListingView explorerView)
    {
        super(CONSTANTS.Hd3dComputerInventory());
        this.listingView = explorerView;
    }

    /** Initialize all view widgets and registers controller to dispatcher. */
    @Override
    public void init()
    {
        this.controller = new InventoryController(this, new InventoryModel());
        this.controller.addChild(listingView.getController());

        EventDispatcher dispatcher = EventDispatcher.get();
        dispatcher.addController(this.controller);
    }

    /** Insert the UI widgets in the navigator. */
    public void initWidgets()
    {
        TabPanel mainPanel = new TabPanel();
        this.mapView = new MapView();

        final TabItem listingTabItem = new TabItem("Listing");
        listingTabItem.setLayout(new FitLayout());
        listingTabItem.add(listingView.getWidget());

        final TabItem mapTabItem = new TabItem("Map");
        mapView.initWidgets();
        mapTabItem.setLayout(new FitLayout());
        mapTabItem.addListener(Events.Select, new EventBaseListener(InventoryEvents.MAP_TAB_CLICKED));
        mapTabItem.add(mapView);

        mainPanel.add(listingTabItem);
        mainPanel.add(mapTabItem);

        Viewport viewport = new Viewport();
        viewport.setLayout(new FitLayout());
        viewport.add(mainPanel);

        RootPanel.get().add(viewport);

        this.listingView.setUpdateScript(this.controller.getCustomButtons());
    }

    public void setControllers()
    {
        // this.controller.addChild(listingView.getController());
        this.controller.addChild(mapView.getController());
    }

    /** @return The current explorer view. */
    public SheetModelData getCurrentView()
    {
        return this.listingView.getCurrentSheet();
    }

    /**
     * Display error message depending on error code passed in parameter.
     * 
     * @param error
     *            The code of the error to display.
     */
    @Override
    public void displayError(Integer error)
    {
        super.displayError(error);

        switch (error)
        {
            case CommonErrors.RECORD_NOT_ON_SERVER:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.RecordNotOnServer(), null);
                break;
            case InventoryErrors.RELATION_NO_RECORD_SELECTED:
                MessageBox.alert(CONSTANTS.Error(), CONSTANTS.SelectRecord(), null);
                break;
            default:
                ;
        }
    }

}

package fr.hd3d.inventory.ui.client.view.impl;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.util.TextMetrics;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.google.gwt.event.dom.client.MouseDownEvent;
import com.google.gwt.event.dom.client.MouseDownHandler;
import com.google.gwt.event.dom.client.MouseMoveEvent;
import com.google.gwt.event.dom.client.MouseMoveHandler;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.dom.client.MouseUpEvent;
import com.google.gwt.event.dom.client.MouseUpHandler;
import com.google.gwt.event.dom.client.MouseWheelEvent;
import com.google.gwt.event.dom.client.MouseWheelHandler;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.ui.Label;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.logs.Logger;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


public class MachineLabel extends Label implements MouseDownHandler, MouseUpHandler, MouseMoveHandler, MouseOutHandler,
        MouseOverHandler, MouseWheelHandler
{
    private static final String MACHINE_CLASS = "machine";
    private static final String MACHINE_SELECTED_CLASS = "machine-selected";
    private static final String MACHINE_DEFAULT_CLASS = "machine-default";
    private static final String MACHINE_OVER_CLASS = "machine-over";

    private final ComputerModelData machine;

    private boolean isDragging = false;
    private boolean isMoved = false;
    private ContentPanel parent;
    private int startX;
    private int startY;

    public MachineLabel(ComputerModelData machine)
    {
        super();
        this.machine = machine;
        // this.setElement(DOM.createDiv());
        this.setElContent();
        this.setElStyles();
        this.setListeners();

        // this.setFocus(false);

        this.onAttach();
    }

    public void onMouseDown(MouseDownEvent event)
    {
        this.isDragging = true;
        this.startX = event.getClientX();// this.getAbsoluteLeft();
        this.startY = event.getClientY();// this.getAbsoluteTop();

        Logger.log("start" + startX + " - " + startY);
    }

    public void onMouseUp(MouseUpEvent event)
    {
        AppEvent mvcEvent = new AppEvent(null);
        mvcEvent.setData(machine);
        if (!this.isMoved)
        {
            mvcEvent.setType(InventoryEvents.MAP_COMPUTER_CLICKED);
        }
        else
        {
            mvcEvent.setType(InventoryEvents.MAP_COMPUTER_MOVED);
        }
        EventDispatcher.forwardEvent(mvcEvent);

        this.isMoved = false;
        this.isDragging = false;
    }

    public void onMouseMove(MouseMoveEvent event)
    {
        if (this.isDragging)
        {
            this.isMoved = true;

            Logger.log(event.getClientX() + " - " + event.getClientY());
            int dx = event.getClientX() - this.startX;
            int dy = event.getClientY() - this.startY;
            this.startX = event.getClientX();
            this.startY = event.getClientY();

            this.machine.setXPosition(new Long(machine.getXPosition() + dx));
            this.machine.setYPosition(new Long(machine.getYPosition() + dy));

            String css = "width: " + (TextMetrics.get().getWidth(machine.getName()) + 4) + "px; ";
            css += "left: " + machine.getXPosition() + "px; ";
            css += "top: " + machine.getYPosition() + "px; ";

            getElement().setAttribute("style", css);
        }
    }

    public void onMouseOut(MouseOutEvent event)
    {
        this.removeOverStyle();
        if (this.isDragging)
        {
            this.isMoved = true;

            Logger.log(event.getClientX() + " - " + event.getClientY());
            int dx = event.getClientX() - this.startX;// - parent.getAbsoluteLeft();
            int dy = event.getClientY() - this.startY;// - parent.getAbsoluteTop();

            this.startX = event.getClientX();
            this.startY = event.getClientY();

            this.machine.setXPosition(new Long(machine.getXPosition() + dx));
            this.machine.setYPosition(new Long(machine.getYPosition() + dy));

            String css = "width: " + (TextMetrics.get().getWidth(machine.getName()) + 4) + "px; ";
            css += "left: " + machine.getXPosition() + "px; ";
            css += "top: " + machine.getYPosition() + "px; ";

            getElement().setAttribute("style", css);
        }
    }

    public void onMouseOver(MouseOverEvent event)
    {
        this.setOverStyle();
    }

    public void onMouseWheel(MouseWheelEvent event)
    {}

    public void setSelected()
    {
        this.getElement().addClassName(MACHINE_SELECTED_CLASS);
        this.getElement().removeClassName(MACHINE_DEFAULT_CLASS);
    }

    public void removeSelected()
    {
        this.getElement().removeClassName(MACHINE_SELECTED_CLASS);
        this.getElement().addClassName(MACHINE_DEFAULT_CLASS);
    }

    private void setOverStyle()
    {
        this.getElement().addClassName(MACHINE_OVER_CLASS);
        this.getElement().removeClassName(MACHINE_DEFAULT_CLASS);
    }

    private void removeOverStyle()
    {
        this.getElement().removeClassName(MACHINE_OVER_CLASS);
        this.getElement().addClassName(MACHINE_DEFAULT_CLASS);
    }

    private void setElStyles()
    {
        String css = "width: " + (TextMetrics.get().getWidth(machine.getName()) + 4) + "px; ";
        css += "left: " + machine.getXPosition() + "px; ";
        css += "top: " + machine.getYPosition() + "px; ";
        getElement().setAttribute("style", css);
        getElement().addClassName(MACHINE_CLASS);
        getElement().addClassName(MACHINE_DEFAULT_CLASS);
    }

    private void setElContent()
    {
        setText(machine.getName());
        // getElement().setInnerHTML(machine.getName());
        DOM.setEventListener(getElement(), this);
    }

    private void setListeners()
    {
        this.addMouseOutHandler(this);
        this.addMouseOverHandler(this);
        this.addMouseUpHandler(this);
        this.addMouseMoveHandler(this);
        this.addMouseDownHandler(this);
    }

    public void setParentComponent(ContentPanel parent)
    {
        this.parent = parent;
    }

}

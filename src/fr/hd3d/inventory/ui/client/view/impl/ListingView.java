package fr.hd3d.inventory.ui.client.view.impl;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.LayoutRegion;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.util.Margins;
import com.extjs.gxt.ui.client.util.Util;
import com.extjs.gxt.ui.client.widget.Component;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.cookie.FavoriteCookie;
import fr.hd3d.common.ui.client.error.ErrorDispatcher;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.InventoryItemModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.util.CSSUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.SheetCombobox;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.constraint.widget.SheetFilterComboBox;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerPanel;
import fr.hd3d.common.ui.client.widget.explorer.ExplorerRefreshButton;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.AddRelationToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.AddRowToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.AutoSaveButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.ConstraintPanelToggle;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.CsvSheetExportToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.DeleteRowToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.DeleteSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.EditSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.NewSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.PrintButton;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.RefreshSheetToolItem;
import fr.hd3d.common.ui.client.widget.explorer.view.widget.toolbar.SaveSheetDataToolItem;
import fr.hd3d.common.ui.client.widget.identitypanel.IdentityPanel;
import fr.hd3d.common.ui.client.widget.mainview.CustomButtonInfos;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationDialog;
import fr.hd3d.common.ui.client.widget.sheeteditor.SheetEditorDisplayer;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.inventory.ui.client.config.InventoryConfig;
import fr.hd3d.inventory.ui.client.constant.InventoryConstants;
import fr.hd3d.inventory.ui.client.constant.InventoryMessages;
import fr.hd3d.inventory.ui.client.controller.ListingController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.error.InventoryErrors;
import fr.hd3d.inventory.ui.client.model.ListingModel;
import fr.hd3d.inventory.ui.client.view.IListingView;
import fr.hd3d.inventory.ui.client.view.column.InventoryColumnInfoProvider;
import fr.hd3d.inventory.ui.client.view.column.InventoryEditorProvider;
import fr.hd3d.inventory.ui.client.view.widget.ModelDialog;
import fr.hd3d.inventory.ui.client.view.widget.RunUpdateScriptButton;
import fr.hd3d.inventory.ui.client.view.widget.TypeDialog;


/**
 * Listing view contains identity panel, explorer and explorer tool bar.
 * 
 * @author HD3D
 */
public class ListingView implements IListingView
{
    /** Constant strings to display : dialog messages, button label... */
    public static InventoryConstants CONSTANTS = GWT.create(InventoryConstants.class);
    /** Parameterizable messages to display (like the counter label) */
    public static InventoryMessages MESSAGES = GWT.create(InventoryMessages.class);

    /** Controller that handles explorer events. */
    private ListingController controller;
    /** Explorer data, like available sheet list. */
    private ListingModel model = new ListingModel();

    public ListingModel getModel()
    {
        return model;
    }

    /** Main panel : tool bar, explorer grid and identity panel. */
    private final BorderedPanel mainPanel = new BorderedPanel();
    /** Explorer Grid. */
    private final ExplorerPanel explorer = new ExplorerPanel();

    /** Button that pops up relation dialog box when clicked. */
    private final AddRelationToolItem addRelationToolItem = new AddRelationToolItem();
    /** Button that refreshes explorer sheet data when clicked. */
    private final RefreshSheetToolItem refreshSheetToolItem = new RefreshSheetToolItem();
    /** Button that enables auto saving. */
    private final AutoSaveButton autoSaveButton = new AutoSaveButton();
    /** Button that save explorer sheet data sheet when clicked. */
    private final SaveSheetDataToolItem saveSheetToolItem = new SaveSheetDataToolItem();
    /** Button that adds row into the explorer when clicked. */
    private final AddRowToolItem addRowToolItem = new AddRowToolItem();
    /** Button that delete selected rows in the explorer when clicked. */
    private final DeleteRowToolItem deleteRowToolItem = new DeleteRowToolItem();
    /** Button to export explorer data to a CSV file. */
    private final CsvSheetExportToolItem csvToolItem = new CsvSheetExportToolItem();

    /** Button to show types editor. */
    private final ToolBarButton editTypesToolItem = new ToolBarButton(InventoryConfig.EDIT_TYPES_ICON, CONSTANTS
            .EditTypes(), InventoryEvents.EDIT_TYPES_CLICKED);
    /** Button to show models editor. */
    private final ToolBarButton editModelsToolItem = new ToolBarButton(InventoryConfig.EDIT_MODELS_ICON, CONSTANTS
            .EditModels(), InventoryEvents.EDIT_MODELS_CLICKED);

    /** Button that retrives data from database. */
    private final RunUpdateScriptButton updateDataButton = new RunUpdateScriptButton();
    /** Button that opens new sheet dialog box. */
    private final Component newSheetToolItem = new NewSheetToolItem();
    /** Button that opens edit sheet dialog box. */
    private final EditSheetToolItem editSheetToolItem = new EditSheetToolItem();
    /** Button that deletes current sheet when clicked. */
    private final DeleteSheetToolItem deleteSheetToolItem = new DeleteSheetToolItem();
    /** Displays an animated GIF showing that application is saving data. */
    private final Status savingToolItem = new Status();

    /** Toolbar separator that can be hidden when tool items around are hidden. */
    private final SeparatorToolItem separator = new SeparatorToolItem();

    /** Combo box used to select view to display in explorer. */
    private final SheetCombobox sheetComboBox = new SheetCombobox();
    /** Filter combobox needed to select filter to apply on current sheet. */
    protected SheetFilterComboBox sheetFilterComboBox = new SheetFilterComboBox();

    /** Identity panel */
    private IdentityPanel identityPanel;
    /** East region for the border layout of the main panel. */
    private final BorderLayoutData eastData = new BorderLayoutData(LayoutRegion.EAST);

    /** Dialog used to set relations between softwares, computers and devices. */
    private final FastMap<RelationDialog> relationDialogs = new FastMap<RelationDialog>();
    /** Dialog used to manage computer and device types. */
    private final TypeDialog typesDialog = new TypeDialog();
    /** Dialog used to manage computer, screen and device models. */
    private final ModelDialog modelsDialog = new ModelDialog();

    /**
     * Initialize the view controller.
     * 
     * @see fr.hd3d.inventory.ui.client.view.IListingView#init()
     */
    public void init()
    {
        controller = new ListingController(this, this.model);
        EventDispatcher.get().removeController(this.explorer.getController());
        controller.addChild(this.explorer.getController());
    }

    /**
     * @return Explorer view controller.
     */
    public ListingController getController()
    {
        return this.controller;
    }

    /**
     * @return Main panel for explorer.
     */
    public ContentPanel getWidget()
    {
        return this.mainPanel;
    }

    /**
     * Initialize all widgets : explorer, toolbar, identity panel, sheet editor window.
     */
    public void initWidget()
    {
        this.mainPanel.hide();
        this.setLayout();
        this.setExplorer();
        this.setIdentityPanel();
        this.setToolbar();

        this.mainPanel.show();
    }

    /** Sets the model that handles sheets and identity data. */
    public void setModel(ListingModel model)
    {
        this.model = model;
    }

    /** Select the first sheet in the sheet combo box. */
    public void selectFirstSheet()
    {
        if (this.model.getSheetStore().getCount() > 0)
        {
            this.sheetComboBox.select(0);
        }
    }

    /**
     * Select sheet in the sheet combo box.
     * 
     * @see fr.hd3d.inventory.ui.client.view.IListingView#selectSheet(fr.hd3d.common.ui.client.widget.explorer.model.modeldata.SheetModelData)
     */
    public void selectSheet(SheetModelData sheet)
    {
        this.sheetComboBox.setValue(sheet);
    }

    /** Display sheet editor window in */
    public void showSheetEditorWindow(boolean isNewSheet)
    {
        if (isNewSheet)
        {
            SheetEditorDisplayer.showEditor();
        }
        else
        {
            SheetEditorDisplayer.showEditor(this.explorer.getCurrentSheet());
        }
    }

    /** Get the sheet currently used by the explorer. */
    public SheetModelData getCurrentSheet()
    {
        return this.sheetComboBox.getValue();
    }

    /**
     * Show dialog box for setting relations between items.
     * 
     * @see fr.hd3d.inventory.ui.client.view.IListingView#showRelationDialog(java.lang.Long)
     */
    public void showRelationDialog()
    {
        if (this.getSelectedModels().size() == 0)
        {
            ErrorDispatcher.sendError(InventoryErrors.RELATION_NO_RECORD_SELECTED);
        }
        else
        {
            RelationDialog relationDialog = relationDialogs.get(this.model.getSimpleClassName());
            if (relationDialog == null)
            {
                relationDialog = new RelationDialog();
                relationDialogs.put(this.model.getSimpleClassName(), relationDialog);
            }

            relationDialog.show(this.getSelectedModels());
        }
    }

    /**
     * Initialize identity panel fields.
     */
    public void buildIdentityPanel(String simpleClassName)
    {
        this.identityPanel.buildFields(simpleClassName);
    }

    /**
     * Clear identity panel fields.
     */
    public void clearAllIdentityPanel()
    {
        this.identityPanel.clearAll();
    }

    public void clearIdentityPanel()
    {
        this.identityPanel.clearData();
    }

    public void refreshIdentityPanel(Long id)
    {
        this.identityPanel.refreshIdentityData(id);
    }

    /**
     * Build sheet editor window and sets authorized entities : computer, device and software.
     */
    public void createSheetEditorWindow(List<EntityModelData> entities)
    {
        SheetEditorDisplayer.createSheetEditorWindow(entities);
        SheetEditorDisplayer.hideValidationButton();
    }

    /**
     * Get selected records.
     */
    public List<Hd3dModelData> getSelectedModels()
    {
        return this.explorer.getSelectedModels();
    }

    /**
     * Clear all selected elements.
     */
    public void resetSelection()
    {
        this.explorer.resetSelection();
    }

    /**
     * Delete row from explorer.
     */
    public void deleteRow(Hd3dModelData model)
    {
        this.explorer.deleteRow(model);
    }

    /**
     * Insert Row in explorer.
     */
    public void insert(Hd3dModelData model)
    {
        this.explorer.insert(model, 0);
    }

    /**
     * Show saving indicator.
     */
    public void showSaving()
    {
        this.savingToolItem.show();
    }

    /**
     * Hide saving indicator.
     */
    public void hideSaving()
    {
        this.savingToolItem.hide();
    }

    /**
     * Disable save button.
     */
    public void disableSave()
    {
        this.saveSheetToolItem.setEnabled(false);
    }

    /**
     * Enable save button.
     */
    public void enableSave()
    {
        this.saveSheetToolItem.setEnabled(true);
    }

    /**
     * @see fr.hd3d.inventory.ui.client.view.IListingView#enableModifiers(boolean)
     */
    public void enableModifiers(boolean enabled)
    {
        this.sheetComboBox.setEnabled(enabled);
        this.deleteSheetToolItem.setEnabled(enabled);
        this.addRelationToolItem.setEnabled(enabled);
        this.addRowToolItem.setEnabled(enabled);
        this.deleteRowToolItem.setEnabled(enabled);
        this.refreshSheetToolItem.setEnabled(enabled);
        this.saveSheetToolItem.setEnabled(enabled);
        this.editSheetToolItem.setEnabled(enabled);
    }

    public void refreshIdentityData()
    {
        this.identityPanel.refreshIdentityData();
    }

    /**
     * Show dialog that handles type lists.
     */
    public void showTypesDialog()
    {
        this.typesDialog.show();
    }

    /**
     * Show dialog that handles model lists.
     */
    public void showModelsDialog()
    {
        this.modelsDialog.show();
    }

    public void showDeviceTypeIcon()
    {
        this.editTypesToolItem.show();
    }

    public void setViewComboBoxVisible(boolean visible)
    {
        this.sheetComboBox.setVisible(visible);
    }

    public void enableAddRowToolItem(boolean enabled)
    {
        this.addRowToolItem.setEnabled(enabled);
    }

    public void enableDeleteRowToolItem(boolean enabled)
    {
        this.deleteRowToolItem.setEnabled(enabled);
    }

    public void enableRelationToolItem(boolean enabled)
    {
        this.addRelationToolItem.setEnabled(enabled);
    }

    public void enableSaveSheetToolItem(boolean enabled)
    {
        this.saveSheetToolItem.setEnabled(enabled);
    }

    public void setEditModelsItemVisible(boolean visible)
    {
        this.editModelsToolItem.setVisible(visible);
    }

    public void setEditTypesToolItemVisible(boolean visible)
    {
        this.editTypesToolItem.setVisible(visible);
    }

    public void setSeparatorVisible(boolean visible)
    {
        this.separator.setVisible(visible);
    }

    public void setNewSheetToolItemVisible(boolean visible)
    {
        this.newSheetToolItem.setVisible(visible);
    }

    public void setEditSheetToolItemVisible(boolean visible)
    {
        this.editSheetToolItem.setVisible(visible);
    }

    public void setDeleteSheetToolItemVisible(boolean visible)
    {
        this.deleteSheetToolItem.setVisible(visible);
    }

    /** Sets layout : One button above the counter label. */
    private void setLayout()
    {}

    /**
     * Sets explorer style and layout.
     */
    private void setExplorer()
    {
        this.explorer.setColumnInfoProvider(new InventoryColumnInfoProvider());
        this.explorer.setConstraintEditorProvider(new InventoryEditorProvider());
        this.explorer.registerFilterComboBox(sheetFilterComboBox);

        this.explorer.setHeaderVisible(false);
        this.explorer.setBorders(false);
        this.explorer.setBodyBorder(true);
        this.explorer.setShadow(false);

        this.mainPanel.addCenter(this.explorer);
    }

    /**
     * Sets the identity panel on the right.
     */
    private void setIdentityPanel()
    {
        this.identityPanel = new IdentityPanel();

        eastData.setFloatable(true);
        eastData.setCollapsible(true);
        eastData.setSplit(true);
        eastData.setMargins(new Margins(5, 5, 5, 0));
        eastData.setMinSize(250);

        this.mainPanel.addEast(this.identityPanel);
    }

    /**
     * Set tool bar for all basic actions on sheets and explorer : add new line, delete line, add relations, refresh,
     * save, edit sheet, delete sheet, new sheet and change sheet.
     */
    private void setToolbar()
    {
        this.sheetComboBox.setStore(this.model.getSheetStore());
        this.sheetComboBox.setProject(null);

        this.mainPanel.setToolBar();
        this.mainPanel.addToToolBar(sheetComboBox);

        ConstraintPanelToggle filterPanelButton = new ConstraintPanelToggle();
        sheetFilterComboBox.registerToggleButton(filterPanelButton);
        CSSUtils.set1pxBorder(filterPanelButton, "transparent");

        this.mainPanel.addToToolBar(filterPanelButton);
        this.mainPanel.addToToolBar(sheetFilterComboBox);
        this.mainPanel.addToToolBar(new SeparatorToolItem());

        this.mainPanel.addToToolBar(addRowToolItem);
        this.mainPanel.addToToolBar(deleteRowToolItem);
        this.mainPanel.addToToolBar(addRelationToolItem);

        this.mainPanel.addToToolBar(separator);
        this.mainPanel.addToToolBar(editModelsToolItem);
        this.mainPanel.addToToolBar(editTypesToolItem);
        this.mainPanel.addToToolBar(new SeparatorToolItem());

        this.mainPanel.addToToolBar(csvToolItem);
        this.mainPanel.addToToolBar(new PrintButton());

        this.mainPanel.addToToolBar(new SeparatorToolItem());
        this.mainPanel.addToToolBar(new ExplorerRefreshButton(this.explorer));
        this.mainPanel.addToToolBar(autoSaveButton);
        this.mainPanel.addToToolBar(saveSheetToolItem);

        this.savingToolItem.setBusy(CONSTANTS.Saving());
        this.savingToolItem.hide();
        this.mainPanel.addToToolBar(savingToolItem);

        this.mainPanel.addToToolBar(new FillToolItem());
        this.mainPanel.addToToolBar(updateDataButton);
        this.mainPanel.addToToolBar(new SeparatorToolItem());
        this.mainPanel.addToToolBar(newSheetToolItem);
        this.mainPanel.addToToolBar(editSheetToolItem);
        this.mainPanel.addToToolBar(deleteSheetToolItem);

        this.sheetComboBox.addSelectionChangedListener(new EventSelectionChangedListener<SheetModelData>(
                ExplorerEvents.CHANGE_VIEW));
    }

    /**
     * Get value corresponding to key from cookie.
     * 
     * @param key
     *            The key of the value to retrieve.
     */
    public String getFavCookieValue(String key)
    {
        return FavoriteCookie.getFavParameterValue(key);
    }

    /**
     * Store a new value in the favorite cookie with <i>key</i> as key. If a value with this key already exists, it is
     * overwrited.
     * 
     * @param key
     *            The key of the value newly set.
     * @param value
     *            The value to store in cookie.
     */
    public void putFavCookieValue(String key, String value)
    {
        FavoriteCookie.putFavValue(key, value);
    }

    /**
     * Initialize permissions in editor dialog box.
     */
    public void setEditorPermissions()
    {
        this.modelsDialog.initPermissions();
        this.typesDialog.initPermissions();
    }

    public void toggleAutoSave()
    {
        this.autoSaveButton.toggle();
    }

    public void enableSheetComboBox(boolean enabled)
    {
        this.sheetComboBox.setEnabled(enabled);
    }

    public void setSheetEntities(List<EntityModelData> entities)
    {
        for (EntityModelData entity : entities)
            this.sheetComboBox.addAvailableEntity(entity.getClassName());
    }

    public void setExplorerSheet(SheetModelData sheet)
    {
        this.explorer.setCurrentSheet(sheet);
    }

    @Override
    public Store<Hd3dModelData> getStore()
    {
        return this.explorer.getStore();
    }

    @Override
    public void setUpdateScript(List<CustomButtonInfos> customButtonInfos)
    {
        for (CustomButtonInfos info : customButtonInfos)
        {
            if (info.getName() != null && info.getName().equalsIgnoreCase("updateFromDatabaseScript"))
            {
                this.updateDataButton.setScriptName(info.getScript());
                this.updateDataButton.setEnabled(true);
                break;
            }
        }
    }

    @Override
    public void removeEmptyCreatedRows()
    {
        List<Hd3dModelData> createdRecords = new ArrayList<Hd3dModelData>(this.model.getCreatedRecords());

        for (Hd3dModelData model : createdRecords)
        {
            if (model == null || Util.isEmptyString((String) model.get(InventoryItemModelData.NAME_FIELD)))
            {
                this.model.removeCreatedRecord(model);
                this.deleteRow(model);
            }
        }
    }
}

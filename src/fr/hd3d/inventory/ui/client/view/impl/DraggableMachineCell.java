package fr.hd3d.inventory.ui.client.view.impl;

import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.event.Events;
import com.extjs.gxt.ui.client.event.Listener;
import com.extjs.gxt.ui.client.fx.Draggable;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.google.gwt.user.client.Element;
import com.google.gwt.user.client.Event;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


/**
 * Adds drag behavior to MachineCell.
 * 
 * @author melissa-faucher
 * 
 */
public class DraggableMachineCell extends Draggable
{
    /** The bounded computer */
    private MachineCell cell;

    /** listener on MouseUp event */
    private Listener<ComponentEvent> clickListener;

    private DraggableMachineCell(MachineCell cell)
    {
        super(cell);
        this.cell = cell;
    }

    public DraggableMachineCell(ComputerModelData machine, MapView view)
    {
        this(new MachineCell(machine, view));
        this.setContainer(view.getImagePanel());
        this.setEnabled(view.getEditButton().isPressed());

        this.clickListener = new Listener<ComponentEvent>() {
            public void handleEvent(ComponentEvent ce)
            {
                onClick(ce);
            }
        };

        this.cell.addListener(Events.OnClick, clickListener);
    }

    public void setSelected()
    {
        this.cell.setSelected();
    }

    public void removeSelected()
    {
        this.cell.removeSelected();
    }

    public Element getElement()
    {
        return this.cell.getElement();
    }

    @Override
    protected void startDrag(Event event)
    {
        super.startDrag(event);
    }

    @Override
    protected void afterDrag()
    {
        super.afterDrag();

        ComputerModelData machine = cell.getMachine();

        int dx = this.lastX - this.startBounds.x;
        int dy = this.lastY - this.startBounds.y;
        machine.setXPosition(new Long(machine.getXPosition() + dx));
        machine.setYPosition(new Long(machine.getYPosition() + dy));

        AppEvent mvcEvent = new AppEvent(null);
        mvcEvent.setData(machine);

        mvcEvent.setType(InventoryEvents.MAP_COMPUTER_MOVED);
        EventDispatcher.forwardEvent(mvcEvent);
    }

    protected void onClick(ComponentEvent event)
    {
        ComputerModelData machine = cell.getMachine();
        AppEvent mvcEvent = new AppEvent(null);
        mvcEvent.setData(machine);
        mvcEvent.setType(InventoryEvents.MAP_COMPUTER_CLICKED);
        EventDispatcher.forwardEvent(mvcEvent);
    }

    /**
     * Sets the visibility of the label.
     * 
     * @param pressed
     */
    public void setVisibleLabel(boolean pressed)
    {
        this.cell.setVisibleLabel(pressed);
    }

    /**
     * Sets the label to display.
     * 
     * @param label
     */
    public void setDisplayedLabel(String label)
    {
        this.cell.setDisplayedLabel(label);
    }

    /**
     * @return true if the computer has 8 cores, false otherwise.
     */
    public boolean isOctocore()
    {
        return this.cell.isOctocore();
    }

    /**
     * @return true if the computer has no assigned worker, false otherwise.
     */
    public boolean isUnassigned()
    {
        return this.cell.isUnassigned();
    }

    public void updateIndicators()
    {
        this.cell.updateIndicators();
    }

    public String getRoomName()
    {
        return this.cell.getRoomName();
    }

}

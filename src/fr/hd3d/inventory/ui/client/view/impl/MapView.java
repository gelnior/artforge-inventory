package fr.hd3d.inventory.ui.client.view.impl;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.Style.Scroll;
import com.extjs.gxt.ui.client.Style.SelectionMode;
import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.dnd.GridDragSource;
import com.extjs.gxt.ui.client.dnd.DND.Operation;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.button.ToggleButton;
import com.extjs.gxt.ui.client.widget.grid.Grid;
import com.extjs.gxt.ui.client.widget.layout.BorderLayoutData;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.SeparatorToolItem;
import com.google.gwt.event.dom.client.MouseOutEvent;
import com.google.gwt.event.dom.client.MouseOutHandler;
import com.google.gwt.event.dom.client.MouseOverEvent;
import com.google.gwt.event.dom.client.MouseOverHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.AbstractImagePrototype;
import com.google.gwt.user.client.ui.Image;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.listener.EventSelectionChangedListener;
import fr.hd3d.common.ui.client.modeldata.InfoFieldModelData;
import fr.hd3d.common.ui.client.modeldata.NameModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.reader.StudioMapReader;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.BorderedPanel;
import fr.hd3d.common.ui.client.widget.EasyMenu;
import fr.hd3d.common.ui.client.widget.ModelDataComboBox;
import fr.hd3d.common.ui.client.widget.RefreshMenuItem;
import fr.hd3d.common.ui.client.widget.ToolBarButton;
import fr.hd3d.common.ui.client.widget.identitypanel.IdentityPanel;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationDialog;
import fr.hd3d.inventory.ui.client.controller.MapController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.ComputerStoreBinder;
import fr.hd3d.inventory.ui.client.model.InventoryItemStoreBinder;
import fr.hd3d.inventory.ui.client.model.MapModel;
import fr.hd3d.inventory.ui.client.view.widget.ComputerStoreFilterField;
import fr.hd3d.inventory.ui.client.view.widget.InfoComboBox;
import fr.hd3d.inventory.ui.client.view.widget.InventoryItemStoreFilterField;
import fr.hd3d.inventory.ui.client.view.widget.MapDialog;
import fr.hd3d.inventory.ui.client.view.widget.QuickFilterComboBox;
import fr.hd3d.inventory.ui.client.view.widget.RoomFilterComboBox;


/**
 * The map view displays computers on a map uploaded by user.
 * 
 * @author HD3D
 */
public class MapView extends BorderedPanel
{
    /** Model that handles map data. */
    private final MapModel model = new MapModel();
    /** Controller that handles map events. */
    private final MapController controller = new MapController(this, model);

    /** Combo box for selecting map to display. */
    private final ModelDataComboBox<StudioMapModelData> mapComboBox = new ModelDataComboBox<StudioMapModelData>(
            new StudioMapReader());

    /** Combo box for selecting label to display. */
    private final InfoComboBox labelComboBox = new InfoComboBox(ComputerModelData.SIMPLE_CLASS_NAME);

    /** Combo box for selecting quick filter to apply. */
    private final QuickFilterComboBox quickFilterComboBox = new QuickFilterComboBox();

    /** Combo box for selecting room. */
    private final RoomFilterComboBox roomFilterComboBox = new RoomFilterComboBox();

    /** Button to show map explorer. */
    private final Button mapExplorerButton = new ToolBarButton(Hd3dImages.getEditIcon(), "EditMaps",
            InventoryEvents.MAP_SHOW_EXPLORER_CLICKED);

    /** Dialog containing map explorer and displayed on map explorer button click. */
    private MapDialog mapDialog;

    private final ContentPanel imagePanel = new ContentPanel();
    private final Image map = new Image();
    private final FastMap<DraggableMachineCell> cells = new FastMap<DraggableMachineCell>();

    private final TabPanel searchPanel = new TabPanel();
    /** Tab displaying the list of computers. */
    private final ContentPanel computerSearchPanel = new ContentPanel();
    /** Tab displaying the list of screens. */
    private final ContentPanel screenSearchPanel = new ContentPanel();
    /** Tab displaying the list of devices. */
    private final ContentPanel deviceSearchPanel = new ContentPanel();

    private final ComputerStoreFilterField computerFilter = new ComputerStoreFilterField(this.model
            .getSearchComputerStore());
    private final InventoryItemStoreFilterField<ScreenModelData> screenFilter = new InventoryItemStoreFilterField<ScreenModelData>(
            this.model.getSearchScreenStore());
    private final InventoryItemStoreFilterField<DeviceModelData> deviceFilter = new InventoryItemStoreFilterField<DeviceModelData>(
            this.model.getSearchDeviceStore());

    private final ContentPanel computerStatusCheckBoxes = computerFilter.getStatusCheckBoxes();
    private final ContentPanel screenStatusCheckBoxes = screenFilter.getStatusCheckBoxes();
    private final ContentPanel deviceStatusCheckBoxes = deviceFilter.getStatusCheckBoxes();

    private Grid<ComputerModelData> computerGrid;
    private Grid<ScreenModelData> screenGrid;
    private Grid<DeviceModelData> deviceGrid;

    private final IdentityPanel identityPanel = new IdentityPanel();

    /** Button to remove a cell from map. */
    private final ToolBarButton removeComputerButton = new ToolBarButton(Hd3dImages.getRemoveAllFiltersIcon(),
            "Remove Computer from map", InventoryEvents.MAP_COMPUTER_REMOVED);

    /** Button to enable/disable the moving of cells in the map. */
    private final ToggleButton editButton = new ToggleButton("", Hd3dImages.getLockIcon()) {
        @Override
        protected void onClick(ComponentEvent ce)
        {
            super.onClick(ce);
            EventDispatcher.forwardEvent(InventoryEvents.EDITION_BUTTON_CLICKED);
            AbstractImagePrototype icon = this.isPressed() ? Hd3dImages.getUnlockIcon() : Hd3dImages.getLockIcon();
            this.setIcon(icon);
        }
    };

    /** Button to show or hide label on cells. */
    private final ToggleButton labelsButton = new ToggleButton("Display labels") {
        @Override
        protected void onClick(ComponentEvent ce)
        {
            super.onClick(ce);
            EventDispatcher.forwardEvent(InventoryEvents.LABEL_BUTTON_CLICKED);
        }
    };

    /** Dialog used to set relations between softwares, computers and devices. */
    private final RelationDialog relationDialog = new RelationDialog();

    public ToggleButton getEditButton()
    {
        return editButton;
    }

    /**
     * Default constructor : registers controller.
     */
    public MapView()
    {
        this.mapComboBox.setEmptyText("Select map...");
        this.mapComboBox.addSelectionChangedListener(new EventSelectionChangedListener<StudioMapModelData>(
                InventoryEvents.MAP_CHANGED));
        this.mapComboBox.setContextMenu();

        this.labelComboBox.addSelectionChangedListener(new EventSelectionChangedListener<InfoFieldModelData>(
                InventoryEvents.DISPLAYED_LABEL_CHANGED));

        this.editButton.setToolTip("Enables the deplacement of ressources");
    }

    /**
     * @return the combo box selecting map to display
     */
    public ModelDataComboBox<StudioMapModelData> getMapComboBox()
    {
        return mapComboBox;
    }

    /**
     * @return the associated controller
     */
    public Controller getController()
    {
        return controller;
    }

    public RoomFilterComboBox getRoomFilterComboBox()
    {
        return roomFilterComboBox;
    }

    /**
     * @return the image panel
     */
    public ContentPanel getImagePanel()
    {
        return imagePanel;
    }

    /**
     * Initialize all views contained by widget.
     */
    public void initWidgets()
    {
        this.setToolBar();

        this.imagePanel.setScrollMode(Scroll.AUTO);
        this.imagePanel.setHeaderVisible(false);
        this.map.setStyleName("map");
        this.map.setVisible(false);
        this.imagePanel.add(map);

        this.addCenter(imagePanel);

        this.setSearchPanel();
        this.setIdentityPanel();
        this.setListeners();
        this.setBinder();
    }

    @Override
    public void setToolBar()
    {
        super.setToolBar();
        this.addToToolBar(this.mapComboBox);
        this.addToToolBar(this.mapExplorerButton);
        this.addToToolBar(new SeparatorToolItem());
        this.addToToolBar(this.removeComputerButton);
        this.addToToolBar(new SeparatorToolItem());
        this.addToToolBar(this.editButton);
        this.addToToolBar(new SeparatorToolItem());
        this.addToToolBar(this.quickFilterComboBox);
        this.addToToolBar(new SeparatorToolItem());
        this.addToToolBar(this.roomFilterComboBox);
        this.addToToolBar(new SeparatorToolItem());
        this.addToToolBar(this.labelComboBox);
        this.addToToolBar(this.labelsButton);
    }

    private void setIdentityPanel()
    {
        this.identityPanel.setFieldsVisible(true);
        this.identityPanel.setHeaderVisible(true);
        this.identityPanel.getHeader().setVisible(true);
        this.identityPanel.buildFields(ComputerModelData.SIMPLE_CLASS_NAME);
        BorderLayoutData data = this.addEast(identityPanel, 300);
        data.setCollapsible(true);
    }

    private void setBinder()
    {
        ComputerStoreBinder computerBinder = new ComputerStoreBinder(this.model.getComputerStore(), this);
        computerBinder.init();

        InventoryItemStoreBinder<ScreenModelData> screenBinder = new InventoryItemStoreBinder<ScreenModelData>(
                this.model.getScreenStore(), this);
        screenBinder.init();

        InventoryItemStoreBinder<DeviceModelData> deviceBinder = new InventoryItemStoreBinder<DeviceModelData>(
                this.model.getDeviceStore(), this);
        deviceBinder.init();
    }

    private HandlerRegistration handlerRegistration = null;

    private void setListeners()
    {
        @SuppressWarnings("unused")
        GridDragSource computerGridSource = new GridDragSource(computerGrid);
        @SuppressWarnings("unused")
        GridDragSource screenGridSource = new GridDragSource(screenGrid);
        @SuppressWarnings("unused")
        GridDragSource deviceGridSource = new GridDragSource(deviceGrid);

        MapDropTarget mapTarget = new MapDropTarget(imagePanel);
        mapTarget.setAllowSelfAsSource(false);
        mapTarget.setOperation(Operation.COPY);

        this.computerGrid.getSelectionModel().addSelectionChangedListener(
                new EventSelectionChangedListener<ComputerModelData>(InventoryEvents.MAP_SEARCH_SELECTED));

        this.screenGrid.getSelectionModel().addSelectionChangedListener(
                new EventSelectionChangedListener<ScreenModelData>(InventoryEvents.SCREEN_SEARCH_SELECTED));

        this.deviceGrid.getSelectionModel().addSelectionChangedListener(
                new EventSelectionChangedListener<DeviceModelData>(InventoryEvents.DEVICE_SEARCH_SELECTED));

        final Event.NativePreviewHandler preventDefaultMouseEvents = new Event.NativePreviewHandler() {
            public void onPreviewNativeEvent(NativePreviewEvent event)
            {
                if (event.getTypeInt() == Event.ONMOUSEDOWN || event.getTypeInt() == Event.ONMOUSEMOVE)
                {
                    event.getNativeEvent().preventDefault();
                }
            }
        };

        addDomHandler(new MouseOverHandler() {
            public void onMouseOver(MouseOverEvent event)
            {
                handlerRegistration = Event.addNativePreviewHandler(preventDefaultMouseEvents);
            }
        }, MouseOverEvent.getType());

        addDomHandler(new MouseOutHandler() {
            public void onMouseOut(MouseOutEvent event)
            {
                if (handlerRegistration != null)
                {
                    handlerRegistration.removeHandler();
                }
            }
        }, MouseOutEvent.getType());

    }

    private void setSearchPanel()
    {
        TabItem computerSearch = this.setComputerSearchTab();
        TabItem screenSearch = this.setScreenSearchTab();
        TabItem deviceSearch = this.setDeviceSearchTab();
        this.searchPanel.add(computerSearch);
        this.searchPanel.add(screenSearch);
        this.searchPanel.add(deviceSearch);
        this.searchPanel.setTabScroll(true);

        BorderLayoutData data = this.addWest(this.searchPanel);
        data.setCollapsible(true);
        data.setSplit(true);
    }

    private TabItem setComputerSearchTab()
    {
        TabItem computerSearch = new TabItem("Computers");
        computerSearch.setLayout(new FitLayout());
        initComputerSearchPanel();
        computerSearch.add(this.computerSearchPanel);
        return computerSearch;
    }

    private TabItem setScreenSearchTab()
    {
        TabItem screenSearch = new TabItem("Screens");
        screenSearch.setLayout(new FitLayout());
        initScreenSearchPanel();
        screenSearch.add(this.screenSearchPanel);
        return screenSearch;
    }

    private TabItem setDeviceSearchTab()
    {
        TabItem deviceSearch = new TabItem("Devices");
        deviceSearch.setLayout(new FitLayout());
        initDeviceSearchPanel();
        deviceSearch.add(this.deviceSearchPanel);
        return deviceSearch;
    }

    /**
     * Initialize the computer search panel
     */
    private void initComputerSearchPanel()
    {
        this.computerGrid = new Grid<ComputerModelData>(this.model.getSearchComputerStore(), GridUtils
                .getNameColumnModel());
        this.computerGrid.setAutoExpandColumn(NameModelData.NAME_FIELD);
        this.computerGrid.setHideHeaders(true);
        this.computerGrid.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        EasyMenu menu = new EasyMenu();
        menu.add(new RefreshMenuItem<ComputerModelData>(this.model.getSearchComputerStore()));
        this.computerGrid.setContextMenu(menu);

        this.computerSearchPanel.setHeading("Computer Search");
        this.computerSearchPanel.setTopComponent(computerFilter);
        this.computerSearchPanel.setLayout(new FitLayout());
        this.computerSearchPanel.add(this.computerGrid);
        this.computerSearchPanel.setBottomComponent(this.computerStatusCheckBoxes);
    }

    /**
     * Initialize the screen search panel.
     */
    private void initScreenSearchPanel()
    {
        this.screenGrid = new Grid<ScreenModelData>(this.model.getSearchScreenStore(), GridUtils.getNameColumnModel());
        this.screenGrid.setAutoExpandColumn(NameModelData.NAME_FIELD);
        this.screenGrid.setHideHeaders(true);
        this.screenGrid.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        EasyMenu menu = new EasyMenu();
        menu.add(new RefreshMenuItem<ScreenModelData>(this.model.getSearchScreenStore()));
        this.screenGrid.setContextMenu(menu);

        this.screenSearchPanel.setHeading("Screen Search");
        this.screenSearchPanel.setTopComponent(screenFilter);
        this.screenSearchPanel.setLayout(new FitLayout());
        this.screenSearchPanel.add(this.screenGrid);
        this.screenSearchPanel.setBottomComponent(this.screenStatusCheckBoxes);
    }

    /**
     * Initialize the device search panel.
     */
    private void initDeviceSearchPanel()
    {
        this.deviceGrid = new Grid<DeviceModelData>(this.model.getSearchDeviceStore(), GridUtils.getNameColumnModel());
        this.deviceGrid.setAutoExpandColumn(NameModelData.NAME_FIELD);
        this.deviceGrid.setHideHeaders(true);
        this.deviceGrid.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);

        EasyMenu menu = new EasyMenu();
        menu.add(new RefreshMenuItem<DeviceModelData>(this.model.getSearchDeviceStore()));
        this.deviceGrid.setContextMenu(menu);

        this.deviceSearchPanel.setHeading("Device Search");
        this.deviceSearchPanel.setTopComponent(deviceFilter);
        this.deviceSearchPanel.setLayout(new FitLayout());
        this.deviceSearchPanel.add(this.deviceGrid);
        this.deviceSearchPanel.setBottomComponent(this.deviceStatusCheckBoxes);
    }

    /**
     * Display map explorer.
     */
    public void showExplorer()
    {
        if (mapDialog == null)
        {
            this.mapDialog = new MapDialog();
            this.controller.addChild(mapDialog.getController());
        }

        this.mapDialog.show();
    }

    /**
     * Displays map.
     * 
     * @param imagePath
     *            of the map
     */
    public void displayMap(String imagePath)
    {
        for (DraggableMachineCell cell : cells.values())
        {
            DOM.removeChild(imagePanel.getElement("body"), cell.getElement());
        }
        cells.clear();
        map.setUrl(imagePath);
        map.setVisible(false);
        map.setVisible(true);

    }

    /**
     * Draws a machine on the map if it matches the current filter
     * 
     * @param machine
     */
    public void drawMachine(ComputerModelData machine)
    {
        DraggableMachineCell cell = new DraggableMachineCell(machine, this);

        if (this.computerFilter.isUnassignedOnly() && !cell.isUnassigned())
            return;

        if (this.computerFilter.isOctocoreOnly() && !cell.isOctocore())
            return;

        if (this.computerFilter.getSpecificRoom() != null
                && !this.computerFilter.getSpecificRoom().equals(cell.getRoomName()))
            return;

        DOM.insertChild(imagePanel.getElement("body"), cell.getElement(), 0);

        cells.put(machine.getId().toString(), cell);
    }

    /**
     * @return current map
     */
    public StudioMapModelData getSelectedMap()
    {
        return (StudioMapModelData) CollectionUtils.getFirst(this.mapComboBox.getSelection());
    }

    /**
     * Sets current map
     * 
     * @param map
     */
    public void setSelectedMap(StudioMapModelData map)
    {
        this.mapComboBox.setValue(map);
    }

    /**
     * Returns a map thanks to its id
     * 
     * @param id
     * @return
     */
    public StudioMapModelData getMapById(Long id)
    {
        return this.mapComboBox.getValueById(id);
    }

    /**
     * Sets the current selected computer.
     * 
     * @param computer
     */
    public void selectComputer(ComputerModelData computer)
    {
        ComputerModelData selectedComputer = this.model.getSelectedComputer();
        if (selectedComputer != null)
            this.deselectComputer(selectedComputer);

        DraggableMachineCell cell = this.getMatchingCell(computer);
        if (cell != null)
            cell.setSelected();

        this.model.setSelectedComputer(computer);
        refreshIdentityPanel(computer);

        // // TODO select aussi dans liste de recherche
        // this.computerGrid.getSelectionModel().select(computer, false);

    }

    /**
     * Deselects the selected computer.
     * 
     * @param computer
     */
    public void deselectComputer(ComputerModelData computer)
    {
        DraggableMachineCell cell = this.getMatchingCell(computer);
        if (cell != null)
            cell.removeSelected();
    }

    /**
     * Removes the computer from the map
     * 
     * @param computer
     */
    public void removeComputer(ComputerModelData computer)
    {

        DraggableMachineCell cell = this.getMatchingCell(computer);
        DOM.removeChild(imagePanel.getElement("body"), cell.getElement());

        cells.remove(computer.getId().toString());
    }

    /**
     * Displays infos of the given computer on the identity panel
     * 
     * @param computerModelData
     */
    public void refreshIdentityPanel(ComputerModelData computerModelData)
    {
        this.identityPanel.refreshIdentityData(computerModelData);
        this.identityPanel.refreshLayout();
    }

    /**
     * Refreshes the identity panel
     */
    public void refreshIdentityPanel()
    {
        this.identityPanel.refreshIdentityData();
        this.identityPanel.refreshLayout();
    }

    /**
     * Clears the identity panel
     */
    public void clearIdentityPanel()
    {
        this.identityPanel.clearData();
    }

    /**
     * Removes all computers from the map
     */
    public void removeAllComputers()
    {
        for (DraggableMachineCell cell : cells.values())
        {
            DOM.removeChild(imagePanel.getElement("body"), cell.getElement());
        }
        cells.clear();
    }

    /**
     * @return true if the cell labels are visible, false otherwise.
     */
    public boolean areLabelsVisible()
    {
        return this.labelsButton.isPressed();
    }

    /**
     * Shows/Hides the label on cells.
     */
    public void toggleLabelOnCells()
    {
        for (DraggableMachineCell cell : cells.values())
        {
            cell.setVisibleLabel(labelsButton.isPressed());
        }
    }

    /**
     * Enables/Disable the moving option on cells.
     */
    public void toggleEditionOnCells()
    {
        for (DraggableMachineCell cell : cells.values())
        {
            cell.setEnabled(editButton.isPressed());
        }
    }

    /**
     * Updates the quick filters on computers
     * 
     * @param octocoreOnly
     * @param unassignedOnly
     */
    public void updateComputerFilters(boolean octocoreOnly, boolean unassignedOnly)
    {
        this.computerFilter.setOctocoreOnly(octocoreOnly);
        this.computerFilter.setUnassignedOnly(unassignedOnly);
        this.computerFilter.refreshFilter();
    }

    /**
     * Updates room filter
     * 
     * @param roomName
     */
    public void updateRoomFilter(String roomName)
    {
        this.computerFilter.setSpecificRoom(roomName);
        this.computerFilter.refreshFilter();
    }

    /**
     * Updates the label to display on computers
     */
    public void updateDisplayedLabel()
    {
        for (DraggableMachineCell cell : cells.values())
        {
            cell.setDisplayedLabel(this.getLabelToDisplay());
        }
    }

    /**
     * @return the label to display on computers
     */
    public String getLabelToDisplay()
    {
        if (this.labelComboBox.getValue() == null)
            return ComputerModelData.NAME_FIELD;
        return this.labelComboBox.getValue().getName();
    }

    /**
     * Show dialog box for setting relations between items.
     * 
     * @see fr.hd3d.inventory.ui.client.view.IListingView#showRelationDialog(java.lang.Long)
     */
    public void showRelationDialog(ComputerModelData computer)
    {
        relationDialog.show(computer);
    }

    /**
     * @param screen
     * @return the list of computers linked to the given screen
     */
    public List<ComputerModelData> getComputers(ScreenModelData screen)
    {
        List<ComputerModelData> computers = new ArrayList<ComputerModelData>();

        for (ComputerModelData computer : this.model.getSearchComputerStore().getModels())
        {
            if (screen.getComputerIDs().contains(computer.getId()))
            {
                computers.add(computer);
            }
        }

        return computers;
    }

    /**
     * @param device
     * @return the list of computers linked to the given device
     */
    public List<ComputerModelData> getComputers(DeviceModelData device)
    {
        List<ComputerModelData> computers = new ArrayList<ComputerModelData>();

        for (ComputerModelData computer : this.model.getSearchComputerStore().getModels())
        {
            if (device.getComputerIDs().contains(computer.getId()))
            {
                computers.add(computer);
            }
        }

        return computers;
    }

    /**
     * @param computer
     * @return the draggable cell associated to the given computer
     */
    public DraggableMachineCell getMatchingCell(ComputerModelData computer)
    {
        if (computer == null)
            return null;

        return cells.get(computer.getId().toString());
    }

}

package fr.hd3d.inventory.ui.client.view.impl;

import com.extjs.gxt.ui.client.widget.Text;
import com.google.gwt.user.client.DOM;


/**
 * A small indicator showing the existence of links between a computer and resources.
 * 
 * @author melissa-faucher
 * 
 */
public class RelationIndicator extends Text
{
    private static final String INDICATOR_CLASS = "indicator";
    public static final String SCREEN_INDICATOR_CLASS = "screen-indicator";
    public static final String DEVICE_INDICATOR_CLASS = "device-indicator";

    public RelationIndicator()
    {
        super();

        this.disableTextSelection(true);

        this.setElement(DOM.createDiv());
        getElement().addClassName(INDICATOR_CLASS);
    }

    public RelationIndicator(String classStyle)
    {
        this();
        getElement().addClassName(classStyle);

        if (classStyle.equals(SCREEN_INDICATOR_CLASS))
            setToolTip("This computer is linked to some screens");
        else if (classStyle.equals(DEVICE_INDICATOR_CLASS))
            setToolTip("This computer is linked to some devices");
    }

}

package fr.hd3d.inventory.ui.client.view;

public interface ITypeDialog
{
    public void show();

    public void hideSaving();

    public void disableTabHeaders();

    public void enableTabHeaders();

    public void hide();

    public void enableDeviceTab(boolean enabled);

    public void enableComputerTab(boolean enabled);
}

package fr.hd3d.inventory.ui.client.view;

public interface IModelDialog
{
    public void show();

    public void hideSaving();

    public void disableTabHeaders();

    public void enableTabHeaders();

    public void hide();

    public void enableComputerTab(boolean enabled);

    public void enableDeviceTab(boolean enabled);

    public void enableScreenTab(boolean enabled);
}

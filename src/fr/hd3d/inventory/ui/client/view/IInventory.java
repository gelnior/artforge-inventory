package fr.hd3d.inventory.ui.client.view;

import fr.hd3d.common.ui.client.widget.mainview.IMainView;


/**
 * Interface for main model view.
 * 
 * @author HD3D
 */
public interface IInventory extends IMainView
{
    /** Initialize all view widgets and registers controller to dispatcher. */
    public void init();

    /** Insert the UI widgets in the navigator. */
    public void initWidgets();

    public void setControllers();

}

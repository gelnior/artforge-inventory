package fr.hd3d.inventory.ui.client.view.widget;

import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.util.Util;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.inventory.ui.client.view.impl.MachineCell;


/**
 * A trigger field that filters its bound store (model ComputerModelData) based on the value of the field
 * 
 * @author melissa-faucher
 * 
 */

public class ComputerStoreFilterField extends InventoryItemStoreFilterField<ComputerModelData>
{

    /** Boolean activating the octocore filter */
    private boolean octocoreOnly = false;
    /** Boolean activating the unassigned filter */
    private boolean unassignedOnly = false;
    /** Boolean activating the room filter */
    private String specificRoom = null;

    /**
     * @return true if filter on octocore is activated, false otherwise
     */
    public boolean isOctocoreOnly()
    {
        return octocoreOnly;
    }

    /**
     * @return true if filter on unassigned is activated, false otherwise
     */
    public boolean isUnassignedOnly()
    {
        return unassignedOnly;
    }

    /**
     * Sets filter on octo-core.
     * 
     * @param octocoreOnly
     */
    public void setOctocoreOnly(boolean octocoreOnly)
    {
        this.octocoreOnly = octocoreOnly;
    }

    /**
     * Sets filter on unassigned.
     * 
     * @param unassignedOnly
     */
    public void setUnassignedOnly(boolean unassignedOnly)
    {
        this.unassignedOnly = unassignedOnly;
    }

    /**
     * Sets the only room to display.
     * 
     * @param specificRoom
     */
    public void setSpecificRoom(String specificRoom)
    {
        this.specificRoom = specificRoom;
    }

    /**
     * @return the only room to display (returns null if there is no filter on room).
     */
    public String getSpecificRoom()
    {
        return specificRoom;
    }

    /**
     * Constructor binding the given store to this object.
     * 
     * @param store
     */
    public ComputerStoreFilterField(Store<ComputerModelData> store)
    {
        super(store);
    }

    /**
     * Checks if the given computer matches the filters on name and on status.
     * 
     * @see fr.hd3d.inventory.ui.client.view.widget.InventoryItemStoreFilterField#doSelect(com.extjs.gxt.ui.client.store.Store,
     *      fr.hd3d.common.ui.client.modeldata.inventory.InventoryItemModelData,
     *      fr.hd3d.common.ui.client.modeldata.inventory.InventoryItemModelData, java.lang.String, java.lang.String)
     */
    @Override
    protected boolean doSelect(Store<ComputerModelData> store, ComputerModelData parent, ComputerModelData record,
            String property, String filter)
    {
        if (!super.doSelect(store, parent, record, property, filter))
            return false;

        if (getSpecificRoom() != null && !getSpecificRoom().equals(record.getRoomName()))
            return false;

        if (isOctocoreOnly())
            return MachineCell.octocorevalue.equals(record.getNumberOfCores());
        if (isUnassignedOnly())
            return Util.isEmptyString(record.getPersonName());

        return true;
    }

}

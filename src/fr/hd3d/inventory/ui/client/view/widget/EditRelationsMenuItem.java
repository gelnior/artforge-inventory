package fr.hd3d.inventory.ui.client.view.widget;

import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


/**
 * 
 * A menu items displayed on right-click on a drawn computer, allowing edition of relation between this computer and
 * other Inventory Items.
 * 
 * @author melissa-faucher
 * 
 */
public class EditRelationsMenuItem extends MenuItem
{
    /** The bounded computer */
    private final ComputerModelData computer;

    public EditRelationsMenuItem(ComputerModelData computer)
    {
        super("Edit relations");

        this.computer = computer;

        this.setIcon(Hd3dImages.getConnectIcon());
        this.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                onSelected();
            }
        });
    }

    /**
     * When button is clicked open the relations set box.
     */
    private void onSelected()
    {
        AppEvent mvcEvent = new AppEvent(InventoryEvents.MACHINE_EDIT_LINKS);
        mvcEvent.setData(this.computer);

        EventDispatcher.forwardEvent(mvcEvent);
    }
}

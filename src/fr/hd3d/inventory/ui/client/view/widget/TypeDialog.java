package fr.hd3d.inventory.ui.client.view.widget;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ComputerTypeReader;
import fr.hd3d.common.ui.client.modeldata.reader.DeviceTypeReader;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;
import fr.hd3d.common.ui.client.widget.tabdialog.TabsDialog;
import fr.hd3d.inventory.ui.client.constant.InventoryConstants;
import fr.hd3d.inventory.ui.client.controller.TypeDialogController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.view.ITypeDialog;


/**
 * Dialog used to manage device type and computer type.
 * 
 * @author HD3D
 */
public class TypeDialog extends TabsDialog implements ITypeDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static InventoryConstants CONSTANTS = GWT.create(InventoryConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Controller that handles type dialog events. */
    private final TypeDialogController controller;

    /** Folder containing all tabs (computer and device). */
    private final TabPanel folder = new TabPanel();
    /** Status label to show loading informations */
    protected Status status;

    /** Tab containing device type editor. */
    private TabItem deviceTypeTab;
    /** Tab containing computer type editor. */
    private TabItem computerTypeTab;

    /** Device type editor. */
    private final SimpleExplorerPanel<DeviceTypeModelData> devicePanel = new SimpleExplorerPanel<DeviceTypeModelData>(
            new DeviceTypeReader());
    /** Computer editor. */
    private final SimpleExplorerPanel<ComputerTypeModelData> computerPanel = new SimpleExplorerPanel<ComputerTypeModelData>(
            new ComputerTypeReader());

    /** Closing button. */
    protected Button closeButton = new Button("Close");

    /**
     * Default constructor.
     */
    public TypeDialog()
    {
        super();
        this.controller = new TypeDialogController(this);
        EventDispatcher.get().addController(controller);

        controller.addChild(devicePanel.getController());
        controller.addChild(computerPanel.getController());

        this.setStyle();
        this.setTabs();
        this.setStatus();
        this.setButtons();

        this.controller.mask();
    }

    /**
     * Show dialog and refreshed editors data.
     */
    @Override
    public void show()
    {
        super.show();
        this.controller.unMask();

        this.devicePanel.refresh();
        this.computerPanel.refresh();
    }

    /**
     * Show status indicator.
     */
    public void showSaving()
    {
        this.status.show();
        this.closeButton.disable();
    }

    /**
     * Hide status indicator.
     */
    public void hideSaving()
    {
        this.status.enable();
        this.closeButton.show();
    }

    /**
     * Set dialog style.
     */
    private void setStyle()
    {
        this.setHeading(CONSTANTS.TypeEditor());
        this.setWidth(338);
        this.setLayout(new FitLayout());
        this.setHeight(400);

        this.setShadow(false);
        this.setClosable(false);
        this.setModal(true);
        this.setResizable(false);

        this.folder.setWidth(450);
        this.folder.setAutoHeight(true);
        this.setBodyBorder(false);
        this.folder.setBorders(false);
        this.add(folder);

        this.setButtons("");
    }

    /**
     * Build tabs : headers and content (editors).
     */
    private void setTabs()
    {
        this.computerTypeTab = this.addTab(CONSTANTS.Device(), devicePanel, devicePanel);
        this.deviceTypeTab = this.addTab(CONSTANTS.Computer(), computerPanel, computerPanel);
    }

    /**
     * Set status indicator (telling if save operations are running or not).
     */
    private void setStatus()
    {
        this.status = new Status();
        this.status.setBusy(COMMON_CONSTANTS.WaitSaving());
        this.status.hide();
        this.getButtonBar().add(status);
        this.getButtonBar().add(new FillToolItem());
    }

    /**
     * Set dialog buttons.
     */
    private void setButtons()
    {
        this.setButtonAlign(HorizontalAlignment.LEFT);

        this.closeButton.addSelectionListener(null);
        this.closeButton.setStyleAttribute("margin-left", "5px");
        this.closeButton.addSelectionListener(new ButtonClickListener(InventoryEvents.TYPE_CLOSE_CLICKED));

        this.getButtonBar().add(closeButton);
    }

    public void enableDeviceTab(boolean enabled)
    {
        deviceTypeTab.setEnabled(enabled);
        devicePanel.setVisible(enabled);
    }

    public void enableComputerTab(boolean enabled)
    {
        computerTypeTab.setEnabled(enabled);
        computerPanel.setVisible(enabled);
    }

    public void initPermissions()
    {
        devicePanel.initPermissions();
        computerPanel.initPermissions();
    }
}

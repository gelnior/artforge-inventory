package fr.hd3d.inventory.ui.client.view.widget;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.widget.Dialog;
import com.extjs.gxt.ui.client.widget.form.TextField;
import com.extjs.gxt.ui.client.widget.grid.CellEditor;
import com.extjs.gxt.ui.client.widget.grid.ColumnConfig;
import com.extjs.gxt.ui.client.widget.grid.ColumnModel;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.reader.StudioMapReader;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.util.GridUtils;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;
import fr.hd3d.inventory.ui.client.constant.InventoryConstants;


/**
 * MapDialog displays a list of available maps and allow the user to create a new map or modify/delete existing ones.
 * For this, MapDialog is based on a simple explorer.
 * 
 * @author HD3D
 */
public class MapDialog extends Dialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static InventoryConstants CONSTANTS = GWT.create(InventoryConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Map panel contains a simple explorer for studio maps. */
    private final SimpleExplorerPanel<StudioMapModelData> mapPanel = new SimpleExplorerPanel<StudioMapModelData>(
            new StudioMapReader(), this.getColumnModel());

    /**
     * Default constructor.
     */
    public MapDialog()
    {
        this.setStyle();

        this.mapPanel.getController().mask();
        this.add(mapPanel);
    }

    /**
     * @return Simple explorer controller.
     */
    public MaskableController getController()
    {
        return this.mapPanel.getController();
    }

    /**
     * Before show, unmask controller and refresh the grid store.
     */
    @Override
    public void show()
    {
        this.mapPanel.getController().unMask();
        this.mapPanel.refresh();
        super.show();
    }

    /**
     * When the dialog is hidden, simple explorer controller is masked.
     * 
     * @see com.extjs.gxt.ui.client.widget.BoxComponent#onHide()
     */
    @Override
    public void onHide()
    {
        super.onHide();
        this.mapPanel.getController().mask();
    }

    /**
     * Set dialog styles and title.
     */
    private void setStyle()
    {
        this.setHeading("Map Editor");
        this.setWidth(500);
        this.setHeight(400);
        this.setLayout(new FitLayout());

        this.setShadow(false);
        this.setClosable(true);
        this.setModal(true);
        this.setResizable(false);

        this.setButtons("");
    }

    /**
     * @return Column Model containing two columns : map name and path name.
     */
    private ColumnModel getColumnModel()
    {
        List<ColumnConfig> columns = new ArrayList<ColumnConfig>();

        ColumnConfig nameColumn = GridUtils.addColumnConfig(columns, StudioMapModelData.NAME_FIELD, CONSTANTS.name(),
                150);
        nameColumn.setEditor(new CellEditor(new TextField<String>()));
        ColumnConfig pathColumn = GridUtils.addColumnConfig(columns, StudioMapModelData.IMAGE_PATH_FIELD, "Path", 330);
        pathColumn.setEditor(new CellEditor(new TextField<String>()));

        return new ColumnModel(columns);
    }
}

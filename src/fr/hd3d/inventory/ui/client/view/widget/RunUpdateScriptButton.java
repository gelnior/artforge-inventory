package fr.hd3d.inventory.ui.client.view.widget;

import fr.hd3d.common.ui.client.images.Hd3dImages;
import fr.hd3d.common.ui.client.widget.RunScriptToolbarButton;


public class RunUpdateScriptButton extends RunScriptToolbarButton
{

    public RunUpdateScriptButton(String scriptName)
    {
        super("", scriptName);
        this.setIcon(Hd3dImages.getRefreshPageIcon());
        this.setToolTip("Udpate ressources from database");
        this.setEnabled(false);
    }

    public RunUpdateScriptButton()
    {
        this("");
    }
}

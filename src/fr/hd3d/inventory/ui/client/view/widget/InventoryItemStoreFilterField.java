package fr.hd3d.inventory.ui.client.view.widget;

import java.util.HashSet;
import java.util.Set;

import com.extjs.gxt.ui.client.core.FastMap;
import com.extjs.gxt.ui.client.event.ComponentEvent;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.widget.ContentPanel;
import com.extjs.gxt.ui.client.widget.form.CheckBox;
import com.extjs.gxt.ui.client.widget.form.StoreFilterField;

import fr.hd3d.common.client.enums.EInventoryStatus;
import fr.hd3d.common.ui.client.modeldata.FieldModelData;
import fr.hd3d.common.ui.client.modeldata.RecordModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.InventoryItemModelData;
import fr.hd3d.inventory.ui.client.view.column.data.MapFactory;


/**
 * A trigger field that filters its bound store based (model InventoryItemModelData) based on the value of the field
 * 
 * @author melissa-faucher
 * 
 * @param <M>
 *            the model type extending InventoryItemModelData
 */
public class InventoryItemStoreFilterField<M extends InventoryItemModelData> extends StoreFilterField<M>
{
    /** Set of inventory status to display */
    protected Set<String> displayedStatus = new HashSet<String>();
    /** Panel containing inventory status check boxes */
    protected ContentPanel statusCheckBoxes = new ContentPanel();
    /** Maps giving the human readable name of every inventory status */
    protected FastMap<FieldModelData> map = MapFactory.getInventoryStatusMap();

    /**
     * @return status check boxes
     */
    public ContentPanel getStatusCheckBoxes()
    {
        return statusCheckBoxes;
    }

    /**
     * Constructor binding the given store to this object.
     * 
     * @param store
     */
    public InventoryItemStoreFilterField(Store<M> store)
    {
        super();
        this.bind(store);
        setStatusFilters();
    }

    /**
     * Refresh the filter
     */
    public void refreshFilter()
    {
        onFilter();
    }

    /**
     * (non-Javadoc)
     * 
     * @see com.extjs.gxt.ui.client.widget.form.StoreFilterField#applyFilters(com.extjs.gxt.ui.client.store.Store)
     */
    @Override
    protected void applyFilters(Store<M> store)
    {
        store.addFilter(filter);
        store.applyFilters(null);
    }

    /**
     * Builds the check boxes matching every inventory status.
     */
    private void setStatusFilters()
    {
        this.statusCheckBoxes.setHeading("Status filter");

        for (EInventoryStatus inventoryStatus : EInventoryStatus.values())
        {
            final String status = inventoryStatus.toString();
            CheckBox statusCheckBox = new CheckBox() {
                @Override
                protected void onClick(ComponentEvent ce)
                {
                    super.onClick(ce);

                    if (this.getValue())
                        displayedStatus.add(status);
                    else
                        displayedStatus.remove(status);

                    onFilter();
                }
            };
            statusCheckBox.setBoxLabel(this.map.get(status).getName());
            statusCheckBox.setValue(true);

            this.displayedStatus.add(status);

            this.statusCheckBoxes.add(statusCheckBox);
        }
    }

    /**
     * Checks if the given record matches the filters on name and on status.
     * 
     * @see com.extjs.gxt.ui.client.widget.form.StoreFilterField#doSelect(com.extjs.gxt.ui.client.store.Store,
     *      com.extjs.gxt.ui.client.data.ModelData, com.extjs.gxt.ui.client.data.ModelData, java.lang.String,
     *      java.lang.String)
     */
    @Override
    protected boolean doSelect(Store<M> store, M parent, M record, String property, String filter)
    {
        String name = record.get(RecordModelData.NAME_FIELD);
        String recordStatus = record.getInventoryStatus();
        name = name.toLowerCase();
        if (name.contains(filter.toLowerCase()) && displayedStatus.contains(recordStatus))
            return true;
        return false;
    }

}

/**
 * 
 */
package fr.hd3d.inventory.ui.client.view.widget;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.extjs.gxt.ui.client.data.DataField;
import com.extjs.gxt.ui.client.data.ModelType;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.widget.form.ComboBox;

import fr.hd3d.common.ui.client.modeldata.InfoFieldModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.service.ServicesModelType;


/**
 * InfoComboBox is used to choose the label to display on computers drawn on the map.
 * 
 * @author melissa-faucher
 * 
 */
public class InfoComboBox extends ComboBox<InfoFieldModelData>
{

    public InfoComboBox(String simpleClassName)
    {
        super();
        setStyles();
        setStore(initStore(simpleClassName));
    }

    /**
     * Initialize the bounded store according to the class name.
     * 
     * @param simpleClassName
     * @return
     */
    private ListStore<InfoFieldModelData> initStore(String simpleClassName)
    {
        ListStore<InfoFieldModelData> store = new ListStore<InfoFieldModelData>();
        Set<String> fields = new HashSet<String>();
        ModelType modeltype = ServicesModelType.getModelType(simpleClassName);
        for (int i = 0; i < modeltype.getFieldCount(); i++)
        {
            if (isAddable(modeltype.getField(i)))
                fields.add(modeltype.getField(i).getName());
        }

        for (String field : fields)
            store.add(new InfoFieldModelData(field, ServicesField.getHumanName(field)));

        return store;
    }

    /**
     * @param field
     * @return true if the label can be displayed, false otherwise
     */
    protected boolean isAddable(DataField field)
    {
        String key = field.getName();

        return !(ExcludedField.isExcluded(key.toLowerCase()) || field.getType() == List.class);
    }

    /**
     * Set combo box behavior.
     */
    protected void setStyles()
    {
        this.setEmptyText("Select label to display...");
        this.setDisplayField(InfoFieldModelData.HUMAN_NAME_FIELD);
        this.setStore(store);

        this.setForceSelection(true);
        this.setTriggerAction(TriggerAction.ALL);
        this.setValidateOnBlur(false);
        this.setTypeAhead(true);
        this.setMinChars(3);
    }
}

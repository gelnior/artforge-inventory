package fr.hd3d.inventory.ui.client.view.widget;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.form.SimpleComboValue;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.inventory.RoomModelData;
import fr.hd3d.common.ui.client.modeldata.reader.RoomReader;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


public class RoomFilterComboBox extends SimpleComboBox<String>
{
    private final ServiceStore<RoomModelData> roomStore = new ServiceStore<RoomModelData>(new RoomReader());

    private final String allRooms = "Show all rooms";

    public RoomFilterComboBox()
    {
        super();
        this.setStyles();
        // loadRoom();
    }

    /**
     * Initializes the filters and their events
     */
    public void updateValues()
    {
        List<String> roomNames = new ArrayList<String>();
        this.removeAll();

        roomStore.reload();

        this.add(allRooms);
        for (RoomModelData room : roomStore.getModels())
            roomNames.add(room.getName());

        Collections.sort(roomNames);
        this.add(roomNames);
    }

    /**
     * Set combo box behavior.
     */
    protected void setStyles()
    {
        this.setTriggerAction(TriggerAction.ALL);
        this.setEmptyText("Select room to display...");

        this.setForceSelection(true);
        this.setEditable(false);
        this.setValidateOnBlur(false);
        this.setTypeAhead(true);
        this.setMinChars(3);
    }

    @Override
    public void onSelect(SimpleComboValue<String> model, int index)
    {
        super.onSelect(model, index);

        if (allRooms.equals(model.getValue()))
        {
            EventDispatcher.forwardEvent(new AppEvent(InventoryEvents.DISPLAY_ALL_ROOMS));
        }
        else
        {
            EventDispatcher.forwardEvent(new AppEvent(InventoryEvents.DISPLAY_ONE_ROOM, model.getValue()));
        }
    }

    /**
     */
    // private void loadRoom()
    // {
    // roomStore.removeAll();
    // BaseCallback callback = new BaseCallback() {
    // @Override
    // public void onSuccess(Request request, Response response)
    // {
    // onLoadRoom(request, response);
    // }
    // };
    // RestRequestHandlerSingleton.getInstance().handleRequest(Method.GET, ServicesURI.ROOMS, null, callback);
    // }
    //
    // private void onLoadRoom(Request request, Response response)
    // {
    // try
    // {
    // String json = response.getEntity().getText();
    // RoomReader reader = new RoomReader();
    // ListLoadResult<RoomModelData> listResult = reader.read(new RoomModelData(), json);
    // List<RoomModelData> result = listResult.getData();
    // roomStore.add(result);
    // }
    // catch (Exception e)
    // {
    // GWT.log("Error occurs while parsing person data for person editor.", e);
    // }
    // }
}

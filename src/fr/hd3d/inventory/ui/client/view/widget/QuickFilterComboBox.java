package fr.hd3d.inventory.ui.client.view.widget;

import java.util.HashMap;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.widget.form.SimpleComboBox;
import com.extjs.gxt.ui.client.widget.form.SimpleComboValue;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


/**
 * Quick Filter combo box propose to select a quick filter to apply to computers displayed on the map.
 * 
 * @author melissa-faucher
 * 
 */
public class QuickFilterComboBox extends SimpleComboBox<String>
{
    /** Map used to link the name of the filter and the event forwarded */
    private HashMap<String, EventType> hashmap = new HashMap<String, EventType>();

    public QuickFilterComboBox()
    {
        super();
        this.setStyles();
        this.initValues();
    }

    /**
     * Initializes the filters and their events
     */
    private void initValues()
    {
        this.addNewFilter("Show all", InventoryEvents.DISPLAY_ALL);
        this.addNewFilter("Octo-core only", InventoryEvents.DISPLAY_ONLY_OCTOCORE);
        this.addNewFilter("Unassigned only", InventoryEvents.DISPLAY_ONLY_UNASSIGNED);
    }

    /**
     * Adds a new filter
     * 
     * @param description
     *            : name of the filter
     * @param event
     *            : event to forward
     */
    public void addNewFilter(String description, EventType event)
    {
        this.add(description);
        this.hashmap.put(description, event);
    }

    /**
     * Set combo box behavior.
     */
    protected void setStyles()
    {
        this.setTriggerAction(TriggerAction.ALL);
        this.setEmptyText("Select filter to apply...");

        this.setForceSelection(true);
        this.setEditable(false);
        this.setValidateOnBlur(false);
        this.setTypeAhead(true);
        this.setMinChars(3);
    }

    @Override
    public void onSelect(SimpleComboValue<String> model, int index)
    {
        super.onSelect(model, index);
        EventDispatcher.forwardEvent(hashmap.get(model.getValue()));
    }
}

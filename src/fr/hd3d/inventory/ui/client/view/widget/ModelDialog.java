package fr.hd3d.inventory.ui.client.view.widget;

import com.extjs.gxt.ui.client.Style.HorizontalAlignment;
import com.extjs.gxt.ui.client.widget.Status;
import com.extjs.gxt.ui.client.widget.TabItem;
import com.extjs.gxt.ui.client.widget.TabPanel;
import com.extjs.gxt.ui.client.widget.button.Button;
import com.extjs.gxt.ui.client.widget.layout.FitLayout;
import com.extjs.gxt.ui.client.widget.toolbar.FillToolItem;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.constant.CommonConstants;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.listener.ButtonClickListener;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ComputerModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.DeviceModelReader;
import fr.hd3d.common.ui.client.modeldata.reader.ScreenModelReader;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerPanel;
import fr.hd3d.common.ui.client.widget.tabdialog.TabsDialog;
import fr.hd3d.inventory.ui.client.constant.InventoryConstants;
import fr.hd3d.inventory.ui.client.controller.ModelDialogController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.view.IModelDialog;


/**
 * Dialog used to manage device type and computer type.
 * 
 * @author HD3D
 */
public class ModelDialog extends TabsDialog implements IModelDialog
{
    /** Constant strings to display : dialog messages, button label... */
    public static InventoryConstants CONSTANTS = GWT.create(InventoryConstants.class);
    /** Constant strings to display : dialog messages, button label... */
    public static CommonConstants COMMON_CONSTANTS = GWT.create(CommonConstants.class);

    /** Controller that handles type dialog events. */
    private final ModelDialogController controller;

    /** Folder containing all tabs (computer and device). */
    private final TabPanel folder = new TabPanel();
    /** Status label to show loading informations */
    protected Status status;

    /** Tab containing device model editor. */
    private TabItem deviceModelTab;
    /** Tab containing computer model editor. */
    private TabItem computerModelTab;
    /** Tab containing computer model editor. */
    private TabItem screenModelTab;

    /** Device model editor. */
    private final SimpleExplorerPanel<DeviceModelModelData> devicePanel = new SimpleExplorerPanel<DeviceModelModelData>(
            new DeviceModelReader());
    /** Computer model editor. */
    private final SimpleExplorerPanel<ScreenModelModelData> screenPanel = new SimpleExplorerPanel<ScreenModelModelData>(
            new ScreenModelReader());
    /** Computer model editor. */
    private final SimpleExplorerPanel<ComputerModelModelData> computerPanel = new SimpleExplorerPanel<ComputerModelModelData>(
            new ComputerModelReader());

    /** Closing button. */
    protected Button closeButton = new Button(CONSTANTS.Close());

    /**
     * Default constructor.
     */
    public ModelDialog()
    {
        super();
        controller = new ModelDialogController(this);
        EventDispatcher.get().addController(controller);

        controller.addChild(devicePanel.getController());
        controller.addChild(screenPanel.getController());
        controller.addChild(computerPanel.getController());

        this.setStyle();
        this.setTabs();
        this.setStatus();
        this.setButtons();

        this.controller.mask();
    }

    /**
     * Show dialog and refreshed editors data.
     */
    @Override
    public void show()
    {
        super.show();
        this.controller.unMask();

        devicePanel.refresh();
        screenPanel.refresh();
        computerPanel.refresh();
    }

    /**
     * Show status indicator.
     */
    public void showSaving()
    {
        status.show();
        closeButton.hide();
    }

    /**
     * Hide status indicator.
     */
    public void hideSaving()
    {
        status.hide();
        closeButton.show();
    }

    /**
     * Set dialog style.
     */
    private void setStyle()
    {
        this.setHeading(CONSTANTS.ModelEditor());
        this.setWidth(338);
        this.setLayout(new FitLayout());
        this.setHeight(400);

        this.setShadow(false);
        this.setClosable(false);
        this.setModal(true);
        this.setResizable(false);

        this.folder.setWidth(450);
        this.folder.setAutoHeight(true);
        this.setBodyBorder(false);
        this.add(folder);

        this.setButtons("");
    }

    /**
     * Build tabs : headers and content (editors).
     */
    private void setTabs()
    {
        deviceModelTab = this.addTab(CONSTANTS.Device(), devicePanel, devicePanel);
        computerModelTab = this.addTab(CONSTANTS.Computer(), computerPanel, computerPanel);
        screenModelTab = this.addTab(CONSTANTS.Screen(), screenPanel, screenPanel);
    }

    /**
     * Set status indicator (telling if save operations are running or not).
     */
    private void setStatus()
    {
        this.status = new Status();
        this.status.setBusy(COMMON_CONSTANTS.WaitSaving());
        this.status.hide();
        this.getButtonBar().add(status);
        this.getButtonBar().add(new FillToolItem());
    }

    /**
     * Set dialog buttons.
     */
    private void setButtons()
    {
        this.setButtonAlign(HorizontalAlignment.LEFT);

        this.closeButton.setStyleAttribute("margin-left", "5px");
        this.closeButton.addSelectionListener(new ButtonClickListener(InventoryEvents.MODEL_CLOSE_CLICKED));

        this.getButtonBar().add(closeButton);
    }

    public void enableComputerTab(boolean enabled)
    {
        computerModelTab.setEnabled(enabled);
        computerPanel.setVisible(enabled);
    }

    public void enableDeviceTab(boolean enabled)
    {
        deviceModelTab.setEnabled(enabled);
        devicePanel.setVisible(enabled);
    }

    public void enableScreenTab(boolean enabled)
    {
        screenModelTab.setEnabled(enabled);
        screenPanel.setVisible(enabled);
    }

    public void initPermissions()
    {
        computerPanel.initPermissions();
        devicePanel.initPermissions();
        screenPanel.initPermissions();
    }
}

package fr.hd3d.inventory.ui.client.controller;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.mvc.controller.MaskableController;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.simpleexplorer.SimpleExplorerEvents;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.view.IModelDialog;


/**
 * Controller that handles events for the type dialog box.
 * 
 * @author HD3D
 */
public class ModelDialogController extends MaskableController
{
    /** The type dialog. */
    final private IModelDialog view;

    /**
     * Default constructor.
     * 
     * @param view
     *            The type dialog.
     */
    public ModelDialogController(IModelDialog view)
    {
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();

        if (isMasked)
        {
            this.forwardToChild(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError();
            this.forwardToChild(event);
        }
        else if (type == CommonEvents.PERMISSION_INITIALIZED)
        {
            this.onPermissionInitialized();
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.MODEL_CLOSE_CLICKED)
        {
            this.onModelCloseClicked();
        }
        else if (type == SimpleExplorerEvents.SAVE_CONFIRMED)
        {
            this.onSimpleSaveClicked();
            this.forwardToChild(event);
        }
        else if (type == SimpleExplorerEvents.SAVE_SUCCESS)
        {
            this.onSimpleSaveSuccess();
            this.forwardToChild(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /**
     * When error occurs, it hide saving status and re-enable tab headers.
     */
    private void onError()
    {
        this.view.hideSaving();
        this.view.enableTabHeaders();
    }

    /**
     * When permissions are initialized, it hides and disables unauthorized elements.
     */
    private void onPermissionInitialized()
    {
        this.view.enableComputerTab(PermissionUtil.hasReadRights(ComputerModelModelData.SIMPLE_CLASS_NAME));
        this.view.enableDeviceTab(PermissionUtil.hasReadRights(DeviceModelModelData.SIMPLE_CLASS_NAME));
        this.view.enableScreenTab(PermissionUtil.hasReadRights(ScreenModelModelData.SIMPLE_CLASS_NAME));
    }

    /**
     * When a simple dialog save button is clicked, it disables all headers.
     */
    private void onSimpleSaveClicked()
    {
        this.view.disableTabHeaders();
    }

    /**
     * When a simple dialog save succeeds, it enable headers.
     */
    private void onSimpleSaveSuccess()
    {
        this.view.enableTabHeaders();
    }

    /**
     * When dialog is closed it hides the view and masks the controller.
     */
    private void onModelCloseClicked()
    {
        this.view.hide();
        this.mask();
    }

    /**
     * Register events that controller can handle.
     */
    private void registerEvents()
    {
        this.registerEventTypes(CommonEvents.ERROR);
        this.registerEventTypes(CommonEvents.PERMISSION_INITIALIZED);
        this.registerEventTypes(InventoryEvents.MODEL_CLOSE_CLICKED);
        this.registerEventTypes(SimpleExplorerEvents.SAVE_CONFIRMED);
        this.registerEventTypes(SimpleExplorerEvents.SAVE_SUCCESS);
    }
}

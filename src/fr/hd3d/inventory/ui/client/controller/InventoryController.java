package fr.hd3d.inventory.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.service.ExcludedField;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.mainview.CustomButtonInfos;
import fr.hd3d.common.ui.client.widget.mainview.MainController;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.InventoryModel;
import fr.hd3d.inventory.ui.client.view.IInventory;


/**
 * Main controller to handle configuration, error and start events.
 * 
 * @author HD3D
 */
public class InventoryController extends MainController
{
    /** Inventory view thats handles widgets. */
    final private IInventory view;
    /** Inventory mode that handles data. */
    private final InventoryModel model;

    /** Default constructor. */
    public InventoryController(IInventory view, InventoryModel model)
    {
        super(model, view);
        this.view = view;
        this.model = model;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        super.handleEvent(event);

        EventType type = event.getType();
        if (type == ExplorerEvents.SAVE_DATA)
        {
            this.model.setListingModified(true);
        }
        else if (type == InventoryEvents.MAP_TAB_CLICKED)
        {
            event.setData(this.model.isListingModified());
            this.model.setListingModified(false);
        }

        this.forwardToChild(event);
    }

    /**
     * @return Custom buttons list set by user.
     */
    public List<CustomButtonInfos> getCustomButtons()
    {
        return this.model.getCustomButtons();
    }

    /** At first sheet loading, when configuration and sheets are initialized, it starts the view initialization. */
    @Override
    protected void onSettingInitialized(AppEvent event)
    {
        ExcludedField.addExcludedField(ComputerModelData.RESOURCEGROUP_IDS_FIELD.toLowerCase());
        ExcludedField.addExcludedField(ComputerModelData.RESOURCEGROUP_NAMES_FIELD.toLowerCase());
        ExcludedField.addExcludedField(ComputerModelData.TAG_IDS_FIELD.toLowerCase());
        ExcludedField.addExcludedField(ComputerModelData.TAG_NAMES_FIELD.toLowerCase());

        ExcludedField.addExcludedField("approvable");
        ExcludedField.addExcludedField("activitiesDuration");
        ExcludedField.addExcludedField("approvalNotes");
        ExcludedField.addExcludedField("boundEntityTaskLink");
        ExcludedField.addExcludedField("boundTasks");
        ExcludedField.addExcludedField("nbTaskOk");
        ExcludedField.addExcludedField("taskActivities");
        ExcludedField.addExcludedField("taskChanges");
        ExcludedField.addExcludedField("taskGroup");
        ExcludedField.addExcludedField("tasksActivitiesDuration");
        ExcludedField.addExcludedField("tasksDurationGap");
        ExcludedField.addExcludedField("tasksEstimation");
        ExcludedField.addExcludedField("tasksStatus");
        ExcludedField.addExcludedField("tasksWorkers");
        ExcludedField.addExcludedField("version");
        ExcludedField.addExcludedField("resourceGroups");
        ExcludedField.addExcludedField("resourceName");
        ExcludedField.addExcludedField("projects");
        ExcludedField.addExcludedField("screens");

        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.ApprovalNotesCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskDurationCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCANCELLEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskCLOSEDCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskOKCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskSTANDBYCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWAITAPPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.NbOfTaskWIPCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TotalNbOfTasksCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskWorkersCollectionQuery");
        ExcludedField.addExcludedField("fr.hd3d.services.resources.collectionquery.TaskWorkersCollectionQuery");

        this.view.initWidgets();
        this.view.hideStartPanel();
        this.view.setControllers();
    }

    /** Register all events the controller can handle. */
    @Override
    protected void registerEvents()
    {
        super.registerEvents();

        this.registerEventTypes(CommonEvents.ERROR);
    }
}

package fr.hd3d.inventory.ui.client.controller;

import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.util.Params;
import com.extjs.gxt.ui.client.widget.Info;

import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.service.callback.GetModelDataCallback;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationEvents;
import fr.hd3d.inventory.ui.client.config.InventoryConfig;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.MapModel;
import fr.hd3d.inventory.ui.client.view.impl.DraggableMachineCell;
import fr.hd3d.inventory.ui.client.view.impl.MapView;


/**
 * MapController handles events raised by MapView.
 * 
 * @author HD3D
 */
public class MapController extends Controller
{
    /** The view displaying computers on maps uploaded by users. */
    private final MapView view;
    /** Model handling data displayed on the map. */
    private final MapModel model;

    /**
     * Default constructor.
     * 
     * @param view
     *            The view displaying computers on maps uploaded by users.
     * @param model
     *            Model handling data displayed on the map.
     */
    public MapController(MapView view, MapModel model)
    {
        this.model = model;
        this.view = view;

        this.registerEvents();
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == CommonEvents.SETTINGS_INITIALIZED)
        {
            this.onSettingsInitialized(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.MAP_SHOW_EXPLORER_CLICKED)
        {
            this.onShowExplorerClicked();
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.MAP_TAB_CLICKED)
        {
            this.onMapTabClicked(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.MAP_CHANGED)
        {
            this.onMapChanged(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.MAP_COMPUTER_DROPPED)
        {
            this.onComputerDropped(event);
        }
        else if (type == InventoryEvents.MACHINE_SCREEN_DROPPED)
        {
            this.onScreenDropped(event);
        }
        else if (type == InventoryEvents.MACHINE_DEVICE_DROPPED)
        {
            this.onDeviceDropped(event);
        }
        else if (type == InventoryEvents.MAP_COMPUTERS_LOADED)
        {
            this.onComputersLoaded();
        }
        else if (type == InventoryEvents.MAP_SCREENS_LOADED)
        {
            ;
        }
        else if (type == InventoryEvents.MAP_DEVICES_LOADED)
        {
            ;
        }
        else if (type == InventoryEvents.MAP_COMPUTER_CLICKED)
        {
            this.onComputerClicked(event);
        }
        else if (type == InventoryEvents.MAP_COMPUTER_MOVED)
        {
            this.onComputerMoved(event);
        }
        else if (type == InventoryEvents.MAP_COMPUTER_REMOVED)
        {
            this.onComputerRemoved(event);
        }
        else if (type == InventoryEvents.MAP_SEARCH_SELECTED)
        {
            this.onSearchSelected(event);
        }
        else if (type == InventoryEvents.EDITION_BUTTON_CLICKED)
        {
            this.onEditionButtonClicked(event);
        }
        else if (type == InventoryEvents.LABEL_BUTTON_CLICKED)
        {
            this.onLabelButtonClicked(event);
        }
        else if (type == InventoryEvents.DISPLAYED_LABEL_CHANGED)
        {
            this.onDisplayedLabelChanged(event);
        }
        else if (type == InventoryEvents.DISPLAY_ONLY_OCTOCORE)
        {
            this.onOnlyOctocore(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.DISPLAY_ONLY_UNASSIGNED)
        {
            this.onOnlyUnassigned(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.DISPLAY_ALL)
        {
            this.onDisplayAll(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.DISPLAY_ALL_ROOMS)
        {
            this.onDisplayAllRooms(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.DISPLAY_ONE_ROOM)
        {
            this.onDisplayOneRoom(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.MAP_SEARCH_SELECTED)
        {
            this.onSearchSelected(event);
        }
        else if (type == InventoryEvents.SCREEN_SEARCH_SELECTED)
        {
            ;
        }
        else if (type == InventoryEvents.DEVICE_SEARCH_SELECTED)
        {
            ;
        }
        else if (type == InventoryEvents.MACHINE_EDIT_LINKS)
        {
            this.onEditLinks(event);
        }
        else if (type == RelationEvents.DIALOG_CLOSED)
        {
            this.afterLinksEdited();
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    private void onEditLinks(AppEvent event)
    {
        ComputerModelData computer = event.getData();
        this.model.setComputerWithEditedLinks(computer);
        this.view.showRelationDialog(computer);
    }

    private void afterLinksEdited()
    {
        this.strongRefresh(this.model.getComputerWithEditedLinks());
    }

    private void onOnlyOctocore(AppEvent event)
    {
        this.view.updateComputerFilters(true, false);
        this.view.removeAllComputers();
        this.onComputersLoaded();
    }

    private void onOnlyUnassigned(AppEvent event)
    {
        this.view.updateComputerFilters(false, true);
        this.view.removeAllComputers();
        this.onComputersLoaded();
    }

    private void onDisplayAll(AppEvent event)
    {
        this.view.updateComputerFilters(false, false);
        this.view.removeAllComputers();
        this.onComputersLoaded();
    }

    private void onDisplayAllRooms(AppEvent event)
    {
        this.view.updateRoomFilter(null);
        this.view.removeAllComputers();
        this.onComputersLoaded();
    }

    private void onDisplayOneRoom(AppEvent event)
    {
        String roomName = event.getData();
        this.view.updateRoomFilter(roomName);
        this.view.removeAllComputers();
        this.onComputersLoaded();
    }

    /**
     * When the map tab is selected, computer and screen stores are reloaded if changes occurred on the listing tab.
     * 
     * @param event
     */
    private void onMapTabClicked(AppEvent event)
    {
        this.view.getRoomFilterComboBox().updateValues();
        Boolean isListingModified = event.getData();
        if (!isListingModified)
            return;

        this.reloadAllStores();

        this.view.refreshIdentityPanel();
    }

    /**
     * When a computer is selected on the list, its associated map is displayed and it becomes the selected computer.
     * 
     * @param event
     */
    private void onSearchSelected(AppEvent event)
    {
        ComputerModelData computer = event.getData();

        if (computer == null)
            return;

        computer.refresh();

        if (this.model.getSelectedComputer() != null)
            this.view.deselectComputer(this.model.getSelectedComputer());

        if (computer.getStudioMapId() != null)
        {
            StudioMapModelData map = this.view.getSelectedMap();
            long computerMapId = computer.getStudioMapId().longValue();

            if (map == null || computerMapId != map.getId().longValue())
            {

                StudioMapModelData computerMap = this.view.getMapById(computerMapId);
                if (computerMap != null)
                {
                    this.model.getComputerStore().removeAll();
                    this.view.setSelectedMap(computerMap);
                    this.model.reloadComputerStore(computerMap);
                    EventDispatcher.forwardEvent(InventoryEvents.MAP_CHANGED, computerMap);
                }
            }
        }

        this.view.selectComputer(computer);
    }

    /**
     * When a computer is removed from map, sets its coordinates to (0,0) and its map to null
     * 
     * @param event
     */
    private void onComputerRemoved(AppEvent event)
    {
        ComputerModelData computer = this.model.getSelectedComputer();
        if (computer != null)
        {
            computer.setXPosition(0L);
            computer.setYPosition(0L);
            computer.setStudioMapID(null);

            computer.save();

            this.model.getComputerStore().remove(computer);
        }
    }

    /**
     * When a computer is clicked on the map, it is selected (or unselected if it already was).
     * 
     * @param event
     */
    private void onComputerClicked(AppEvent event)
    {
        ComputerModelData computer = event.getData();
        selectUnselectComputer(computer);
    }

    private void selectUnselectComputer(ComputerModelData computer)
    {
        ComputerModelData selectedComputer = this.model.getSelectedComputer();

        if (selectedComputer == null || computer.getId().longValue() != selectedComputer.getId().longValue())
        {
            if (selectedComputer != null)
                this.view.deselectComputer(selectedComputer);
            this.view.selectComputer(computer);
        }
        else
        {
            this.view.deselectComputer(computer);
            this.model.setSelectedComputer(null);
        }

        if (this.model.getSelectedComputer() != null)
        {
            this.view.refreshIdentityPanel(this.model.getSelectedComputer());
        }
        else
        {
            this.view.clearIdentityPanel();
        }
    }

    /**
     * When a computer is moved, it is selected and its new position is saved to database.
     */
    private void onComputerMoved(AppEvent event)
    {
        ComputerModelData computer = event.getData();
        computer.save();

        this.view.selectComputer(computer);
    }

    /**
     * When a computer is dropped on the map, it becomes the selected computer. Its position and its map are saved to
     * the database.
     * 
     * @param event
     */
    private void onComputerDropped(AppEvent event)
    {
        ComputerModelData computer = event.getData(InventoryConfig.COMPUTER_DROPPED_EVENT_VAR_NAME);
        computer.setXPosition((Long) event.getData(InventoryConfig.DROP_LOCATION_X_EVENT_VAR_NAME)
                + this.view.getImagePanel().getHScrollPosition());
        computer.setYPosition((Long) event.getData(InventoryConfig.DROP_LOCATION_Y_EVENT_VAR_NAME)
                + this.view.getImagePanel().getVScrollPosition());

        StudioMapModelData map = this.view.getSelectedMap();
        if (map != null)
        {
            ComputerModelData mapComputer = this.model.getComputerStore().findModel(ComputerModelData.ID_FIELD,
                    computer.getId());
            if (mapComputer != null)
                this.model.getComputerStore().remove(mapComputer);
            this.model.getComputerStore().add(computer);

            computer.setStudioMap(map);
            computer.save();

            this.view.selectComputer(computer);
        }
    }

    /**
     * When a screen is dropped on a computer, a link between them is created on the database.
     * 
     * @param event
     */
    private void onScreenDropped(AppEvent event)
    {
        ScreenModelData screen = event.getData(InventoryConfig.SCREEN_DROPPED_EVENT_VAR_NAME);
        ComputerModelData computer = event.getData(InventoryConfig.SCREEN_DROP_LOCATION_COMPUTER_EVENT_VAR_NAME);

        this.model.linkComputerScreen(computer, screen);
        this.view.selectComputer(computer);

        this.updateComputerIndicators(computer);

        Params params = new Params();
        params.add(computer.getName());
        params.add(screen.getName());
        Info.display("Computer/Screen link", "Link added between computer {0} and screen {1}", params);
    }

    /**
     * When a device is dropped on a computer, a link between them is created on the database.
     * 
     * @param event
     */
    private void onDeviceDropped(AppEvent event)
    {
        DeviceModelData device = event.getData(InventoryConfig.DEVICE_DROPPED_EVENT_VAR_NAME);
        ComputerModelData computer = event.getData(InventoryConfig.DEVICE_DROP_LOCATION_COMPUTER_EVENT_VAR_NAME);

        this.model.linkComputerDevice(computer, device);
        this.view.selectComputer(computer);

        this.updateComputerIndicators(computer);

        Params params = new Params();
        params.add(computer.getName());
        params.add(device.getName());
        Info.display("Computer/device link", "Link added between computer {0} and device {1}", params);
    }

    private void updateComputerIndicators(ComputerModelData computer)
    {
        DraggableMachineCell cell = this.view.getMatchingCell(computer);
        cell.updateIndicators();
    }

    /**
     * When map is changed, displays the new map and refresh the associated records.
     * 
     * @param event
     */
    private void onMapChanged(AppEvent event)
    {
        final StudioMapModelData map = event.getData();

        if (map == null)
            return;

        this.view.displayMap(map.getImagePath());

        map.refresh(new GetModelDataCallback(map, null) {
            @Override
            protected void onRecordReturned(List<Hd3dModelData> records)
            {
                Hd3dModelData newRecord = records.get(0);
                this.record.updateFromModelData(newRecord);
                model.reloadComputerStore(map);
            }
        });
    }

    private void onDisplayedLabelChanged(AppEvent event)
    {
        this.view.updateDisplayedLabel();
    }

    private void onLabelButtonClicked(AppEvent event)
    {
        this.view.toggleLabelOnCells();
    }

    private void onEditionButtonClicked(AppEvent event)
    {
        this.view.toggleEditionOnCells();
    }

    /**
     * When settings are initialized, all map widgets are constructed then displayed.
     * 
     * @param event
     *            Settings initialized event.
     */
    private void onSettingsInitialized(AppEvent event)
    {
        this.view.getMapComboBox().doQuery("", true); // loads the maps
        this.view.getRoomFilterComboBox().updateValues();
        this.reloadAllStores();
    }

    /**
     * When show explorer is clicked, the map dialog is displayed.
     */
    private void onShowExplorerClicked()
    {
        this.view.showExplorer();
    }

    /**
     * Draws the loaded computers and set the selected one.
     */
    private void onComputersLoaded()
    {
        final ComputerModelData selectedComputer = this.model.getSelectedComputer();

        for (ComputerModelData computer : this.model.getComputerStore().getModels())
        {
            this.view.drawMachine(computer);
            if (computer.equals(selectedComputer))
                this.view.selectComputer(computer);
        }
    }

    /**
     * Reload all the computers of the current map and refresh the given one.
     * 
     * @param computer
     */
    private void strongRefresh(ComputerModelData computer)
    {
        model.reloadComputerStore(this.view.getSelectedMap());
        computer.refresh();
    }

    /**
     * Refreshes all search stores.
     */
    private void reloadAllStores()
    {
        this.model.getSearchComputerStore().reload();
        this.model.getSearchScreenStore().reload();
        this.model.getSearchDeviceStore().reload();
    }

    /**
     * Register events the controller can handle.
     */
    protected void registerEvents()
    {
        this.registerEventTypes(CommonEvents.SETTINGS_INITIALIZED);
        this.registerEventTypes(InventoryEvents.MAP_TAB_CLICKED);
        this.registerEventTypes(InventoryEvents.MAP_SHOW_EXPLORER_CLICKED);
        this.registerEventTypes(InventoryEvents.MAP_CHANGED);
        this.registerEventTypes(InventoryEvents.MAP_COMPUTER_DROPPED);
        this.registerEventTypes(InventoryEvents.MAP_COMPUTERS_LOADED);
        this.registerEventTypes(InventoryEvents.MAP_SCREENS_LOADED);
        this.registerEventTypes(InventoryEvents.MAP_DEVICES_LOADED);
        this.registerEventTypes(InventoryEvents.MAP_COMPUTER_CLICKED);
        this.registerEventTypes(InventoryEvents.MAP_COMPUTER_MOVED);
        this.registerEventTypes(InventoryEvents.MAP_COMPUTER_REMOVED);
        this.registerEventTypes(InventoryEvents.MAP_SEARCH_SELECTED);
        this.registerEventTypes(InventoryEvents.SCREEN_SEARCH_SELECTED);
        this.registerEventTypes(InventoryEvents.DEVICE_SEARCH_SELECTED);
        this.registerEventTypes(InventoryEvents.DISPLAYED_LABEL_CHANGED);
        this.registerEventTypes(InventoryEvents.LABEL_BUTTON_CLICKED);
        this.registerEventTypes(InventoryEvents.EDITION_BUTTON_CLICKED);
        this.registerEventTypes(InventoryEvents.DISPLAY_ALL);
        this.registerEventTypes(InventoryEvents.DISPLAY_ONLY_OCTOCORE);
        this.registerEventTypes(InventoryEvents.DISPLAY_ONLY_UNASSIGNED);
        this.registerEventTypes(InventoryEvents.DISPLAY_ALL_ROOMS);
        this.registerEventTypes(InventoryEvents.DISPLAY_ONE_ROOM);

        this.registerEventTypes(InventoryEvents.MACHINE_SCREEN_DROPPED);
        this.registerEventTypes(InventoryEvents.MACHINE_DEVICE_DROPPED);
        this.registerEventTypes(InventoryEvents.MACHINE_EDIT_LINKS);

        this.registerEventTypes(RelationEvents.DIALOG_CLOSED);
    }
}

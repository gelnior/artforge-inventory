package fr.hd3d.inventory.ui.client.controller;

import java.util.ArrayList;
import java.util.List;

import com.extjs.gxt.ui.client.event.EventType;
import com.extjs.gxt.ui.client.event.MenuEvent;
import com.extjs.gxt.ui.client.event.SelectionListener;
import com.extjs.gxt.ui.client.mvc.AppEvent;
import com.extjs.gxt.ui.client.mvc.Controller;
import com.extjs.gxt.ui.client.store.ListStore;
import com.extjs.gxt.ui.client.store.Record;
import com.extjs.gxt.ui.client.widget.menu.Menu;
import com.extjs.gxt.ui.client.widget.menu.MenuItem;

import fr.hd3d.common.client.Editor;
import fr.hd3d.common.ui.client.config.CommonConfig;
import fr.hd3d.common.ui.client.error.CommonErrors;
import fr.hd3d.common.ui.client.event.CommonEvents;
import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceTypeModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.LicenseModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.PoolModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.SoftwareModelData;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.PermissionPath;
import fr.hd3d.common.ui.client.service.ServicesField;
import fr.hd3d.common.ui.client.setting.UserSettings;
import fr.hd3d.common.ui.client.userrights.PermissionUtil;
import fr.hd3d.common.ui.client.widget.explorer.config.ExplorerConfig;
import fr.hd3d.common.ui.client.widget.explorer.controller.ExplorerController;
import fr.hd3d.common.ui.client.widget.explorer.event.ExplorerEvents;
import fr.hd3d.common.ui.client.widget.relationeditor.RelationEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.event.SheetEditorEvents;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.modeldata.EntityModelData;
import fr.hd3d.inventory.ui.client.config.InventoryConfig;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.ListingModel;
import fr.hd3d.inventory.ui.client.view.IListingView;


/**
 * Controller that links explorer, identity panel and sheet widgets.
 * 
 * @author HD3D
 */
public class ListingController extends Controller
{
    /** Inventory explorer widget */
    final private IListingView view;
    /** Data model (sheets and identity application. */
    final private ListingModel model;

    /** Sheet identifier. */
    private Long sheetId = null;

    private boolean sheetCreated = false;

    /** True when update operations are all done. */
    private boolean updateDone = true;
    /** True when creation operations are all done. */
    private boolean createDone = false;
    /** True when deletion operations are all done. */
    private boolean deleteDone = false;

    /** Number of creation query transmitted after save action. */
    private int creationQueryNumber = 0;
    /** Number of delete query transmitted after save action. */
    private int deletionQueryNumber = 0;
    private final List<EntityModelData> entities = new ArrayList<EntityModelData>();

    /** Default constructor. */
    public ListingController(IListingView view, ListingModel model)
    {
        this.model = model;
        this.view = view;
        this.registerEvents();

        this.view.initWidget();
    }

    /**
     * @return The last selected sheet id.
     */
    public Long getSheetId()
    {
        return this.sheetId;
    }

    /**
     * Sets the last selected sheetId.
     * 
     * @param sheetId
     *            The id to set.
     */
    public void setSheetId(Long sheetId)
    {
        this.sheetId = sheetId;
    }

    /** @return True when update operations are all done. */
    public boolean isUpdateDone()
    {
        return this.updateDone;
    }

    /** @return True when creation operations are all done. */
    public boolean isCreateDone()
    {
        return this.createDone;
    }

    /** @return True when deletion operations are all done. */
    public boolean isDeleteDone()
    {
        return this.deleteDone;
    }

    /** @return Number of creation query transmitted after save action. */
    public int getCreationQueryNumber()
    {
        return creationQueryNumber;
    }

    /**
     * Set the number of creation query transmitted after save action.
     * 
     * @param creationQueryNumber
     *            number of creation query transmitted after save action.
     */
    public void setCreationQueryNumber(int creationQueryNumber)
    {
        this.creationQueryNumber = creationQueryNumber;
    }

    /** @return Number of delete query transmitted after save action. */
    public int getDeletionQueryNumber()
    {
        return deletionQueryNumber;
    }

    /**
     * Set the number of delete query transmitted after save action.
     * 
     * @param deletionQueryNumber
     *            number of delete query transmitted after save action.
     */
    public void setDeletionQueryNumber(int deletionQueryNumber)
    {
        this.deletionQueryNumber = deletionQueryNumber;
    }

    @Override
    public void handleEvent(AppEvent event)
    {
        EventType type = event.getType();
        if (type == CommonEvents.SETTINGS_INITIALIZED)
        {
            this.onSettingsIntialized();
        }
        else if (type == InventoryEvents.SHEETS_LOAD_FINISHED)
        {
            this.onSheetsLoadFinished(event);
            this.forwardToChild(event);
        }
        else if (type == SheetEditorEvents.NEW_SHEET_CLICKED)
        {
            this.view.showSheetEditorWindow(true);
            this.forwardToChild(event);
        }
        else if (type == SheetEditorEvents.EDIT_SHEET_CLICKED)
        {
            this.view.showSheetEditorWindow(false);
            this.forwardToChild(event);
        }
        else if (type == ExplorerEvents.CHANGE_VIEW)
        {
            this.onChangeView();
            this.forwardToChild(event);
        }
        else if (type == CommonEvents.MODEL_DATA_DELETE_SUCCESS)
        {
            this.onDeleteModelDataSucess(event);
        }
        else if (type == SheetEditorEvents.SHEET_CREATED)
        {
            this.sheetCreated = true;

            this.forwardToChild(event);
        }
        else if (type == SheetEditorEvents.END_SAVE)
        {
            this.onSheetSaved(event);
            this.forwardToChild(event);
        }
        else if (type == SheetEditorEvents.DELETE_SHEET_CLICKED)
        {
            this.onDeleteSheetClicked(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.SHEET_DELETED)
        {
            this.onSheetDeleted(event);
            this.forwardToChild(event);
        }
        else if (type == ExplorerEvents.SELECT_CHANGE)
        {
            this.onSelectChange(event);
            this.forwardToChild(event);
        }
        else if (type == ExplorerEvents.NEW_ROW)
        {
            this.onNewRow();
        }
        else if (type == ExplorerEvents.DELETE_ROW)
        {
            this.onDeleteRow();
        }
        else if (type == ExplorerEvents.SAVE_DATA)
        {
            this.onSaveData();
            this.forwardToChild(event);
        }
        else if (type == ExplorerEvents.BEFORE_SAVE)
        {
            this.onBeforeExplorerSave();
        }
        else if (type == ExplorerEvents.AFTER_SAVE)
        {
            this.onAfterSave(event);
            this.forwardToChild(event);
        }
        else if (type == InventoryEvents.ROW_CREATION_SUCCESS)
        {
            if (this.creationQueryNumber > 0)
            {
                this.creationQueryNumber--;
            }
            this.checkEndSave();
        }
        else if (type == ExplorerEvents.RELATIONS_ADD_CLICK)
        {
            this.onAddRelationClick(event);
        }

        else if (type == RelationEvents.DIALOG_CLOSED)
        {
            this.forwardToChild(event);
            this.onRelationDialogClosed();
        }
        else if (type == InventoryEvents.EDIT_MODELS_CLICKED)
        {
            this.view.showModelsDialog();
        }
        else if (type == InventoryEvents.EDIT_TYPES_CLICKED)
        {
            this.view.showTypesDialog();
        }
        else if (type == InventoryEvents.CREATE_MENU_ASSIGNATION)
        {
            this.onCreateMenuAssignation(event);
        }
        else if (type == InventoryEvents.UPDATE_CELL_WORKER)
        {
            this.onUpdateCellWorker(event);
        }
        else if (type == CommonEvents.ERROR)
        {
            this.onError(event);
        }
        else
        {
            this.forwardToChild(event);
        }
    }

    /* Work around should be rewritten. */
    private void onUpdateCellWorker(AppEvent event)
    {
        PersonModelData worker = event.getData();

        for (Hd3dModelData item : this.view.getSelectedModels())
        {
            Record rd = this.view.getStore().getRecord(item);
            rd.set(Editor.PERSON, worker);
            rd.setDirty(true);
        }

        if (ExplorerController.autoSave)
            EventDispatcher.forwardEvent(ExplorerEvents.SAVE_DATA, Boolean.TRUE);
    }

    /* Work around should be rewritten. */
    private void onCreateMenuAssignation(AppEvent event)
    {
        Menu menu = event.getData(InventoryConfig.PERSON_MENU_EVENT_VAR_NAME);
        MenuItem menuItem = new MenuItem("<i>Unassigned</i>");
        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                AppEvent event = new AppEvent(InventoryEvents.UPDATE_CELL_WORKER);
                EventDispatcher.forwardEvent(event);
            }
        });
        menu.add(menuItem);

        for (final PersonModelData person : this.model.getAssignationStore().getModels())
        {
            String stringValue = person.getFullName();
            menuItem = new MenuItem(stringValue);
            menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
                @Override
                public void componentSelected(MenuEvent ce)
                {
                    AppEvent event = new AppEvent(InventoryEvents.UPDATE_CELL_WORKER, person);
                    EventDispatcher.forwardEvent(event);
                }
            });
            menu.add(menuItem);
        }

        menuItem = new MenuItem("Refresh person list");
        menuItem.addSelectionListener(new SelectionListener<MenuEvent>() {
            @Override
            public void componentSelected(MenuEvent ce)
            {
                model.getAssignationStore().reload();
            }
        });

        menu.add(menuItem);

        menuItem = new MenuItem("<i>To add other person, use </i></br>" + "<i>Human Resources application</i>");
        menu.add(menuItem);
    }

    private void onSheetDeleted(AppEvent event)
    {
        SheetModelData view = this.view.getCurrentSheet();
        this.model.getSheetStore().remove(view);

        this.view.selectFirstSheet();
        SheetModelData sheet = this.view.getCurrentSheet();

        this.view.clearAllIdentityPanel();
        if (sheet != null)
        {
            this.view.buildIdentityPanel(this.view.getCurrentSheet().getBoundClassName());
            this.model.deleteViewFromDb(sheet);
        }

        this.view.enableSheetComboBox(true);
    }

    private void onDeleteSheetClicked(AppEvent event)
    {
        SheetModelData view = this.view.getCurrentSheet();
        if (view != null)
        {
            this.view.enableSheetComboBox(false);
            view.delete(InventoryEvents.SHEET_DELETED);
        }

    }

    private void onSettingsIntialized()
    {
        Boolean isAutoSave = UserSettings.getSettingAsBoolean(ExplorerConfig.SETTING_AUTOSAVE);
        if (isAutoSave != null)
            ExplorerController.autoSave = isAutoSave;
        if (ExplorerController.autoSave)
            this.view.toggleAutoSave();

        this.setToolbarPermission();
        this.buildSheetTools();
        this.view.setEditorPermissions();
        this.model.getSheetStore().reload();
    }

    private void onDeleteModelDataSucess(AppEvent event)
    {
        String className = event.getData(CommonConfig.CLASS_EVENT_VAR_NAME);

        if (SheetModelData.SIMPLE_CLASS_NAME.equals(className))
        {
            this.onDeleteViewFinished();
        }
        else
        {
            if (this.deletionQueryNumber > 0)
            {
                this.deletionQueryNumber--;
            }
            this.checkEndSave();
        }
    }

    private void onRelationDialogClosed()
    {
        if (this.view.getSelectedModels().size() == 1)
        {
            this.view.refreshIdentityData();
        }
    }

    public void setToolbarPermission()
    {
        this.view.setViewComboBoxVisible(PermissionUtil.hasReadRights(SheetModelData.SIMPLE_CLASS_NAME));
        boolean isNewSheetVisible = PermissionUtil.hasCreateRights(SheetModelData.SIMPLE_CLASS_NAME);
        this.view.setNewSheetToolItemVisible(isNewSheetVisible);
        boolean isEditSheetVisible = PermissionUtil.hasUpdateRights(SheetModelData.SIMPLE_CLASS_NAME);
        this.view.setEditSheetToolItemVisible(isEditSheetVisible);
        boolean isDeleteSheetVisible = PermissionUtil.hasDeleteRights(SheetModelData.SIMPLE_CLASS_NAME);
        this.view.setDeleteSheetToolItemVisible(isDeleteSheetVisible);

        boolean isEditModelVisible = PermissionUtil.hasUpdateRights(ComputerModelModelData.SIMPLE_CLASS_NAME)
                || PermissionUtil.hasUpdateRights(DeviceModelModelData.SIMPLE_CLASS_NAME)
                || PermissionUtil.hasUpdateRights(ScreenModelModelData.SIMPLE_CLASS_NAME);
        this.view.setEditModelsItemVisible(isEditModelVisible);

        boolean isEditTypelVisible = PermissionUtil.hasUpdateRights(ComputerTypeModelData.SIMPLE_CLASS_NAME)
                || PermissionUtil.hasUpdateRights(DeviceTypeModelData.SIMPLE_CLASS_NAME);
        this.view.setEditTypesToolItemVisible(isEditTypelVisible);

        boolean isSeparatorVisible = (isNewSheetVisible || isEditSheetVisible || isDeleteSheetVisible)
                && (isEditModelVisible || isEditTypelVisible);
        this.view.setSeparatorVisible(isSeparatorVisible);
    }

    public void setExplorerPermissions()
    {
        String path = PermissionPath.getPath(this.view.getCurrentSheet().getBoundClassName());

        this.view.enableAddRowToolItem(PermissionUtil.hasCreateRightsOnPath(path));
        this.view.enableDeleteRowToolItem(PermissionUtil.hasDeleteRightsOnPath(path));
        this.view.enableRelationToolItem(PermissionUtil.hasUpdateRightsOnPath(path));
        this.view.enableSaveSheetToolItem(PermissionUtil.hasUpdateRightsOnPath(path));
    }

    private void buildSheetTools()
    {
        List<String> classes = new ArrayList<String>();

        if (PermissionUtil.hasReadRights(ComputerModelData.SIMPLE_CLASS_NAME))
        {
            classes.add(ComputerModelData.SIMPLE_CLASS_NAME);
            this.addEntityToSheetEditor(ComputerModelData.SIMPLE_CLASS_NAME);
        }
        if (PermissionUtil.hasReadRights(DeviceModelData.SIMPLE_CLASS_NAME))
        {
            classes.add(DeviceModelData.SIMPLE_CLASS_NAME);
            this.addEntityToSheetEditor(DeviceModelData.SIMPLE_CLASS_NAME);
        }
        if (PermissionUtil.hasReadRights(ScreenModelData.SIMPLE_CLASS_NAME))
        {
            classes.add(ScreenModelData.SIMPLE_CLASS_NAME);
            this.addEntityToSheetEditor(ScreenModelData.SIMPLE_CLASS_NAME);
        }
        if (PermissionUtil.hasReadRights(LicenseModelData.SIMPLE_CLASS_NAME))
        {
            classes.add(LicenseModelData.SIMPLE_CLASS_NAME);
            this.addEntityToSheetEditor(LicenseModelData.SIMPLE_CLASS_NAME);
        }
        if (PermissionUtil.hasReadRights(SoftwareModelData.SIMPLE_CLASS_NAME))
        {
            classes.add(SoftwareModelData.SIMPLE_CLASS_NAME);
            this.addEntityToSheetEditor(SoftwareModelData.SIMPLE_CLASS_NAME);
        }
        if (PermissionUtil.hasReadRights(PoolModelData.SIMPLE_CLASS_NAME))
        {
            classes.add(PoolModelData.SIMPLE_CLASS_NAME);
            this.addEntityToSheetEditor(PoolModelData.SIMPLE_CLASS_NAME);
        }
        this.view.createSheetEditorWindow(this.entities);
        this.view.setSheetEntities(entities);
        this.model.getSheetStore().addInConstraint(SheetModelData.BOUND_CLASS_NAME_FIELD, classes);
    }

    /**
     * When selection changes in explorer grid, it asks the model to retrieve data about the new selection.
     */
    private void onSelectChange(AppEvent event)
    {
        Hd3dModelData record = event.getData(ExplorerEvents.EVENT_VAR_MODELDATA);

        if (this.view.getSelectedModels().size() == 0)
        {
            this.setExplorerPermissions();
        }
        else
        {
            this.updateExplorerPermssion();
        }

        if (record != null && record.getId() != null && this.view.getSelectedModels().size() == 1)
        {
            this.view.refreshIdentityPanel(record.getId());
        }
        else
        {
            this.view.clearIdentityPanel();
        }
    }

    private void updateExplorerPermssion()
    {
        boolean updateRights = true;
        boolean deleteRights = true;

        for (Hd3dModelData model : this.view.getSelectedModels())
        {
            String path = PermissionPath.getPath(this.model.getSimpleClassName()) + ":" + model.getId();

            if (updateRights)
            {
                updateRights = PermissionUtil.hasUpdateRightsOnPath(path);
            }

            if (deleteRights)
            {
                deleteRights = PermissionUtil.hasDeleteRightsOnPath(path);
            }
        }
        this.view.enableRelationToolItem(updateRights);
        this.view.enableDeleteRowToolItem(deleteRights);
    }

    /**
     * Add an authorized entity to the sheet editor.
     * 
     * @param entityName
     *            The authorized entity name to add.
     * @param entityClass
     *            The authorized class name to add.
     */
    private void addEntityToSheetEditor(String entityName)
    {
        EntityModelData entity = new EntityModelData();

        entity.setName(ServicesField.getHumanName(entityName));
        entity.setClassName(entityName);
        this.entities.add(entity);
    }

    /**
     * When sheets are loaded, if there are no previously selected sheet, it selects the first sheet. Else it selects
     * last selected sheet. Then if there is no sheets after loading, it disables all sheet modifiers in the UI. If
     * there are, it enables it.
     */
    private void onSheetsLoadFinished(AppEvent event)
    {
        if (this.model.getSheetStore().getCount() == 0)
        {
            this.view.enableModifiers(false);
        }
        else
        {
            this.autoSelectSheet();

            this.view.enableModifiers(true);
        }
    }

    /**
     * Auto-select first sheet when none has been selected before. Select last selected sheet either.
     */
    private void autoSelectSheet()
    {
        boolean viewSelected = false;
        String stringId = this.view.getFavCookieValue(InventoryConfig.COOKIE_SHEET_VALUE);
        if (this.sheetId == null && stringId != null)
        {
            this.sheetId = Long.parseLong(stringId);
        }

        if (this.sheetId != null)
        {
            for (SheetModelData sheet : this.model.getSheetStore().getModels())
            {
                if (sheet.getId().longValue() == sheetId.longValue())
                {
                    this.view.selectSheet(sheet);
                    viewSelected = true;
                    break;
                }
            }
        }

        if (!viewSelected)
        {
            this.view.selectFirstSheet();
        }
    }

    /**
     * When view changes, selection is reset, explorer record path is updated and records marked as created or deleted
     * are no longer marked.
     */
    private void onChangeView()
    {
        if (this.view.getCurrentSheet() != null)
        {
            String sheetId = this.view.getCurrentSheet().getId().toString();
            this.view.putFavCookieValue("selected_inventory_sheet", sheetId);
        }

        this.model.clearCreatedRecords();
        this.model.clearDeletedRecords();
        this.view.clearAllIdentityPanel();

        if (this.view.getCurrentSheet() != null)
        {
            String boundClassName = this.view.getCurrentSheet().getBoundClassName();

            this.model.setRecordPath(boundClassName);

            this.view.resetSelection();
            this.view.buildIdentityPanel(boundClassName);
            this.setExplorerPermissions();
        }
    }

    /**
     * When delete request response is a success, the sheet is locally removed, the first sheet is selected if at least
     * there is one sheet, else the sheet modifiers widget are disabled.
     * 
     */
    private void onDeleteViewFinished()
    {
        this.model.removeView(this.view.getCurrentSheet());
        this.view.selectFirstSheet();

        if (this.model.getSheetStore().getCount() == 0)
        {
            this.view.enableModifiers(false);
        }
    }

    /**
     * When sheet is saved, sheet combo box is updated by adding it when it is a new sheet, or updating it if sheet
     * already exists. Then this sheet is selected.
     * 
     * @param event
     *            Sheet saved event.
     */
    private void onSheetSaved(AppEvent event)
    {
        SheetModelData sheet = (SheetModelData) event.getData();
        ListStore<SheetModelData> store = this.model.getSheetStore();
        if (store.indexOf(sheet) == -1)
        {
            store.add(sheet);
            this.view.selectSheet(sheet);
        }
        else
        {
            store.update(sheet);
            Record rec = store.getRecord(sheet);
            rec.set(SheetModelData.NAME_FIELD, sheet.getName());
            this.view.setExplorerSheet(sheet);
        }
    }

    /**
     * When save button is clicked and before asking to the model for saving creation and deletion, it counts the number
     * of query to send, disables the save button and shows saving indicator.
     */
    private void onBeforeExplorerSave()
    {
        this.view.disableSave();
        this.view.showSaving();

        this.updateDone = false;
        this.creationQueryNumber = this.model.getCreatedRecords().size();
        this.deletionQueryNumber = this.model.getDeletedRecords().size();
    }

    /**
     * When new row button is clicked, it resets selection, insert a new record in explorer and add it to the
     * "record to create" list.
     */
    private void onNewRow()
    {
        this.view.resetSelection();

        Hd3dModelData model = new Hd3dModelData();
        model.setSimpleClassName(this.model.getSimpleClassName());
        this.view.insert(model);
        this.model.addCreatedRecord(model);
    }

    /**
     * When delete row button is clicked, for each selected row, controller removes it from explorer, removes it from
     * "record to create" list if it is in, and add it to the "record to delete" list.
     */
    private void onDeleteRow()
    {
        List<Hd3dModelData> models = this.view.getSelectedModels();

        for (Hd3dModelData model : models)
        {
            this.model.removeCreatedRecord(model);
            if (model.getId() != null)
            {
                this.model.addDeletedRecord(model);
            }
            this.view.deleteRow(model);
        }

        this.view.resetSelection();
        this.view.clearIdentityPanel();
    }

    /**
     * When save button is clicked or auto-save is active, it asks to the model for saving creation and deletion.
     */
    private void onSaveData()
    {
        this.view.removeEmptyCreatedRows();
        this.model.save();
    }

    /**
     * Check if saving action is ended. When saving is done, saving indicator is hidden and save button is enabled.
     */
    private void checkEndSave()
    {
        this.createDone = this.creationQueryNumber == 0;
        this.deleteDone = this.deletionQueryNumber == 0;

        if (this.updateDone && this.createDone && this.deleteDone)
        {
            this.createDone = false;
            this.deleteDone = false;

            this.view.hideSaving();
            this.view.enableSave();
        }
    }

    /**
     * When update operations are saved, it checks if it is the last one. If yes, it checks if the creation and deletion
     * saving operations are ended too.
     * 
     * @param event
     *            event containing saving advancement.
     */
    private void onAfterSave(AppEvent event)
    {
        this.updateDone = true;

        this.checkEndSave();
    }

    /**
     * When add relations button is clicked, the relation dialog is shown.
     * 
     * @param event
     *            Event telling that add relations button is clicked.
     */
    private void onAddRelationClick(AppEvent event)
    {
        this.view.showRelationDialog();
    }

    /**
     * When error occurs, it warns the user that a problem occurred. If it is a connection error it enables save button.
     * 
     * @param event
     *            Event giving the error type.
     */
    private void onError(AppEvent event)
    {
        Integer error = event.getData(CommonConfig.ERROR_EVENT_VAR_NAME);

        if (error == CommonErrors.NO_SERVICE_CONNECTION || error == CommonErrors.SERVER_ERROR)
        {
            this.view.hideSaving();
            this.view.enableSave();
        }
    }

    /** Register all events the controller can handle. */
    private void registerEvents()
    {
        this.registerEventTypes(CommonEvents.SETTINGS_INITIALIZED);
        this.registerEventTypes(InventoryEvents.SHEETS_LOAD_FINISHED);

        this.registerEventTypes(ExplorerEvents.CHANGE_VIEW);
        this.registerEventTypes(SheetEditorEvents.SHEET_CREATED);
        this.registerEventTypes(ExplorerEvents.SELECT_CHANGE);

        this.registerEventTypes(ExplorerEvents.NEW_ROW);
        this.registerEventTypes(ExplorerEvents.DELETE_ROW);
        this.registerEventTypes(ExplorerEvents.RELATIONS_ADD_CLICK);
        this.registerEventTypes(ExplorerEvents.SAVE_DATA);
        this.registerEventTypes(ExplorerEvents.AFTER_SAVE);
        this.registerEventTypes(InventoryEvents.ROW_CREATION_SUCCESS);
        this.registerEventTypes(InventoryEvents.ROW_DELETION_SUCCESS);

        this.registerEventTypes(InventoryEvents.EDIT_TYPES_CLICKED);
        this.registerEventTypes(InventoryEvents.EDIT_MODELS_CLICKED);

        this.registerEventTypes(SheetEditorEvents.NEW_SHEET_CLICKED);
        this.registerEventTypes(SheetEditorEvents.EDIT_SHEET_CLICKED);
        this.registerEventTypes(SheetEditorEvents.DELETE_SHEET_CLICKED);
        this.registerEventTypes(InventoryEvents.SHEET_DELETED);

        this.registerEventTypes(SheetEditorEvents.END_SAVE);
        this.registerEventTypes(CommonEvents.MODEL_DATA_DELETE_SUCCESS);
        this.registerEventTypes(RelationEvents.DIALOG_CLOSED);

        this.registerEventTypes(InventoryEvents.CREATE_MENU_ASSIGNATION);
        this.registerEventTypes(InventoryEvents.UPDATE_CELL_WORKER);

        this.registerEventTypes(CommonEvents.ERROR);
    }
}

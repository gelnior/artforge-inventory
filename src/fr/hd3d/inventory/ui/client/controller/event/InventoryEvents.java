package fr.hd3d.inventory.ui.client.controller.event;

import com.extjs.gxt.ui.client.event.EventType;


/**
 * Event code list.
 * 
 * @author HD3D
 */
public class InventoryEvents
{
    public static final int FIRST_EVENT_NUMBER = 21000;

    public static final EventType START = new EventType(FIRST_EVENT_NUMBER + 100);

    public static final EventType SHEETS_LOAD_FINISHED = new EventType(FIRST_EVENT_NUMBER + 103);

    public static final EventType ROW_DELETION_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 400);
    public static final EventType ROW_CREATION_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 401);
    public static final EventType ROWS_CREATION_SUCCESS = new EventType(FIRST_EVENT_NUMBER + 402);

    public static final EventType EDIT_TYPES_CLICKED = new EventType(FIRST_EVENT_NUMBER + 403);
    public static final EventType EDIT_MODELS_CLICKED = new EventType();
    public static final EventType TYPE_CLOSE_CLICKED = new EventType();
    public static final EventType MODEL_CLOSE_CLICKED = new EventType();

    public static final EventType MAP_TAB_CLICKED = new EventType();
    public static final EventType RESERVATION_TAB_CLICKED = new EventType();
    public static final EventType MAP_SHOW_EXPLORER_CLICKED = new EventType();
    public static final EventType MAP_CHANGED = new EventType();
    public static final EventType DISPLAYED_LABEL_CHANGED = new EventType();
    public static final EventType LABEL_BUTTON_CLICKED = new EventType();
    public static final EventType EDITION_BUTTON_CLICKED = new EventType();
    public static final EventType MAP_COMPUTERS_LOADED = new EventType();
    public static final EventType MAP_SCREENS_LOADED = new EventType();
    public static final EventType MAP_DEVICES_LOADED = new EventType();
    public static final EventType MAP_COMPUTER_DROPPED = new EventType();
    public static final EventType MAP_COMPUTER_CLICKED = new EventType();
    public static final EventType MAP_COMPUTER_REMOVED = new EventType();
    public static final EventType MAP_SEARCH_SELECTED = new EventType();
    public static final EventType SCREEN_SEARCH_SELECTED = new EventType();
    public static final EventType DEVICE_SEARCH_SELECTED = new EventType();

    public static final EventType MACHINE_SCREEN_DROPPED = new EventType();
    public static final EventType MACHINE_DEVICE_DROPPED = new EventType();
    public static final EventType MACHINE_EDIT_LINKS = new EventType();

    public static final EventType MAP_COMPUTER_MOVED = new EventType();

    public static final EventType SHEET_DELETED = new EventType();

    public static final EventType CREATE_MENU_ASSIGNATION = new EventType();
    public static final EventType UPDATE_CELL_WORKER = new EventType();

    /** Filter events */
    public static final EventType DISPLAY_ALL = new EventType();
    public static final EventType DISPLAY_ONLY_OCTOCORE = new EventType();
    public static final EventType DISPLAY_ONLY_UNASSIGNED = new EventType();
    public static final EventType DISPLAY_ALL_ROOMS = new EventType();
    public static final EventType DISPLAY_ONE_ROOM = new EventType();

    /** Reservation events */
    public static final EventType RESERVATION_RESOURCES_LOADED = new EventType();
    public static final EventType RESERVATION_RESERVATIONS_LOADED = new EventType();
    public static final EventType RESERVATION_GRID_CREATED = new EventType();
    public static final EventType RESERVATION_FILTER_CHANGED = new EventType();
    public static final EventType RESERVATION_GRID_CHANGED = new EventType();
    public static final EventType RESERVATION_EVENT_EDITOR_CLICKED = new EventType();
    public static final EventType RESERVATION_RESERVATION_EDITOR_CLICKED = new EventType();
    public static final EventType RESERVATION_GROUP_CHANGED = new EventType();
    public static final EventType RESERVATION_REFRESH_CLICKED = new EventType();
    public static final EventType RESERVATION_RESERVATION_EDITOR_HIDE = new EventType();
    public static final EventType RESERVATION_RESERVATION_SAVE_SUCCESS = new EventType();
    public static final EventType RESERVATION_CREATE_RESERVATION_CLICKED = new EventType();
}

package fr.hd3d.inventory.ui.client.config;

/**
 * Static variables needed in application.
 * 
 * @author HD3D
 */
public class InventoryConfig
{
    /** CSS class for styling view combo box. */
    public static final String VIEW_COMBOBOX_STYLE = "select-view-combo";

    /** Cookie field that contains last selected sheet ID. */
    public static final String COOKIE_SHEET_VALUE = "selected_inventory_sheet";

    /** Icon style for types editor tool. */
    public static final String EDIT_TYPES_ICON = "edit-type-icon";
    /** Icon style for models editor tool. */
    public static final String EDIT_MODELS_ICON = "edit-model-icon";

    /** Data for computer DND */
    public static String COMPUTER_DROPPED_EVENT_VAR_NAME = "computer-dropped";
    public static final String DROP_LOCATION_X_EVENT_VAR_NAME = "x-drop-location";
    public static final String DROP_LOCATION_Y_EVENT_VAR_NAME = "y-drop-location";

    /** Data for screen DND */
    public static String SCREEN_DROPPED_EVENT_VAR_NAME = "screen-dropped";
    public static String SCREEN_DROP_LOCATION_COMPUTER_EVENT_VAR_NAME = "screen-dropped-location";

    /** Data for device DND */
    public static String DEVICE_DROPPED_EVENT_VAR_NAME = "device-dropped";
    public static String DEVICE_DROP_LOCATION_COMPUTER_EVENT_VAR_NAME = "device-dropped-location";

    public static final String PERSON_MENU_EVENT_VAR_NAME = "person-menu";

}

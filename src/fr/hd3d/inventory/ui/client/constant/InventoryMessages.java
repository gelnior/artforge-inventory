package fr.hd3d.inventory.ui.client.constant;

/**
 * Interface to represent the messages contained in resource bundle:
 * 	/home/frank.rousseau/workspace/ComputerInventory/war/WEB-INF/classes/fr/hd3d/inventory/ui/client/constant/InventoryMessages.properties'.
 */
public interface InventoryMessages extends com.google.gwt.i18n.client.Messages {
  
  /**
   * Translated "InventoryMessages".
   * 
   * @return translated "InventoryMessages"
   */
  @DefaultMessage("InventoryMessages")
  @Key("ClassName")
  String ClassName();
}

package fr.hd3d.inventory.ui.client.constant;

/**
 * Interface to represent the constants contained in resource bundle:'/home/frank.rousseau/workspace/ComputerInventory/war/WEB-INF/classes/fr/hd3d/inventory/ui/client/constant/InventoryConstants.properties
 * ' .
 */
public interface InventoryConstants extends com.google.gwt.i18n.client.Constants
{

    /**
     * Translated "Active".
     * 
     * @return translated "Active"
     */
    @DefaultStringValue("Active")
    @Key("Active")
    String Active();

    /**
     * Translated "Add".
     * 
     * @return translated "Add"
     */
    @DefaultStringValue("Add")
    @Key("Add")
    String Add();

    /**
     * Translated "Available relations".
     * 
     * @return translated "Available relations"
     */
    @DefaultStringValue("Available relations")
    @Key("AvailableRelations")
    String AvailableRelations();

    /**
     * Translated "Average".
     * 
     * @return translated "Average"
     */
    @DefaultStringValue("Average")
    @Key("Average")
    String Average();

    /**
     * Translated "Bad".
     * 
     * @return translated "Bad"
     */
    @DefaultStringValue("Bad")
    @Key("Bad")
    String Bad();

    /**
     * Translated "InventoryConstants".
     * 
     * @return translated "InventoryConstants"
     */
    @DefaultStringValue("InventoryConstants")
    @Key("ClassName")
    String ClassName();

    /**
     * Translated "Classic".
     * 
     * @return translated "Classic"
     */
    @DefaultStringValue("Classic")
    @Key("Classic")
    String Classic();

    /**
     * Translated "Close".
     * 
     * @return translated "Close"
     */
    @DefaultStringValue("Close")
    @Key("Close")
    String Close();

    /**
     * Translated "Computer".
     * 
     * @return translated "Computer"
     */
    @DefaultStringValue("Computer")
    @Key("Computer")
    String Computer();

    /**
     * Translated "Data initialized".
     * 
     * @return translated "Data initialized"
     */
    @DefaultStringValue("Data initialized")
    @Key("DataInitialized")
    String DataInitialized();

    /**
     * Translated "Destroyed".
     * 
     * @return translated "Destroyed"
     */
    @DefaultStringValue("Destroyed")
    @Key("Destroyed")
    String Destroyed();

    /**
     * Translated "Device".
     * 
     * @return translated "Device"
     */
    @DefaultStringValue("Device")
    @Key("Device")
    String Device();

    /**
     * Translated "Domain".
     * 
     * @return translated "Domain"
     */
    @DefaultStringValue("Domain")
    @Key("Domain")
    String Domain();

    /**
     * Translated "Edit models".
     * 
     * @return translated "Edit models"
     */
    @DefaultStringValue("Edit models")
    @Key("EditModels")
    String EditModels();

    /**
     * Translated "Edit types".
     * 
     * @return translated "Edit types"
     */
    @DefaultStringValue("Edit types")
    @Key("EditTypes")
    String EditTypes();

    /**
     * Translated "Error".
     * 
     * @return translated "Error"
     */
    @DefaultStringValue("Error")
    @Key("Error")
    String Error();

    /**
     * Translated "Excellent".
     * 
     * @return translated "Excellent"
     */
    @DefaultStringValue("Excellent")
    @Key("Excellent")
    String Excellent();

    /**
     * Translated "Float".
     * 
     * @return translated "Float"
     */
    @DefaultStringValue("Float")
    @Key("Float")
    String Float();

    /**
     * Translated "Good".
     * 
     * @return translated "Good"
     */
    @DefaultStringValue("Good")
    @Key("Good")
    String Good();

    /**
     * Translated "Graphic card".
     * 
     * @return translated "Graphic card"
     */
    @DefaultStringValue("Graphic card")
    @Key("GraphicCard")
    String GraphicCard();

    /**
     * Translated "Hard drive".
     * 
     * @return translated "Hard drive"
     */
    @DefaultStringValue("Hard drive")
    @Key("HardDrive")
    String HardDrive();

    /**
     * Translated "HD3D - Computer Area Inventory".
     * 
     * @return translated "HD3D - Computer Area Inventory"
     */
    @DefaultStringValue("HD3D - Computer Area Inventory")
    @Key("Hd3dComputerInventory")
    String Hd3dComputerInventory();

    /**
     * Translated "Inactive".
     * 
     * @return translated "Inactive"
     */
    @DefaultStringValue("Inactive")
    @Key("Inactive")
    String Inactive();

    /**
     * Translated "Keyboard".
     * 
     * @return translated "Keyboard"
     */
    @DefaultStringValue("Keyboard")
    @Key("Keyboard")
    String Keyboard();

    /**
     * Translated "Loading...".
     * 
     * @return translated "Loading..."
     */
    @DefaultStringValue("Loading...")
    @Key("Loading")
    String Loading();

    /**
     * Translated "Logs".
     * 
     * @return translated "Logs"
     */
    @DefaultStringValue("Logs")
    @Key("Logs")
    String Logs();

    /**
     * Translated "Missing".
     * 
     * @return translated "Missing"
     */
    @DefaultStringValue("Missing")
    @Key("Missing")
    String Missing();

    /**
     * Translated "Model Editor".
     * 
     * @return translated "Model Editor"
     */
    @DefaultStringValue("Model Editor")
    @Key("ModelEditor")
    String ModelEditor();

    /**
     * Translated "Mouse".
     * 
     * @return translated "Mouse"
     */
    @DefaultStringValue("Mouse")
    @Key("Mouse")
    String Mouse();

    /**
     * Translated "New name".
     * 
     * @return translated "New name"
     */
    @DefaultStringValue("New name")
    @Key("NewName")
    String NewName();

    /**
     * Translated "No Group Selected".
     * 
     * @return translated "No Group Selected"
     */
    @DefaultStringValue("No Group Selected")
    @Key("NoGroupSelected")
    String NoGroupSelected();

    /**
     * Translated "No selection".
     * 
     * @return translated "No selection"
     */
    @DefaultStringValue("No selection")
    @Key("NoSelection")
    String NoSelection();

    /**
     * Translated "No worker".
     * 
     * @return translated "No worker"
     */
    @DefaultStringValue("No worker")
    @Key("NoWorker")
    String NoWorker();

    /**
     * Translated "Node-lock".
     * 
     * @return translated "Node-lock"
     */
    @DefaultStringValue("Node-lock")
    @Key("NodeLock")
    String NodeLock();

    /**
     * Translated "OK".
     * 
     * @return translated "OK"
     */
    @DefaultStringValue("OK")
    @Key("OK")
    String OK();

    /**
     * Translated "Out of service".
     * 
     * @return translated "Out of service"
     */
    @DefaultStringValue("Out of service")
    @Key("OOS")
    String OOS();

    /**
     * Translated "Other".
     * 
     * @return translated "Other"
     */
    @DefaultStringValue("Other")
    @Key("Other")
    String Other();

    /**
     * Translated "Overwrite".
     * 
     * @return translated "Overwrite"
     */
    @DefaultStringValue("Overwrite")
    @Key("Overwrite")
    String Overwrite();

    /**
     * Translated "Printer".
     * 
     * @return translated "Printer"
     */
    @DefaultStringValue("Printer")
    @Key("Printer")
    String Printer();

    /**
     * Translated "Selected record is no more on server.".
     * 
     * @return translated "Selected record is no more on server."
     */
    @DefaultStringValue("Selected record is no more on server.")
    @Key("RecordNotOnServer")
    String RecordNotOnServer();

    /**
     * Translated "Relation tool".
     * 
     * @return translated "Relation tool"
     */
    @DefaultStringValue("Relation tool")
    @Key("RelationTool")
    String RelationTool();

    /**
     * Translated "Relations to set".
     * 
     * @return translated "Relations to set"
     */
    @DefaultStringValue("Relations to set")
    @Key("RelationsToSet")
    String RelationsToSet();

    /**
     * Translated "Rename group".
     * 
     * @return translated "Rename group"
     */
    @DefaultStringValue("Rename group")
    @Key("RenameGroup")
    String RenameGroup();

    /**
     * Translated "Render".
     * 
     * @return translated "Render"
     */
    @DefaultStringValue("Render")
    @Key("Render")
    String Render();

    /**
     * Translated "Retrieved data are malformed.".
     * 
     * @return translated "Retrieved data are malformed."
     */
    @DefaultStringValue("Retrieved data are malformed.")
    @Key("RetrievedDataAreMalformed")
    String RetrievedDataAreMalformed();

    /**
     * Translated "Running".
     * 
     * @return translated "Running"
     */
    @DefaultStringValue("Running")
    @Key("Running")
    String Running();

    /**
     * Translated "Saving...".
     * 
     * @return translated "Saving..."
     */
    @DefaultStringValue("Saving...")
    @Key("Saving")
    String Saving();

    /**
     * Translated "Screen".
     * 
     * @return translated "Screen"
     */
    @DefaultStringValue("Screen")
    @Key("Screen")
    String Screen();

    /**
     * Translated "Please, select records before adding relations.".
     * 
     * @return translated "Please, select records before adding relations."
     */
    @DefaultStringValue("Please, select records before adding relations.")
    @Key("SelectRecord")
    String SelectRecord();

    /**
     * Translated "A server error occured. If you were saving data, please verify that data are correctly saved.".
     * 
     * @return translated
     *         "A server error occured. If you were saving data, please verify that data are correctly saved."
     */
    @DefaultStringValue("A server error occured. If you were saving data, please verify that data are correctly saved.")
    @Key("ServerError")
    String ServerError();

    /**
     * Translated "Software".
     * 
     * @return translated "Software"
     */
    @DefaultStringValue("Software")
    @Key("Software")
    String Software();

    /**
     * Translated "Type Editor".
     * 
     * @return translated "Type Editor"
     */
    @DefaultStringValue("Type Editor")
    @Key("TypeEditor")
    String TypeEditor();

    /**
     * Translated "Unknown".
     * 
     * @return translated "Unknown"
     */
    @DefaultStringValue("Unknown")
    @Key("Unknown")
    String Unknown();

    /**
     * Translated "Very Bad".
     * 
     * @return translated "Very Bad"
     */
    @DefaultStringValue("Very Bad")
    @Key("VeryBad")
    String VeryBad();

    /**
     * Translated "View :".
     * 
     * @return translated "View :"
     */
    @DefaultStringValue("View :")
    @Key("View")
    String View();

    /**
     * Translated
     * "There was a problem while retrieving general configuration file. Ensure connection to server is still available."
     * .
     * 
     * @return translated
     *         "There was a problem while retrieving general configuration file. Ensure connection to server is still available."
     */
    @DefaultStringValue("There was a problem while retrieving general configuration file. Ensure connection to server is still available.")
    @Key("configurationFileError")
    String configurationFileError();

    /**
     * Translated "Computer Inventory cannot establish connection to the data server.".
     * 
     * @return translated "Computer Inventory cannot establish connection to the data server."
     */
    @DefaultStringValue("Computer Inventory cannot establish connection to the data server.")
    @Key("dataServerError")
    String dataServerError();

    /**
     * Translated "database connection error".
     * 
     * @return translated "database connection error"
     */
    @DefaultStringValue("database connection error")
    @Key("databaseConnectionError")
    String databaseConnectionError();

    /**
     * Translated "delete".
     * 
     * @return translated "delete"
     */
    @DefaultStringValue("delete")
    @Key("delete")
    String delete();

    /**
     * Translated "Error".
     * 
     * @return translated "Error"
     */
    @DefaultStringValue("Error")
    @Key("error")
    String error();

    /**
     * Translated "ID".
     * 
     * @return translated "ID"
     */
    @DefaultStringValue("ID")
    @Key("identifier")
    String identifier();

    /**
     * Translated "Configuration file seems malformed. Please, correct it.".
     * 
     * @return translated "Configuration file seems malformed. Please, correct it."
     */
    @DefaultStringValue("Configuration file seems malformed. Please, correct it.")
    @Key("malformedConfigurationFileError")
    String malformedConfigurationFileError();

    /**
     * Translated "name".
     * 
     * @return translated "name"
     */
    @DefaultStringValue("name")
    @Key("name")
    String name();

    /**
     * Translated "You does not have the permission to execute this request.".
     * 
     * @return translated "You does not have the permission to execute this request."
     */
    @DefaultStringValue("You does not have the permission to execute this request.")
    @Key("noPermission")
    String noPermission();

    /**
     * Translated "redo".
     * 
     * @return translated "redo"
     */
    @DefaultStringValue("redo")
    @Key("redo")
    String redo();

    /**
     * Translated "refresh from database".
     * 
     * @return translated "refresh from database"
     */
    @DefaultStringValue("refresh from database")
    @Key("refreshFromDB")
    String refreshFromDB();

    /**
     * Translated "rename".
     * 
     * @return translated "rename"
     */
    @DefaultStringValue("rename")
    @Key("rename")
    String rename();

    /**
     * Translated "save to database".
     * 
     * @return translated "save to database"
     */
    @DefaultStringValue("save to database")
    @Key("saveToDB")
    String saveToDB();

    /**
     * Translated "save to database".
     * 
     * @return translated "save to database"
     */
    @DefaultStringValue("After Sales")
    @Key("AfterSales")
    String AfterSales();

    /**
     * Translated "undo".
     * 
     * @return translated "undo"
     */
    @DefaultStringValue("undo")
    @Key("undo")
    String undo();
}

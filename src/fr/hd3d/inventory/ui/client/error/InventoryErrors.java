package fr.hd3d.inventory.ui.client.error;

/**
 * Give a code to all events raised by the launcher.
 * 
 * @author HD3D
 */
public class InventoryErrors
{
    public static final int RELATION_NO_RECORD_SELECTED = 105;
}

package fr.hd3d.inventory.ui.client.model;

import java.util.List;

import com.extjs.gxt.ui.client.binder.StoreBinder;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreEvent;
import com.extjs.gxt.ui.client.widget.Component;

import fr.hd3d.common.ui.client.modeldata.inventory.InventoryItemModelData;
import fr.hd3d.inventory.ui.client.view.impl.MapView;


public class InventoryItemStoreBinder<M extends InventoryItemModelData> extends StoreBinder<Store<M>, MapView, M>
{

    public InventoryItemStoreBinder(Store<M> store, MapView container)
    {
        super(store, container);
    }

    @Override
    protected void createAll()
    {}

    @Override
    public Component findItem(M model)
    {
        return null;
    }

    @Override
    protected List<M> getSelectionFromComponent()
    {
        return null;
    }

    @Override
    protected void onAdd(StoreEvent<M> se)
    {}

    @Override
    protected void onRemove(StoreEvent<M> se)
    {}

    @Override
    protected void onUpdate(StoreEvent<M> se)
    {}

    @Override
    protected void removeAll()
    {}

    @Override
    protected void setSelectionFromProvider(List<M> selection)
    {}

    @Override
    protected void update(M model)
    {}
}

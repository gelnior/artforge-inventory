package fr.hd3d.inventory.ui.client.model;

import java.util.ArrayList;
import java.util.List;

import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.modeldata.reader.PersonReader;
import fr.hd3d.common.ui.client.modeldata.reader.ReaderFactory;
import fr.hd3d.common.ui.client.modeldata.resource.PersonModelData;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.restlet.IRestRequestHandler;
import fr.hd3d.common.ui.client.restlet.RestRequestHandlerSingleton;
import fr.hd3d.common.ui.client.service.ServicesClass;
import fr.hd3d.common.ui.client.service.ServicesPath;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.callback.RetrieveSheetCallback;


/**
 * Handles :
 * <ul>
 * <li>Sheet data needed by the explorer to know which object from services to display.</li>
 * <li>Identity panel data.</li>
 * </ul>
 * 
 * @author HD3D
 */
public class ListingModel extends MainModel
{
    /** Store containing all sheets available for user. */
    private final ServiceStore<SheetModelData> sheetStore = new ServiceStore<SheetModelData>(ReaderFactory
            .getSheetReader());

    /** Services path of the records currently displayed. */
    private String recordPath;
    /** Lightweight class name of the records currently displayed. */
    private String simpleClassName;

    /** The "Record to create in database" list. */
    private final ArrayList<Hd3dModelData> createdRecords = new ArrayList<Hd3dModelData>();
    /** The "Record to delete in database" list. */
    private final ArrayList<Hd3dModelData> deletedRecords = new ArrayList<Hd3dModelData>();

    /** The list of person available for assignation via context menu of worker renderer. */
    private final ServiceStore<PersonModelData> assignationStore = new ServiceStore<PersonModelData>(new PersonReader());

    /**
     * Default constructor.
     */
    public ListingModel()
    {}

    /**
     * @return Sheets available for user.
     */
    public ServiceStore<SheetModelData> getSheetStore()
    {
        return this.sheetStore;
    }

    /**
     * @return service path needed to save data.
     */
    public String getRecordPath()
    {
        return this.recordPath;
    }

    /**
     * Sets the record path corresponding to the bound class name given in parameter.
     * 
     * @param boundClassName
     *            Records bound class name parameter.
     */
    public void setRecordPath(String boundClassName)
    {
        this.recordPath = ServicesPath.getPath(boundClassName);
        this.simpleClassName = boundClassName;
    }

    /**
     * Clear sheet list and sets new one.
     * 
     * @param sheets
     */
    // public void setSheets(List<SheetModelData> sheets)
    // {
    // this.sheetStore.removeAll();
    // for (SheetModelData sheet : sheets)
    // {
    // this.sheetStore.add(sheet);
    // }
    // this.sheetStore.sort(SheetModelData.NAME_FIELD, SortDir.ASC);
    // }

    /**
     * Delete a view from the sheet store.
     * 
     * @param view
     */
    public void removeView(SheetModelData view)
    {
        this.sheetStore.remove(view);
    }

    /**
     * Load all available sheets for given classes.
     * 
     * @param classes
     *            Classes from which sheets are retrieved.
     */
    // public void loadAvailableSheets(List<String> classes)
    // {
    // String path = ServicesPath.getPath(SheetModelData.SIMPLE_CLASS_NAME);
    // Constraint constraint = new Constraint(EConstraintOperator.in, SheetModelData.BOUND_CLASS_NAME_FIELD,
    // classes,
    // null);
    // path += ParameterBuilder.parameterToString(constraint);
    //
    // if (classes.size() > 0)
    // {
    // IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
    // requestHandler.getRequest(path, new InitSheetsCallback(this));
    // }
    // else
    // {
    // EventDispatcher.forwardEvent(InventoryEvents.SHEETS_LOAD_FINISHED);
    // }

    // }

    /**
     * Sends a DELETE request to the sheet service.
     * 
     * @param sheet
     *            The sheet to delete.
     */
    public void deleteViewFromDb(SheetModelData sheet)
    {
        sheet.delete();
    }

    /**
     * @return "Records to create" list.
     */
    public List<Hd3dModelData> getCreatedRecords()
    {
        return this.createdRecords;
    }

    /**
     * Adds a record to the "Records to create" list.
     * 
     * @param record
     */
    public void addCreatedRecord(Hd3dModelData record)
    {
        this.createdRecords.add(record);
    }

    /**
     * Clear the "Records to create" list.
     */
    public void clearCreatedRecords()
    {
        this.createdRecords.clear();
    }

    /**
     * Remove a record from "Records to create" list.
     * 
     * @param record
     *            The record to remove.
     */
    public void removeCreatedRecord(Hd3dModelData record)
    {
        if (this.createdRecords.contains(record))
        {
            this.createdRecords.remove(record);
        }
    }

    /**
     * Save to database all records from "Records to create" list.
     */
    public void saveCreatedRecords()
    {
        for (Hd3dModelData record : this.createdRecords)
        {
            if (record == null)
                continue;
            record.setClassName(ServicesClass.getClass(simpleClassName));
            record.save(InventoryEvents.ROW_CREATION_SUCCESS);
        }
    }

    /**
     * @return List of deleted records by user but not yet deleted in the database.
     */
    public List<Hd3dModelData> getDeletedRecords()
    {
        return this.deletedRecords;
    }

    /**
     * Add a model to the deleted record list.
     * 
     * @param model
     *            The model to delete.
     */
    public void addDeletedRecord(Hd3dModelData model)
    {
        this.deletedRecords.add(model);
    }

    /**
     * Clear deleted records list.
     */
    public void clearDeletedRecords()
    {
        this.deletedRecords.clear();
    }

    /**
     * Save delete actions to database.
     */
    public void saveDeletedRecords()
    {
        for (Hd3dModelData record : this.deletedRecords)
        {
            record.setSimpleClassName(this.simpleClassName);
            record.delete();
        }
    }

    /**
     * Save created and deleted records.
     */
    public void save()
    {
        this.saveCreatedRecords();
        this.saveDeletedRecords();

        this.clearCreatedRecords();
        this.clearDeletedRecords();
    }

    public void retrieveNewSheet(Long id)
    {
        String path = ServicesPath.getPath(SheetModelData.SIMPLE_CLASS_NAME) + id;
        IRestRequestHandler requestHandler = RestRequestHandlerSingleton.getInstance();
        requestHandler.getRequest(path, new RetrieveSheetCallback(this));
    }

    public void addSheet(SheetModelData sheet)
    {
        this.sheetStore.add(sheet);
    }

    public String getSimpleClassName()
    {
        return this.simpleClassName;
    }

    public ServiceStore<PersonModelData> getAssignationStore()
    {
        return assignationStore;
    }
}

package fr.hd3d.inventory.ui.client.model.modeldata.reader;

import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.widget.explorer.model.reader.SheetReader;


/**
 * Give record reader instance needed to read JSON data from server.
 * 
 * @author HD3D
 */
public class SheetReaderSingleton
{
    /** The record reader instance to return. */
    private static IReader<SheetModelData> reader;

    /**
     * @return The record reader instance. If it is null, it returns a new real record reader.
     */
    public final static IReader<SheetModelData> getInstance()
    {
        if (reader == null)
        {
            reader = new SheetReader();
        }
        return reader;
    }

    /**
     * Sets the record instance. This method is useful for unit tests.
     * 
     * @param mock
     *            The request handler to set.
     */
    public final static void setInstanceAsMock(IReader<SheetModelData> mock)
    {
        reader = mock;
    }
}

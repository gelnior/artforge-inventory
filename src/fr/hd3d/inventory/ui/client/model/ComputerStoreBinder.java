package fr.hd3d.inventory.ui.client.model;

import java.util.List;

import com.extjs.gxt.ui.client.binder.StoreBinder;
import com.extjs.gxt.ui.client.store.Store;
import com.extjs.gxt.ui.client.store.StoreEvent;
import com.extjs.gxt.ui.client.widget.Component;

import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.inventory.ui.client.view.impl.MapView;


public class ComputerStoreBinder extends StoreBinder<Store<ComputerModelData>, MapView, ComputerModelData>
{

    public ComputerStoreBinder(Store<ComputerModelData> store, MapView container)
    {
        super(store, container);
    }

    @Override
    protected void createAll()
    {}

    @Override
    public Component findItem(ComputerModelData model)
    {
        return null;
    }

    @Override
    protected List<ComputerModelData> getSelectionFromComponent()
    {
        return null;
    }

    @Override
    protected void onAdd(StoreEvent<ComputerModelData> se)
    {
        ComputerModelData computer = (ComputerModelData) CollectionUtils.getFirst(se.getModels());
        this.component.drawMachine(computer);
    }

    @Override
    protected void onRemove(StoreEvent<ComputerModelData> se)
    {
        this.component.removeComputer(se.getModel());
    }

    @Override
    protected void onUpdate(StoreEvent<ComputerModelData> se)
    {

    }

    @Override
    protected void removeAll()
    {
        this.component.removeAllComputers();
    }

    @Override
    protected void setSelectionFromProvider(List<ComputerModelData> selection)
    {}

    @Override
    protected void update(ComputerModelData model)
    {}
}

package fr.hd3d.inventory.ui.client.model.callback;

import java.io.IOException;
import java.util.List;

import org.restlet.client.Request;
import org.restlet.client.Response;

import com.extjs.gxt.ui.client.data.ListLoadResult;
import com.google.gwt.core.client.GWT;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.reader.IReader;
import fr.hd3d.common.ui.client.modeldata.technical.SheetModelData;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;
import fr.hd3d.inventory.ui.client.model.ListingModel;
import fr.hd3d.inventory.ui.client.model.modeldata.reader.SheetReaderSingleton;


public class RetrieveSheetCallback extends BaseCallback
{
    ListingModel model;

    public RetrieveSheetCallback(ListingModel model)
    {
        super();
        this.model = model;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        try
        {
            this.loadSheetInModel(response.getEntity().getText());

            EventDispatcher.forwardEvent(InventoryEvents.SHEETS_LOAD_FINISHED);
        }
        catch (IOException e)
        {
            GWT.log("Error occurs while parsing sheet data.", e);
        }
    }

    private void loadSheetInModel(String json)
    {
        IReader<SheetModelData> reader = SheetReaderSingleton.getInstance();

        ListLoadResult<SheetModelData> result = reader.read(json);
        List<SheetModelData> sheets = result.getData();

        if (sheets.size() > 0)
        {
            this.model.addSheet(sheets.get(0));
        }
    }
}

package fr.hd3d.inventory.ui.client.model.callback;



/**
 * When sheets are retrieved it sets it in the explorer model (in fact in the sheet combo store).
 * 
 * @author HD3D
 */
// public class InitSheetsCallback extends BaseCallback
// {
//
// /** The model that handles sheets data. */
// private final ListingModel model;
//
// /**
// * Default constructor.
// *
// * @param model
// * The explorer model that handles sheets data.
// */
// public InitSheetsCallback(ListingModel model)
// {
// super();
// this.model = model;
// }
//
// @Override
// public void onSuccess(Request request, Response response)
// {
// try
// {
// this.loadSheetsInModel(response.getEntity().getText());
//
// EventDispatcher.forwardEvent(InventoryEvents.SHEETS_LOAD_FINISHED);
// }
// catch (IOException e)
// {
// GWT.log("Error occurs while parsing sheet data.", e);
// }
// }
//
// /**
// * Sets retrieved data into the model that handles sheets data
// *
// * @param json
// * The server response (sheets) at JSON format.
// */
// private void loadSheetsInModel(String json)
// {
// IReader<SheetModelData> reader = SheetReaderSingleton.getInstance();
//
// ListLoadResult<SheetModelData> result = reader.read(json);
// List<SheetModelData> sheets = result.getData();
//
// this.model.setSheets(sheets);
// }
// }

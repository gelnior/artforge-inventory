package fr.hd3d.inventory.ui.client.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.modeldata.Hd3dModelData;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


/**
 * When a row is created in database, it gets the id from server response, and sets it locally in the newly created row.
 * 
 * @author HD3D
 */
public class CreateRowCallback extends BaseCallback
{
    /** Record to update with its new ID. */
    private final Hd3dModelData record;

    /**
     * Default constructor.
     * 
     * @param record
     *            Record to update with its new ID.
     */
    public CreateRowCallback(Hd3dModelData record)
    {
        this.record = record;
    }

    @Override
    public void onSuccess(Request request, Response response)
    {
        String url = response.getEntity().getLocationRef().toString();
        long id = new Long(url.substring(url.lastIndexOf('/') + 1));
        this.record.setId(id);

        EventDispatcher.forwardEvent(InventoryEvents.ROW_CREATION_SUCCESS);
    }
}

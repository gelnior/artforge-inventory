package fr.hd3d.inventory.ui.client.model.callback;



/**
 * When a sheet is deleted, it warn the controllers that the view has been correctly deleted.
 * 
 * @author HD3D
 */
// public class DeleteSheetCallback extends BaseCallback
// {
// @Override
// public void onSuccess(Request request, Response response)
// {
// // EventDispatcher.forwardEvent(InventoryEvents.DELETE_VIEW_FINISHED);
// }
// }

package fr.hd3d.inventory.ui.client.model.callback;

import org.restlet.client.Request;
import org.restlet.client.Response;

import fr.hd3d.common.ui.client.event.EventDispatcher;
import fr.hd3d.common.ui.client.service.callback.BaseCallback;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


/**
 * When a row is successfully deleted, it warns the controller that deletion succeed.
 * 
 * @author HD3D
 */
public class DeleteRowCallback extends BaseCallback
{
    @Override
    public void onSuccess(Request request, Response response)
    {
        EventDispatcher.forwardEvent(InventoryEvents.ROW_DELETION_SUCCESS);
    }
}

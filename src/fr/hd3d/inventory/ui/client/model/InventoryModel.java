package fr.hd3d.inventory.ui.client.model;

import fr.hd3d.common.client.enums.ESheetType;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.common.ui.client.widget.sheeteditor.model.SheetEditorModel;


/**
 * Main model handles application configuration data.
 * 
 * @author HD3D
 */
public class InventoryModel extends MainModel
{
    /** Name of configuration file needed by sheet editor. */
    private static final String DATA_TYPE_FILE = "config/items_info_inventory.json";

    private boolean isListingModified = false;

    public InventoryModel()
    {
        SheetEditorModel.addDataFile(DATA_TYPE_FILE);
        SheetEditorModel.setType(ESheetType.MACHINE);
    }

    public void setListingModified(boolean isListingModified)
    {
        this.isListingModified = isListingModified;
    }

    public boolean isListingModified()
    {
        return isListingModified;
    }

}

package fr.hd3d.inventory.ui.client.model;

import java.util.ArrayList;

import com.extjs.gxt.ui.client.event.EventType;

import fr.hd3d.common.client.enums.EConstraintOperator;
import fr.hd3d.common.ui.client.listener.EventLoadListener;
import fr.hd3d.common.ui.client.modeldata.inventory.ComputerModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.DeviceModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.ScreenModelData;
import fr.hd3d.common.ui.client.modeldata.inventory.StudioMapModelData;
import fr.hd3d.common.ui.client.modeldata.reader.ComputerReader;
import fr.hd3d.common.ui.client.modeldata.reader.DeviceReader;
import fr.hd3d.common.ui.client.modeldata.reader.ScreenReader;
import fr.hd3d.common.ui.client.service.parameter.Constraint;
import fr.hd3d.common.ui.client.service.store.AllLoadListener;
import fr.hd3d.common.ui.client.service.store.ServiceStore;
import fr.hd3d.common.ui.client.util.CollectionUtils;
import fr.hd3d.common.ui.client.widget.mainview.MainModel;
import fr.hd3d.inventory.ui.client.controller.event.InventoryEvents;


/**
 * MapModel handles data displayed by MapView.
 * 
 * @author HD3D
 */
public class MapModel extends MainModel
{
    private final ServiceStore<ComputerModelData> computerStore = new ServiceStore<ComputerModelData>(
            new ComputerReader());
    private final ServiceStore<ComputerModelData> searchComputerStore = new ServiceStore<ComputerModelData>(
            new ComputerReader());
    private final Constraint computerConstraint = new Constraint(EConstraintOperator.in, ComputerModelData.ID_FIELD,
            new ArrayList<Long>());

    private final ServiceStore<ScreenModelData> screenStore = new ServiceStore<ScreenModelData>(new ScreenReader());
    private final ServiceStore<ScreenModelData> searchScreenStore = new ServiceStore<ScreenModelData>(
            new ScreenReader());
    private final Constraint screenConstraint = new Constraint(EConstraintOperator.in, ScreenModelData.ID_FIELD,
            new ArrayList<Long>());

    private final ServiceStore<DeviceModelData> deviceStore = new ServiceStore<DeviceModelData>(new DeviceReader());
    private final ServiceStore<DeviceModelData> searchDeviceStore = new ServiceStore<DeviceModelData>(
            new DeviceReader());
    private final Constraint deviceConstraint = new Constraint(EConstraintOperator.in, DeviceModelData.ID_FIELD,
            new ArrayList<Long>());

    private ComputerModelData selectedComputer;
    private ComputerModelData computerWithEditedLinks;

    public MapModel()
    {
        this.computerStore.addParameter(computerConstraint);
        this.computerStore.addLoadListener(new EventLoadListener(InventoryEvents.MAP_COMPUTERS_LOADED));
        this.searchComputerStore.addLoadListener(new AllLoadListener<ComputerModelData>(this.searchComputerStore,
                new EventType()));

        this.screenStore.addParameter(screenConstraint);
        this.screenStore.addLoadListener(new EventLoadListener(InventoryEvents.MAP_SCREENS_LOADED));
        this.searchScreenStore.addLoadListener(new AllLoadListener<ScreenModelData>(this.searchScreenStore,
                new EventType()));

        this.deviceStore.addParameter(deviceConstraint);
        this.deviceStore.addLoadListener(new EventLoadListener(InventoryEvents.MAP_DEVICES_LOADED));
        this.searchDeviceStore.addLoadListener(new AllLoadListener<DeviceModelData>(this.searchDeviceStore,
                new EventType()));
    }

    public ServiceStore<ComputerModelData> getComputerStore()
    {
        return this.computerStore;
    }

    public ServiceStore<ComputerModelData> getSearchComputerStore()
    {
        return this.searchComputerStore;
    }

    public ServiceStore<ScreenModelData> getScreenStore()
    {
        return screenStore;
    }

    public ServiceStore<ScreenModelData> getSearchScreenStore()
    {
        return searchScreenStore;
    }

    public ServiceStore<DeviceModelData> getDeviceStore()
    {
        return deviceStore;
    }

    public ServiceStore<DeviceModelData> getSearchDeviceStore()
    {
        return searchDeviceStore;
    }

    public void reloadComputerStore(StudioMapModelData map)
    {
        if (CollectionUtils.isNotEmpty(map.getComputerIDs()))
        {
            computerConstraint.setLeftMember(map.getComputerIDs());
            this.computerStore.reload();
        }
    }

    public ComputerModelData getSelectedComputer()
    {
        return selectedComputer;
    }

    public void setSelectedComputer(ComputerModelData computer)
    {
        this.selectedComputer = computer;
    }

    public void linkComputerScreen(ComputerModelData computer, ScreenModelData screen)
    {
        computer.addScreen(screen);
        screen.addComputer(computer);

        computer.save();
        screen.save();
    }

    public void unlinkComputerScreen(ComputerModelData computer, ScreenModelData screen)
    {
        computer.removeScreen(screen);
        screen.removeComputer(computer);

        computer.save();
        screen.save();
    }

    public void linkComputerDevice(ComputerModelData computer, DeviceModelData device)
    {
        computer.addDevice(device);
        device.addComputer(computer);

        computer.save();
        device.save();
    }

    public void unlinkComputerDevice(ComputerModelData computer, DeviceModelData device)
    {
        computer.removeDevice(device);
        device.removeComputer(computer);

        computer.save();
        device.save();
    }

    public void setComputerWithEditedLinks(ComputerModelData computerWithEditedLinks)
    {
        this.computerWithEditedLinks = computerWithEditedLinks;
    }

    public ComputerModelData getComputerWithEditedLinks()
    {
        computerWithEditedLinks.refresh();
        return computerWithEditedLinks;
    }

    public ComputerModelData getComputerById(Long computerId)
    {
        for (ComputerModelData computer : this.computerStore.getModels())
        {
            if (computer.getId().equals(computerId))
                return computer;
        }
        return null;
    }

}
